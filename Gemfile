# frozen_string_literal: true
source 'https://rubygems.org'

gem 'rails', '~> 5.0.0', '>= 5.0.0.1'

gem 'dotenv-rails'

gem 'pg'
gem 'pg_search'
gem 'counter_culture', github: 'deniskorobicyn/counter_culture',
                       branch: 'polymorphic-assosiations'
gem 'order_as_specified'
# schema_plus would be nice once it supports rails 5

gem 'puma'

gem 'sass-rails'
gem 'uglifier'
gem 'coffee-rails'
gem 'jbuilder'
gem 'chosen-rails'
gem 'meta-tags'

# TODO: add fix refs before release
gem 'activeadmin', github: 'activeadmin'
gem 'inherited_resources', github: 'activeadmin/inherited_resources'
gem 'devise'

gem 'grape'
gem 'grape-entity', '0.4.8' # because this is the max grape-entity-matchers can take atm
gem 'grape-kaminari'
gem 'grape_logging'
gem 'grape-swagger'
gem 'grape-swagger-entity'
gem 'grape-swagger-rails'

gem 'acts_as_list'

gem 'paperclip'
gem 'aws-sdk'
gem 'streamio-ffmpeg'

gem 'sidekiq'
gem 'apn_sender', github: 'manuelvanrijn/apn_sender' # allows activesupport > 5

gem 'heroic-sns', github: 'team-supercharge/heroic-sns'

gem 'rack-dev-mark'
gem 'ffaker'

gem 'slack-ruby-client'
gem 'gibbon'
gem 'geocoder'
gem 'redis', '~>3.2'
gem 'whenever', :require => false
gem 'twitter'
gem 'intercom', '~> 3.5.23'
gem 'one_signal'

# gem 'codeclimate-test-reporter', group: :test, require: nil

## Authorization
gem 'koala'
gem 'oauth'

group :development, :test do
  gem 'fakes3'

  gem 'byebug', platform: :mri

  gem 'rubocop', require: false
  gem 'rubocop-checkstyle_formatter', require: false

  gem 'factory_girl_rails'
  gem 'database_cleaner'
  gem 'rspec-rails'
  gem 'rspec_junit_formatter'
  gem 'turnip'
  gem 'jsonpath'
  gem 'shoulda-matchers'
  gem 'grape-entity-matchers', github: 'team-supercharge/grape-entity-matchers'
  gem 'rails-controller-testing'

  gem 'vcr'
  gem 'webmock'
end

group :test do
  gem 'simplecov', require: false
  gem 'nyan-cat-formatter'
end

group :development do
  gem 'overcommit'

  gem 'foreman'
  gem 'powder'

  gem 'web-console'
  gem 'listen'
  gem 'spring'
  gem 'spring-watcher-listen'
  gem 'annotate'

  gem 'grape_on_rails_routes'

  gem 'ultrahook'
end

group :deployment do
  gem 'capistrano'
  gem 'capistrano-rails'
  gem 'capistrano-rvm'
  gem 'capistrano-bundler'
  gem 'capistrano-postgresql'
  gem 'capistrano3-nginx'
  gem 'capistrano3-puma'
  gem 'capistrano-sidekiq'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
