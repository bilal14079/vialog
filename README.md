# Towards API

# Project Initialization

* `bundle install`
* `powder install`
* `powder link`

# Start Server

* `foreman start`
* open `http://<service>.towards.dev:5000`

# Overcommit

* setup the hooks with `overcommit --install`
* if you change `.overcommit.yml` you need to sign it with `overcommit --sign`

# Push notifications, old version, now OneSignal is used

* the certificate needs to be a `.pem` file named `"apn_#{Rails.env}.pem"` in `config/certs`
* notification will only be delivered if the `PUSH_NOTIFICATIONS_ENABLED` environment variable is set
* push notification delivery is done in the background via `sidekiq`, in `development` you can start the `rails server` and `sidekiq` togerter with `foreman`
* you can find a certificate [here](https://gitlab.supercharge.io/towards/towards-ios/wikis/home) for develoment purposes

# Environment variables needed in production

* `SECRET_KEY_BASE`
* `TWITTER_KEY`
* `TWITTER_SECRET`
* `GOOGLE_API_KEY`
* `AWS_ACCESS_KEY_ID`
* `AWS_SECRET_ACCESS_KEY`
* `AWS_REGION`
* `S3_BUCKET_NAME`
* `PIPELINE_ID`
* `LOW_QUALITY_PRESET_ID`
* `MED_QUALITY_PRESET_ID`
* `HIGH_QUALITY_PRESET_ID`
* `PUSH_NOTIFICATIONS_ENABLED`
* `APP_NAME`
* `APP_ID`
* `APP_PROTOCOL`
* `OG_CANONICAL_BASE`
* `ADMIN_SUBDOMAIN`
* `API_SUBDOMAIN`
* `SNS_SUBDOMAIN`
* `VIDEO_SUBDOMAIN`
* `ASK_SUBDOMAIN`
* `SUPPORT_SUBDOMAIN`
* `TERMS_SUBDOMAIN`
* `MAILCHIMP_SUBSCRIBE_URL`
* `MAILCHIMP_CAPTCHA_HASH`
* `MAILCHIMP_API_KEY`
* `MAILCHIMP_LIST_ID`
* `SUPPORT_URL`
* `TERMS_URL`
* `SLACK_TOKEN`
* `SLACK_OFFENSE_CHANNEL`
* `ONE_SIGNAL_API_KEY`
* `ONE_SIGNAL_APP_ID`

# Subdomains

* `admin`
* `api`
* `sns`
* `v`
* `ask`
* `support`
* `terms`

# Deploy process

* cap staging/production deploy

# Video upload process

* POST /v1/topics/:id/upload_opinion endpoint
* Opinion object is created
* after_save callback starts the transcoding (AWS Elastic Transcoder)
* When it finishes we receive an SNS notification, which is handled by the SnsController handle_notification function
* the SnsMessageHandler updates the db
* the after_transcoding callbacks runs (they are in the Opinion class)


