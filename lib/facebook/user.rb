# frozen_string_literal: true
require 'koala'

module Facebook
  class AuthenticationError < StandardError; end

  class User
    def initialize(access_token)
      oauth = Koala::Facebook::OAuth.new(ENV['FACEBOOK_APP_ID2'], ENV['FACEBOOK_APP_SECRET2'])
      begin
        oauth.exchange_access_token(access_token)
      rescue Koala::Facebook::OAuthTokenRequestError
        raise Facebook::AuthenticationError
      end

      @api = Koala::Facebook::API.new(access_token)

      begin
        @me ||= @api.get_object('me', fields: 'first_name,last_name,id,email')
      rescue Koala::Facebook::AuthenticationError
        raise Facebook::AuthenticationError
      end
    end

    def first_name
      @first_name ||= @me['first_name']
    end

    def last_name
      @last_name ||= @me['last_name']
    end

    def id
      @id ||= @me['id']
    end

    def name
      @name ||= first_name + ' ' + last_name
    end

    def email
      @email ||= @me['email']
    end

    def friend_ids(limit = 5000)
      # returns [] if user_friends permission is not granted
      @friend_ids ||= @api.get_connections('me', 'friends', limit: limit, fields: 'id').map { |user| user['id'] }
    end

    def profile_picture_url
      "https://graph.facebook.com/#{id}/picture?width=500&height=500"
    end
  end
end
