# frozen_string_literal: true
require 'facebook/user'

module Facebook
  class Authenticator
    def initialize(access_token:, device_uid:)
      @access_token = access_token
      @device_uid = device_uid
    end

    def fb_user
      @fb_user ||= Facebook::User.new(@access_token)
    end

    def authenticate
      @user = ::User.find_by_facebook_uid(fb_user.id)
      if @user.present?
        create_or_update_token!
      else
        token = create_user_with_token!
        @user.user_settings.create(key: 'issue_tracking', value: 'true')
        # notify_friends
        follow_friends
        token
      end
    end

    def create_or_update_token!
      @user.email = fb_user.email if fb_user.email && !fb_user.email.empty? && fb_user.email != @user.email
      @user.save
      session_token = @user.session_tokens.where(provider: 'facebook', device_uid: @device_uid).first_or_initialize

      session_token.tap do |token|
        token.challenge = @access_token
        token.save!
      end
    end

    def create_user_with_token!
      ActiveRecord::Base.transaction do
        @user = ::User.create!(name: fb_user.name,
                               profile_picture_url: fb_user.profile_picture_url,
                               facebook_uid: fb_user.id,
                               email: fb_user.email)
        SessionToken.create!(provider: 'facebook',
                             challenge: @access_token,
                             device_uid: @device_uid,
                             user: @user)
      end
    end

    def notify_friends
      # TODO: this should be done in the background
      facebook_friends = ::User.where(facebook_uid: fb_user.friend_ids)
      facebook_friends.find_each do |facebook_friend|
        FacebookFriendJoinedNotification.create(subject: @user, user: facebook_friend)
      end
    end

    def follow_friends
      facebook_friends = ::User.where(facebook_uid: fb_user.friend_ids)
      facebook_friends.find_each do |facebook_friend|
        facebook_friend.follow_user(@user)
        @user.follow_user(facebook_friend)
      end
    end
  end
end
