# frozen_string_literal: true
class SnsMessageHandler
  attr_reader :payload

  def self.process(message)
    new(message).process_message
  end

  def initialize(message)
    @payload = JSON.parse(message.body)
  end

  def process_message
    return unless opinion

    opinion.mark_as_transcoded(video_hls_url: video_hls_url,
                               video_thumbnail_url: video_thumbnail_url)
  end

  def job_id
    payload['jobId']
  end

  def opinion
    @opinion ||= Opinion.find_by(elastic_transcoder_job_id: job_id)
  end

  def video_hls_url
    "#{s3_url_base}#{playlist_name}"
  end

  def video_thumbnail_url
    "#{s3_url_base}#{thumbnail_name}"
  end

  def playlist_name
    playlist = payload['playlists'].first || { name: 'master' }

    "#{playlist['name']}.m3u8"
  end

  def thumbnail_name
    'thumb-00001.jpg'
  end

  def s3_url_base
    return '' unless opinion

    "https://s3-#{opinion.video.s3_region}.amazonaws.com/" \
      "#{opinion.video.s3_bucket.name}/#{payload['outputKeyPrefix']}"
  end
end
