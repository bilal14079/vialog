class TimeToText
  def initialize(time)
    @time = time
  end
  
  def to_text
    if @time < 60
      "#{@time} s"
    elsif @time < 60 * 60
      "#{@time / 60} m #{@time % 60} s"
    elsif @time < 24 * 60 * 60
      hour = @time / (60 * 60)
      min = (@time - hour * 60 * 60) / 60
      sec = (@time - hour * 60 * 60) % 60
      "#{hour} h #{min} m #{sec} s"
    else
      day = @time / (24 * 60 * 60)
      hour = (@time - day * 24 * 60 * 60) / (60 * 60)
      min = (@time - day * 24 * 60 * 60 - hour * 60 * 60) / 60
      sec = (@time - day * 24 * 60 * 60 - hour * 60 * 60) % 60
      "#{day} d #{hour} h #{min} m #{sec} s"
    end
  end
end