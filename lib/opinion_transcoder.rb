# frozen_string_literal: true
class OpinionTranscoder
  attr_reader :opinion

  def self.start_transcoding(opinion, low_quality)
    new(opinion, low_quality).start_transcoding
  end

  def initialize(opinion, low_quality)
    @low_quality = low_quality
    @opinion = opinion
    @uuid = SecureRandom.uuid
    @client = Aws::ElasticTranscoder::Client.new
  end

  def start_transcoding
    return unless transcoding_needed?

    job_response = @client.create_job pipeline_id: pipeline_id,
                                      input: {key: input_key},
                                      outputs: outputs,
                                      playlists: [master_playlist],
                                      output_key_prefix: output_key_prefix,
                                      user_metadata: metadata

    opinion.update_column :elastic_transcoder_job_id, job_response.job.id
  end

  def input_key
    opinion.video.s3_object.key
  end

  def transcoding_needed?
    s3_storage? && video_changed?
  end

  def s3_storage?
    opinion.video.options[:storage] == :s3
  end

  def video_changed?
    opinion.video_updated_at_changed?
  end

  def pipeline_id
    ENV.fetch('PIPELINE_ID')
  end

  def output_key_prefix
    "transcoded_opinions/#{@uuid}/"
  end

  def low_quality_output
    thumb_pattern = ''
    if @low_quality
      thumb_pattern = 'thumb-{count}'
    end
    {key: 'low_quality',
     thumbnail_pattern: thumb_pattern,
     preset_id: ENV.fetch('LOW_QUALITY_PRESET_ID_4'),
     segment_duration: '3'}
  end

  def med_quality_output
    {key: 'med_quality',
     thumbnail_pattern: '',
     preset_id: ENV.fetch('MED_QUALITY_PRESET_ID_4'),
     segment_duration: '3'}
  end

  def high_quality_output
    {key: 'high_quality',
     thumbnail_pattern: 'thumb-{count}',
     preset_id: ENV.fetch('HIGH_QUALITY_PRESET_ID_7'),
     segment_duration: '3'}
  end

  def low_outputs
    [low_quality_output]
  end

  def normal_outputs
    [low_quality_output, high_quality_output]
  end

  def outputs
    if @low_quality
      low_outputs
    else
      normal_outputs
    end
  end

  def output_keys
    outputs.map {|output| output[:key]}
  end

  def master_playlist
    {name: 'master',
     format: 'HLSv3',
     output_keys: output_keys}
  end

  def metadata
    {opinion_id: opinion.id.to_s}
  end
end
