# frozen_string_literal: true

require 'geocoder'

class GeocoderHelper
  def initialize(lat, lng)
    @result = Geocoder.search "#{lat},#{lng}"
  end

  def address
    @address ||= @result.first.data['address_components'] if @result.size > 0
  end

  def town_name
    @town_name ||= address_component_long_name types: %w(locality political)
  end

  def country
    @country ||= address_component_long_name types: %w(country political)
  end

  def country_code
    @country_code ||= address_component_short_name types: %w(country political)
  end

  def address_component_short_name(types:)
    address_component_name types: types,
                           param_name: 'short_name'
  end

  def address_component_long_name(types:)
    address_component_name types: types,
                           param_name: 'long_name'
  end

  def address_component_name(types:, param_name:)
    if address
      components = address.select do |c|
        types.all? do |t|
          c['types'].include?(t)
        end
      end

      if components.size > 0
        components.first[param_name]
      end
    end
  end
end