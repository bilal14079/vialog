# frozen_string_literal: true
require 'twitter'

module Twitter
  class GhostTweeter

    THRESHOLDS = [2, 11, 22, 111, 222, 1111, 2222, 11111, 22222].freeze

    def self.send_ghost_tweet(user:, topic:)
      return unless Rails.env.production?

      new(user, topic).tweet if user.ghost && THRESHOLDS.include?(user.request_count_in_topic(topic))
    end

    def initialize(user, topic)
      @user = user
      @topic = topic
    end

    def tweet
      begin
        $twitter.update(tweet_text)
      rescue Twitter::Error::Forbidden => e
        puts "Error while tweeting ghost profile: #{e}"
      end
    end

    def tweet_text
      "@#{@user.twitter_handle} 👋 #{ask_count_text}people are asking you about \"#{@topic.title}\" respond to this video thread on vialog 🌪️ https://appsto.re/gb/Kk_sfb.i"
    end

    def ask_count_text
      request_count = @user.request_count_in_topic(@topic)
      if request_count >= THRESHOLDS[1]
        "#{request_count} "
      else
        ''
      end
    end
  end
end