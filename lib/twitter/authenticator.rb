# frozen_string_literal: true
# require 'twitter_oauth'
require 'twitter/vialog_user'

module Twitter
  class AuthenticationError < StandardError; end

  class Authenticator
    def initialize(access_token:, access_token_secret:, device_uid:)
      @access_token = access_token
      @access_token_secret = access_token_secret
      @device_uid = device_uid
    end

    def twitter_user
      @twitter_user ||= Twitter::VialogUser.new(@access_token, @access_token_secret)
      raise Twitter::AuthenticationError unless @twitter_user.authorized?
      @twitter_user
    end

    def authenticate
      user = ::User.find_by_twitter_uid(twitter_user.id)
      if user.present?
        create_or_update_token!(user)
      else
        create_user_with_token!
      end
    end

    def create_or_update_token!(user)
      user.email = twitter_user.email if twitter_user.email && !twitter_user.email.empty? && twitter_user.email != user.email
      user.ghost = false
      user.bio = twitter_user.bio unless user.bio
      user.save

      user.session_tokens.where(provider: 'twitter', device_uid: @device_uid).first_or_create
    end

    def create_user_with_token!
      ActiveRecord::Base.transaction do
        user = ::User.create!(name: twitter_user.name,
                              twitter_handle: twitter_user.twitter_handle,
                              profile_picture_url: twitter_user.profile_picture_url,
                              twitter_uid: twitter_user.id,
                              bio: twitter_user.bio,
                              email: twitter_user.email)
        SessionToken.create!(provider: 'twitter',
                             device_uid: @device_uid,
                             user: user)
        user.user_settings.create(key: 'issue_tracking', value: 'true')
      end
    end
  end
end
