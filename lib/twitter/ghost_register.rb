# frozen_string_literals: true
require 'twitter'

module Twitter
  class GhostRegister
    def initialize(handle:)
      @handle = handle
    end

    def register
      return unless handle_exists?

      ::User.create!(name: twitter_name,
                     twitter_handle: twitter_handle,
                     profile_picture_url: profile_picture_url,
                     twitter_uid: twitter_uid,
                     bio: twitter_bio,
                     email: twitter_email,
                     ghost: true)
    end

    def twitter_email
      twitter_user.try(:email)
    end

    def twitter_uid
      twitter_user.id
    end

    def twitter_name
      twitter_user.name
    end

    def twitter_handle
      twitter_user.screen_name
    end

    def profile_picture_url
      "https://twitter.com/#{twitter_handle}/profile_image?size=original"
    end

    def twitter_bio
      twitter_user.description
    end

    def twitter_user
      @twitter_user ||= $twitter.user(@handle)
    end

    def handle_exists?
      begin
        twitter_user
        true
      rescue Twitter::Error::NotFound => _
        false
      end
    end
  end
end