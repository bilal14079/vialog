# frozen_string_literal: true
module Twitter
  class VialogUser
    def initialize(access_token, access_token_secret)
      consumer = OAuth::Consumer.new(ENV['TWITTER_KEY_2'],
                                     ENV['TWITTER_SECRET_2'],
                                     site: 'https://api.twitter.com',
                                     scheme: :header)

      access_token = OAuth::AccessToken.from_hash(consumer, oauth_token: access_token,
                                                            oauth_token_secret: access_token_secret)
      response = access_token.request(:get, 'https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true')
      @me ||= JSON.parse(response.body)
    end

    def authorized?
      @me['errors'].blank?
    end

    def id
      @id ||= @me['id']
    end

    def first_name
      @first_name ||= name.split[0]
    end

    def last_name
      @last_name ||= name.split.size > 1 ? name.split[1] : ''
    end

    def name
      @name ||= @me['name']
    end

    def twitter_handle
      @twitter_handle ||= @me['screen_name']
    end

    def email
      @email ||= @me['email']
    end

    def bio
      @bio ||= @me['description']
    end

    def profile_picture_url
      "https://twitter.com/#{twitter_handle}/profile_image?size=original"
    end
  end
end
