class TextHighlighter
  def initialize(text, query)
    @text = text
    @query = query
  end

  def highlight(formatter)
    text = @text.dup
    q = @query
    q_lower = q.downcase
    li = 0
    while text.downcase.index(q_lower, li)
      i = text.downcase.index(q_lower, li)
      r = i..(i + q.length - 1)
      sub = formatter.call(text[r])
      text[r] = sub
      li = i + sub.length
    end

    text
  end
end