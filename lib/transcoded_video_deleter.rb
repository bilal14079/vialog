# frozen_string_literal: true
require 'aws-sdk'

class TranscodedVideoDeleter
  # https://s3-eu-west-1.amazonaws.com/towards-staging/transcoded_opinions/a12e8aef-529f-4818-b74e-faaa35fcc522/master.m3u8
  FOLDER_REGEX = %r{transcoded_opinions/.*/}

  def initialize(video_hls_url)
    # Make sure not to delete everything with nil or '' prefix
    raise 'Not valid video hls url' unless video_hls_url[FOLDER_REGEX]

    @folder_path = path_from_file_url(video_hls_url)
    @bucket = Aws::S3::Resource.new.bucket(ENV['S3_BUCKET_NAME'])
  end

  def delete
    @bucket.objects(prefix: @folder_path).batch_delete!
  end

  def path_from_file_url(url)
    url[FOLDER_REGEX][0..-2]
  end
end
