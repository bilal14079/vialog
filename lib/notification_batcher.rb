# frozen_string_literal: true

class NotificationBatcher

  def self.batch_notifications(notifications)
    new(notifications).batch
  end

  def initialize(notifications)
    @notifications = notifications
  end

  def batch
    batched_notifications = unbatched_notifications.map {|noti| NotificationBatch.new([noti])}
    batched_notifications += group_by_topic(reactions).values.map {|notifications| NotificationBatch.new(notifications)}
    batched_notifications += group_by_topic(asks).values.map {|notifications| NotificationBatch.new(notifications)}
    # batched_notifications += group_by_topic(responds).values.map {|notifications| NotificationBatch.new(notifications)}

    batched_notifications
  end

  def reactions
    @reactions ||= @notifications.select do |noti|
      noti.notification_type == 'reaction_received'
    end
  end

  def asks
    @asks ||= @notifications.select do |noti|
      noti.notification_type == 'request_received'
    end
  end

  def responds
    @responds ||= @notifications.select do |noti|
      noti.notification_type == 'request_answered'
    end
  end

  def unbatched_notifications
    @unbatched_notifications ||= @notifications.select do |noti|
      !(%w(reaction_received request_received).include?(noti.notification_type))
    end
  end

  def group_by_topic(notifications)
    notifications.select {|noti| !noti.subject.nil?}.group_by do |noti|
      noti.push_topic_id
    end
  end
end