# frozen_string_literal: true
require 'intercom'

class IntercomHelper
  def self.handle_notification(notification)
    topic = notification[:topic]
    return if topic.nil?

    data = notification[:data][:item]

    case topic
      when "ping"
        handle_ping_notification(data)
      when "user.deleted"
        handle_user_delete_notification(data)
      when "user.created"
        handle_user_create_notification(data)
    end
  end

  def initialize
    @intercom = Intercom::Client.new(token: ENV['INTERCOM_TOKEN'])
  end

  def register_user(user)
    @intercom.users.create(user_id: user.intercom_id, email: user.email, name: user.name, signed_up_at: Time.now.to_i)
  end

  def update_attributes(user)
    intercom_user = @intercom.users.find(user_id: user.intercom_id)
    attrs = user.intercom_attributes

    attrs.keys.each do |key|
      intercom_user.custom_attributes[key.to_s] = attrs[key]
    end

    @intercom.users.save(intercom_user)
  end

  private
  def self.handle_ping_notification(data)
    { type: 'ping', message: 'pong' }
  end

  def self.handle_user_delete_notification(data)
    intercom_id = data[:user_id]
    user_id = intercom_id.split('-')[0].to_i
    environment = intercom_id.split('-')[1]
    return if environment != Rails.env

    user = User.find(user_id)
    user.update(intercom_status: User::INTERCOM_STATUS_DELETED)

    data
  end

  def self.handle_user_create_notification(data)
    intercom_id = data[:user_id]
    user_id = intercom_id.split('-')[0].to_i
    environment = intercom_id.split('-')[1]
    return if environment != Rails.env

    user = User.find(user_id)
    user.update(intercom_status: User::INTERCOM_STATUS_ACTIVE)

    data
  end
end