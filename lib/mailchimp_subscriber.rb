require 'gibbon'
require 'digest/md5'

class MailchimpSubscriber
  def initialize
    @gibbon = Gibbon::Request.new(api_key: api_key)
  end

  def subscribe(list_id:, email:, first_name:, last_name:)
    return if already_subscribed(list_id: list_id, email: email)

    @gibbon.lists(list_id).members.create(body: {email_address: email, status: 'subscribed', merge_fields: {FNAME: first_name, LNAME: last_name}})
  end

  def already_subscribed(list_id:, email:)
    email_md5 = Digest::MD5.hexdigest(email)
    begin
      @gibbon.lists(list_id).members(email_md5).retrieve
      true
    rescue Gibbon::MailChimpError => e
      if e.body['status'] == 404
        false
      else
        raise
      end
    end
  end

  def api_key
    ENV['MAILCHIMP_API_KEY']
  end
end