# frozen_string_literal: true
if Rails.env.development?
  task :set_annotation_options do
    Annotate.set_defaults show_indexes: false, simple_indexes: true
  end

  Annotate.load_tasks
end
