# frozen_string_literal: true
require 'redis'

namespace :opinions do
  desc 'Calculate opinion durations'
  task calculate_durations: [:dotenv, :environment] do
    Opinion.processed.find_each do |opinion|
      begin
        video_path = opinion.video.url
        tempfile = open(video_path)
        duration = FFMPEG::Movie.new(tempfile.path).duration

        opinion.update_column(:duration, duration)

        print '.'
      rescue StandardError => e
        puts e.message
      end
    end
  end

  desc 'Update the watch time of opinions'
  task update_watch_times: [:dotenv, :environment] do
    redis = Redis.new

    redis.hkeys(:opinion_watch_times).each do |opinion_id|
      watch_time = redis.hget(:opinion_watch_times, opinion_id).to_i
      redis.hincrby(:opinion_watch_times, opinion_id, -watch_time)

      begin
        opinion = Opinion.find(opinion_id)

        if opinion
          new_watch_time = opinion.watch_time + watch_time
          opinion.update(watch_time: new_watch_time)
        end
      rescue ActiveRecord::RecordNotFound => e
        redis.hdel(:opinion_watch_times, opinion_id)
      end

    end

    Topic.find_each do |topic|
      topic.update_watch_time
    end
  end
end
