# frozen_string_literal: true

namespace :topics do
  desc 'Generate Topic keywords'
  task generate_keywords: [:dotenv, :environment] do
    topic_keywords = Topic.all.each_with_object(Hash.new) { |t, h| h[t.id] = t.keywords_hash }
    threshold = [Topic.count / 10, 1].max

    all_keywords = []
    topic_keywords.each do |k, v|
      all_keywords += v[:generated].map { |kw| { word: kw[0], count: kw[1], topic: k } }
    end
    all_keywords = all_keywords.sort_by { |kw| kw[:count] }.reverse

    filtered_keywords = []
    all_keywords.each do |kw|
      word_count = filtered_keywords.count { |fkw| fkw[:word] == kw[:word] }
      filtered_keywords += [kw] #if word_count < threshold
    end

    keywords = {}
    topic_keywords.each do |topic_id, kw_hash|
      current_keywords = filtered_keywords.select { |kw| kw[:topic] == topic_id }.map{ |kw| kw[:word] }
      current_keywords += kw_hash[:constant]

      keywords[topic_id] = current_keywords.uniq.join(', ')
    end

    keywords.each do |topic_id, words|
      Topic.find(topic_id).update(keywords: words)
    end
  end
end