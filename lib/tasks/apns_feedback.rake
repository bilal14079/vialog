# frozen_string_literal: true
namespace :apns_feedback do
  desc 'Run APNS feedback and delete invalid tokens'
  task invalidate_tokens: [:dotenv, :environment] do
    APN::Feedback.new.tokens.each do |token|
      # PushNotificationRegistration.where(push_notification_token: token).destroy_all
      UserDevice.where(push_notification_token: token).destroy_all
    end
  end
end