# frozen_string_literal: true
namespace :opinion_requests do
  desc 'Fullfill unfullfilled requests with valid opinion'
  task fix_fullfill: [:dotenv, :environment] do
    OpinionRequest.where(fullfilled: false).find_each do |req|
      o = Opinion.where(user: req.requested_user, topic: req.topic).order(:created_at).first
      req.mark_as_fullfilled(o) if o
    end
  end
end