# frozen_string_literal: true
require 'intercom_helper'

namespace :intercom do
  desc 'Update the custom attributes for every active intercom user'
  task update_attributes: [:dotenv, :environment] do
    User.where(intercom_status: User::INTERCOM_STATUS_ACTIVE).find_each do |user|
      IntercomHelper.new.update_attributes(user)
    end
  end
end