# frozen_string_literal: true

require 'one_signal'

class OneSignalNotification
  def initialize(player_ids, message, data = {})
    player_ids = [player_ids] unless player_ids.is_a?(Array)

    @app_id = ENV['ONE_SIGNAL_APP_ID']
    @player_ids = player_ids
    @messages = { en: message }
    @data = data
  end

  def add_localization(language, message)
    @messages[language] = message
  end

  def send
    OneSignal::Notification.create(params: push_data)
  end

  private

  def push_data
    {
        app_id: @app_id,
        contents: @messages,
        data: @data,
        include_player_ids: @player_ids,
        ios_badgeType: 'SetTo',
        ios_badgeCount: @data[:badge] || 0
    }
  end
end
