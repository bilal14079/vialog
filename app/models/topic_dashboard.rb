# frozen_string_literal: true
# == Schema Information
#
# Table name: topic_dashboards
#
#  id         :integer          not null, primary key
#  main       :boolean          default(FALSE), indexed
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TopicDashboard < ApplicationRecord
  has_one :banner, inverse_of: :topic_dashboard, dependent: :destroy
  accepts_nested_attributes_for :banner, reject_if: :all_blank

  has_many :topic_modules, -> { order(position: :asc) }, inverse_of: :topic_dashboard, dependent: :destroy
  accepts_nested_attributes_for :topic_modules, allow_destroy: true

  # there can be only one main dashboard
  validates :main, uniqueness: { if: ->(dashboard) { dashboard.main } }
  validates :banner, presence: { unless: :main? }

  delegate :title, to: :banner, prefix: true, allow_nil: true

  def title
    banner_title || 'main'
  end

  def modules(country_code, latitude, longitude)
    topic_modules.select { |tm| tm.available_at?(country_code, latitude, longitude) }
  end
end
