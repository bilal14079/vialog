# frozen_string_literal: true
# == Schema Information
#
# Table name: banners
#
#  id                     :integer          not null, primary key
#  title                  :string           not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  topic_dashboard_id     :integer
#  thumbnail_file_name    :string
#  thumbnail_content_type :string
#  thumbnail_file_size    :integer
#  thumbnail_updated_at   :datetime
#

class Banner < ApplicationRecord
  THUMBNAIL_WIDTH = 796
  THUMBNAIL_HEIGHT = 312

  has_attached_file :thumbnail, path: 'banners/:id/thumbnail.:extension'
  validates_attachment :thumbnail, presence: true,
                                   content_type: { content_type: ['image/jpeg', 'image/png'] }

  belongs_to :topic_dashboard, dependent: :destroy
  has_many :topic_modules, dependent: :destroy

  validates :title, presence: true
  validates :thumbnail, image_dimension: { height: THUMBNAIL_HEIGHT, width: THUMBNAIL_WIDTH }

  delegate :url, to: :thumbnail, prefix: true, allow_nil: true
end
