# == Schema Information
#
# Table name: opinion_watches
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null, indexed
#  opinion_id :integer          not null, indexed
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class OpinionWatch < ApplicationRecord
  belongs_to :user, required: true
  belongs_to :opinion, required: true

  validates :user_id, uniqueness: {scope: [:user_id, :opinion_id]}
end
