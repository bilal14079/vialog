# frozen_string_literal: true
# == Schema Information
#
# Table name: hashtags
#
#  id         :integer          not null, primary key
#  name       :string           not null, indexed
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Hashtag < ApplicationRecord
  has_many :taggings, inverse_of: :hashtag, dependent: :destroy
  has_many :topics, through: :taggings, source: :taggable, source_type: 'Topic'

  validates :name, presence: true, uniqueness: true
end
