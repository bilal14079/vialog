# == Schema Information
#
# Table name: player_ids
#
#  id         :integer          not null, primary key
#  user_id    :integer          indexed
#  device_uid :string
#  player_id  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class PlayerId < ApplicationRecord
  belongs_to :user
end
