# frozen_string_literal: true
# == Schema Information
#
# Table name: users
#
#  id                    :integer          not null, primary key
#  name                  :string           not null, indexed
#  profile_picture_url   :string           not null
#  facebook_uid          :string           indexed
#  twitter_uid           :string           indexed
#  twitter_handle        :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  opinions_count        :integer          default(0), not null
#  follower_users_count  :integer          default(0), not null
#  followed_users_count  :integer          default(0), not null
#  followed_topics_count :integer          default(0), not null
#  email                 :string
#  last_lat              :decimal(, )
#  last_lng              :decimal(, )
#  last_country          :string
#  guest                 :boolean          default(FALSE)
#  ghost                 :boolean          default(FALSE)
#  bio                   :text
#  profile_video_id      :integer          indexed
#  handle                :string
#  intercom_status       :string           default("unregistered")
#  seen_time             :integer
#  hosted_issues_count   :integer
#  open_asks_count       :integer
#  responded_asks_count  :integer
#  last_login            :datetime
#

require 'geocoder_helper'
require 'time_to_text'

class User < ApplicationRecord
  include PgSearch
  pg_search_scope :search_by_name_and_handles, against: [:name, :twitter_handle, :handle], using: { tsearch: { prefix: true } }

  INTERCOM_STATUS_UNREGISTERED = 'unregistered'.freeze
  INTERCOM_STATUS_ACTIVE = 'active'.freeze
  INTERCOM_STATUS_DELETED = 'deleted'.freeze

  has_many :followed_followings,
           class_name: 'Following',
           foreign_key: 'follower_id',
           dependent: :destroy
  has_many :followed_by_followings,
           class_name: 'Following',
           as: :followed,
           dependent: :destroy
  has_many :followed_topics,
           through: :followed_followings,
           source: :followed,
           source_type: 'Topic'
  has_many :followed_users,
           through: :followed_followings,
           source: :followed,
           source_type: 'User'
  has_many :follower_users,
           through: :followed_by_followings,
           source: :follower,
           class_name: 'User'

  has_many :session_tokens, dependent: :destroy
  has_many :user_devices, dependent: :destroy
  has_many :player_ids, dependent: :destroy

  has_many :opinions, -> { processed }, dependent: :destroy
  has_many :opinioned_topics, -> { distinct }, through: :opinions, source: :topic

  has_many :offense_reports, dependent: :destroy

  has_many :push_notification_registrations, dependent: :destroy
  has_many :notifications, -> { ordered }, dependent: :destroy
  has_many :facebook_friend_joined_notifications, as: :subject, dependent: :destroy
  has_many :opinion_watches, dependent: :destroy

  has_many :requested_opinion_requests,
           -> { unfullfilled },
           class_name: 'OpinionRequest',
           foreign_key: 'requester_user_id',
           dependent: :destroy
  has_many :received_opinion_requests,
           -> { unfullfilled },
           class_name: 'OpinionRequest',
           foreign_key: 'requested_user_id',
           dependent: :destroy
  has_many :topics_needing_opinion,
           -> { distinct },
           through: :received_opinion_requests,
           source: :topic,
           class_name: 'Topic'
  # count cannot be called on this because of the inner select
  has_many :topics_needing_opinion_ordered_by_request_count,
           (lambda do
             select('topics.*, count(opinion_requests.topic_id) as opinion_requests_count')
                 .group('topics.id')
                 .order('opinion_requests_count desc')
           end),
           through: :received_opinion_requests,
           source: :topic,
           class_name: 'Topic'

  has_many :user_settings, dependent: :destroy

  has_many :claps, dependent: :destroy

  belongs_to :profile_video, class_name: 'Opinion'

  has_one :ghost_metadatum, foreign_key: 'ghost_user_id', dependent: :destroy

  validates :name, :profile_picture_url, presence: true
  validates :facebook_uid, uniqueness: { if: ->(user) { user.facebook_uid.present? } }
  validates :twitter_uid, uniqueness: { if: ->(user) { user.twitter_uid.present? } }
  validates :intercom_status, inclusion: { in: [INTERCOM_STATUS_UNREGISTERED, INTERCOM_STATUS_ACTIVE, INTERCOM_STATUS_DELETED] }

  scope :ordered, -> { reorder('name ASC') }
  scope :recently_followed_first, -> { includes(:followed_followings).order('followings.created_at desc') }
  scope :recent_followers_first, -> { includes(:follower_users).order('followings.created_at desc') }

  scope :country_available, -> { where.not(last_country: nil) }
  scope :users_by_country, -> (country_code) { country_available.where('lower(last_country) = ?', country_code.downcase) }
  scope :location_available, -> { where('"last_lat" IS NOT NULL AND "last_lng" IS NOT NULL') }
  scope :users_in_range, -> (lat, lng, radius) { location_available.where('lat_long_distance(last_lat, last_lng, ?, ?) < ?', lat, lng, radius) }

  scope :uploaded_to_topic, ->(topic) { where(id: Opinion.select(:user_id).where(topic: topic).distinct) }
  scope :asked_in_topic, ->(topic) { where(id: OpinionRequest.select(:requested_user_id).where(topic: topic).distinct) }
  scope :order_by_ask_count_desc, ->(topic) { order("(SELECT count(*) FROM opinion_requests WHERE opinion_requests.topic_id = #{topic.id} AND opinion_requests.requested_user_id = users.id) desc") }

  scope :search_by_query, ->(query) { where("(lower(unaccent(name)) LIKE lower(unaccent(?))) OR (lower(unaccent(twitter_handle)) LIKE lower(unaccent(?))) OR (lower(unaccent(handle)) LIKE lower(unaccent(?)))", "%#{query}%", "%#{query}%", "%#{query}%") }

  after_create :subscribe_to_notifications

  def self.find_by_twitter(handle)
    User.where('lower(twitter_handle) = ?', handle.downcase).first
  end

  def self.suggestions(current_user, count)
    pool_size = count * 3
    pool = []

    if current_user
      pool += current_user.followed_users.map { |u| u.followed_users }.flatten.uniq.select do |u|
        u != current_user && !current_user.followed_users.include?(u) && !u.guest
      end
    end

    if pool.size < pool_size
      remaining_count = pool_size - pool.size
      pool += order(follower_users_count: :desc).limit(remaining_count).select do |u|
        if current_user
          u != current_user && !current_user.followed_users.include?(u) && !u.guest
        else
          !u.guest
        end
      end
    end

    pool.uniq.sample(count)
  end

  def last_request_in_topic(topic)
    OpinionRequest.select(:created_at).where(requested_user: self, topic: topic).order(created_at: :desc).first.try(:created_at)
  end

  def asked_in_topic(topic)
    User.where(id: requested_opinion_requests.select(:requested_user_id).where(topic: topic))
  end

  def subscribe_to_notifications
    Notification::DEFAULT_NOTIFICATION_TYPES.each do |type|
      push_notification_registrations.create(notification_type: type)
    end
  end

  def locked_opinions
    opinions.where.not(locked_at: nil).order(locked_at: :desc)
  end

  def mark_opinion_watched(opinion)
    opinion_watches.create(opinion: opinion)
  end

  def received_opinion_request_from_user?(user, topic)
    received_opinion_requests.where(requester_user: user, topic: topic).exists?
  end

  def followed_by_user?(user)
    follower_users.exists?(user.id)
  end

  def request_opinion_of_user(user, topic)
    return false if self == user

    if requested_opinion_requests.exists?(requested_user: user, topic: topic)
      false
    else
      request = requested_opinion_requests.create(requested_user: user, topic: topic)
      ActionItem.create(actor: self, target: request, action_type: ActionItem::ACTION_ASK)

      true
    end
  end

  def follow_user(user)
    return if self == user
    return if followed_users.exists?(user.id)

    ActionItem.create(actor: self, target: user, action_type: ActionItem::ACTION_USER_FOLLOW)

    followed_users << user
  end

  def unfollow(record)
    followed_followings.where(followed: record).destroy_all
  end

  def follow_topic(topic)
    return if followed_topics.exists?(topic.id)

    ActionItem.create(actor: self, target: topic, action_type: ActionItem::ACTION_TOPIC_FOLLOW) unless topic.opinions_count.zero?

    followed_topics << topic
  end

  def logout_device(device_uid)
    session_tokens.where(device_uid: device_uid).destroy_all
  end

  def report_opinion(opinion, reason)
    offense_reports.create(reason: reason, opinion: opinion)
  end

  def register_for_push_notification(notification_type)
    push_notification_registrations.create(notification_type: notification_type)
  end

  def deregister_from_push_notification(notification_type)
    push_notification_registrations.where(notification_type: notification_type).destroy_all
  end

  def update_or_create_push_token(device_uid, push_token)
    records = user_devices.where(device_uid: device_uid)

    if records.count.zero?
      user_devices.create(device_uid: device_uid, push_notification_token: push_token)
    else
      records.update_all(push_notification_token: push_token)
    end
  end

  def registered_to_notification?(notification_type)
    push_notification_registrations.where(notification_type: notification_type).count > 0
  end

  def push_tokens
    user_devices.select(:push_notification_token).collect(&:push_notification_token)
  end

  def player_ids_list
    player_ids.select(:player_id).collect(&:player_id)
  end

  def update_last_location(lat:, lng:)
    geocoder = GeocoderHelper.new(lat, lng)

    update_values = { last_lat: lat, last_lng: lng }

    if geocoder.country_code
      update_values[:last_country] = geocoder.country_code.upcase
    else
      update_values[:last_country] = nil
    end

    update!(update_values)
  end

  def request_count_in_topic(topic)
    received_opinion_requests.where(topic: topic).count
  end

  def participating_topics
    profile_topic_id = Topic.find_by(profile_topic: true).try(:id)

    opinions.newest_first.most_reacted_first.map { |o| o.topic_id }.select { |id| id != profile_topic_id }.uniq
  end

  def asks_by_topic(current_user = self)
    requests = OpinionRequest.where(requested_user: self, fullfilled: false)
    grouped_requests = requests.group_by(&:topic_id).sort_by { |_k, v| v.count }.reverse

    grouped_requests.map { |rs| RequestsInTopic.new(rs[0], rs[1], current_user) }
  end

  def follows_user?(user)
    !followed_users.where(id: user.id).count.zero?
  end

  def intercom_id
    "#{id}-#{Rails.env}"
  end

  def seen_time_text
    TimeToText.new(seen_time).to_text
  end

  def update_stats
    attrs = {}
    attrs[:seen_time] = Opinion.select('sum(watch_time) as total_watch_time').where(user: self)[0].total_watch_time || 0
    attrs[:hosted_issues_count] = Topic.where(creator: self).where.not(opinions_count: 0).count
    attrs[:open_asks_count] = OpinionRequest.where(requested_user: self, fullfilled: false).count
    attrs[:responded_asks_count] = OpinionRequest.where(requested_user: self, fullfilled: true).count

    update(attrs)
  end

  def intercom_attributes
    update_stats

    {
        number_of_videos: opinions.count,
        hosted_issues: hosted_issues_count,
        video_watch_time: seen_time,
        sent_asks: OpinionRequest.where(requester_user: self).count,
        received_asks: OpinionRequest.where(requested_user: self).count
    }
  end
end
