# == Schema Information
#
# Table name: ghost_metadata
#
#  id              :integer          not null, primary key
#  creator_user_id :integer          indexed
#  ghost_user_id   :integer          indexed
#  topic_id        :integer          indexed
#  ask_time        :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class GhostMetadatum < ApplicationRecord
  belongs_to :creator_user, class_name: 'User'
  belongs_to :ghost_user, class_name: 'User'
  belongs_to :topic
end
