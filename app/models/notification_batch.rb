# frozen_string_literals: true
class NotificationBatch
  def initialize(notifications)
    @notifications = notifications.sort_by {|n| n.created_at}.reverse
  end

  def base_noti
    @notifications[0]
  end

  def created_at
    base_noti.created_at
  end

  def notification_type
    base_noti.notification_type
  end

  def target
    base_noti.target
  end

  def opinion_id
    base_noti.target.id if base_noti.target.is_a?(Opinion)
  end

  def notification_message
    if base_noti.is_a?(ReactionReceivedNotification)
      reaction_received_message(->(text) {text})
    elsif base_noti.is_a?(RequestReceivedNotification)
      request_received_message(->(text) {text})
    # elsif base_noti.is_a?(RequestAnsweredNotification)
    #   request_answered_message(->(text) {text})
    else
      base_noti.push_notification_message
    end
  end

  def notification_long_message
    if base_noti.is_a?(OpinionLockedNotification)
      base_noti.notification_long_text
    else
      notification_message
    end
  end

  def notification_read_text
    highlighter = lambda {|text| "<span style='color: #{base_noti.read_highlight_color}; font-family: SFUIText-Bold'>#{text}</span>"}

    if base_noti.is_a?(ReactionReceivedNotification)
      highlight(true, reaction_received_message(highlighter))
    elsif base_noti.is_a?(RequestReceivedNotification)
      highlight(true, request_received_message(highlighter))
    # elsif base_noti.is_a?(RequestAnsweredNotification)
    #   highlight(true, request_answered_message(highlighter))
    else
      base_noti.notification_read_text
    end
  end

  def notification_unread_text
    highlighter = lambda {|text| "<span style='color: #{Notification::UNREAD_HIGHLIGHT}; font-family: SFUIText-Bold'>#{text}</span>"}

    if base_noti.is_a?(ReactionReceivedNotification)
      highlight(false, reaction_received_message(highlighter))
    elsif base_noti.is_a?(RequestReceivedNotification)
      highlight(false, request_received_message(highlighter))
    # elsif base_noti.is_a?(RequestAnsweredNotification)
    #   highlight(false, request_answered_message(highlighter))
    else
      base_noti.notification_unread_text
    end
  end

  def read
    @notifications.all? do |notification|
      notification.read
    end
  end

  def opinion
    base_noti.target if base_noti.target.is_a?(Opinion)
  end

  def thumbnail
    base_noti.thumbnail
  end

  def facebook_friend
    # FacebookFriendJoinedNotification
    base_noti.target if base_noti.is_a?(FacebookFriendJoinedNotification)
  end

  def follower
    # UserFollowedNotification
    target if base_noti.is_a?(UserFollowedNotification)
  end

  def opinion_video_thumbnail_url
    # TranscodingDoneNotification
    target.video_thumbnail_url if base_noti.is_a?(TranscodingDoneNotification)
  end

  def opinioned_topic
    # TranscodingDoneNotification
    target.topic if base_noti.is_a?(TranscodingDoneNotification)
  end

  def reacting_users
    # ReactionReceivedNotification
    if base_noti.is_a?(ReactionReceivedNotification)
      @notifications.map { |notification| notification.subject.user }.uniq
    end
  end

  def reacted_topic
    # ReactionReceivedNotification
    target.topic if base_noti.is_a?(ReactionReceivedNotification)
  end

  def requester_users
    # RequestReceivedNotification
    if base_noti.is_a?(RequestReceivedNotification)
      @notifications.map { |notification| notification.subject.requester_user }.uniq
    end
  end

  def requested_topic
    # RequestReceivedNotification
    target if base_noti.is_a?(RequestReceivedNotification)
  end

  def ask_data
    # RequestReceivedNotification
    return unless base_noti.is_a?(RequestReceivedNotification)

    user = base_noti.user
    requests = @notifications.map { |notification| notification.subject }
    topic_id = base_noti.subject.topic_id
    RequestsInTopic.new(topic_id, requests, user)
  end

  def requested_user
    # RequestAnsweredNotification
    base_noti.target if base_noti.is_a?(RequestAnsweredNotification)
  end

  # def requested_users
  #   # RequestAnsweredNotification
  #   if base_noti.is_a?(RequestAnsweredNotification)
  #     @notifications.map { |notification| notification.target }.uniq
  #   end
  # end

  def interesting_topic
    # RequestAnsweredNotification
    base_noti.subject.topic if base_noti.is_a?(RequestAnsweredNotification)
  end

  def locked_opinion
    # OpinionLockedNotification
    target if base_noti.is_a?(OpinionLockedNotification)
  end

  def hosted_topic
    # HostedTopicNewVideoNotification
    target.topic if base_noti.is_a?(HostedTopicNewVideoNotification)
  end

  def uploader_user
    # HostedTopicNewVideoNotification
    target.user if base_noti.is_a?(HostedTopicNewVideoNotification)
  end

  def reaction_received_message(highlighter)
    case @notifications.size
      when 1
        "#{highlighter.call(reacting_users[0].name)} reacted to your video about #{highlighter.call(reacted_topic.title)}"
      when 2
        "#{highlighter.call(reacting_users[0].name)} and #{highlighter.call(reacting_users[1].name)} reacted to your video about #{highlighter.call(reacted_topic.title)}"
      else
        "#{highlighter.call(reacting_users[0].name)}, #{highlighter.call(reacting_users[1].name)} and #{highlighter.call(@notifications.size - 2)} #{highlighter.call(others_text)} reacted to your video about #{highlighter.call(reacted_topic.title)}"
    end
  end

  def request_received_message(highlighter)
    case @notifications.size
      when 1
        "#{highlighter.call(requester_users[0].name)} asked you about #{highlighter.call(requested_topic.title)}"
      when 2
        "#{highlighter.call(requester_users[0].name)} and #{highlighter.call(requester_users[1].name)} asked you about #{highlighter.call(requested_topic.title)}"
      else
        "#{highlighter.call(requester_users[0].name)}, #{highlighter.call(requester_users[1].name)} and #{highlighter.call(@notifications.size - 2)} #{highlighter.call(others_text)} asked you about #{highlighter.call(requested_topic.title)}"
    end
  end

  def request_answered_message(highlighter)
    case @notifications.size
      when 1
        "#{highlighter.call(requested_users[0].name)} has responded about #{highlighter.call(interesting_topic.title)}"
      when 2
        "#{highlighter.call(requested_users[0].name)} and #{highlighter.call(requested_users[1].name)} have responded about #{highlighter.call(interesting_topic.title)}"
      else
        "#{highlighter.call(requested_users[0].name)}, #{highlighter.call(requested_users[1].name)} and #{highlighter.call(@notifications.size - 2)} #{highlighter.call(others_text)} have responded about #{highlighter.call(interesting_topic.title)}"
    end
  end

  def others_text
    if @notifications.size - 2 == 1
      'other'
    else
      'others'
    end
  end

  def highlight(read_highlight, text)
    base_color = if read_highlight
                   Notification::READ_BASE
                 else
                   Notification::UNREAD_BASE
                 end
    "<span style='color: #{base_color}; font-family: SFUIText-Regular; font-size: larger'>#{text}</span>"
  end
end