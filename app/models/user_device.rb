# == Schema Information
#
# Table name: user_devices
#
#  id                      :integer          not null, primary key
#  user_id                 :integer          indexed
#  device_uid              :string
#  push_notification_token :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class UserDevice < ApplicationRecord
  belongs_to :user
end
