# frozen_string_literal: true
# == Schema Information
#
# Table name: topic_modules
#
#  id                 :integer          not null, primary key
#  type               :string           not null
#  topic_dashboard_id :integer          not null, indexed
#  position           :integer          not null
#  title              :string           not null
#  display_type       :string           not null
#  category_id        :integer          indexed
#  banner_id          :integer          indexed
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  title_bg_color     :string
#  title_color        :string
#  recents            :boolean
#  topic_limit        :integer          default(5)
#

class TopicModule < ApplicationRecord
  self.inheritance_column = nil # maybe later it will come in handy

  CATEGORY_TYPE = 'category'
  HASHTAG_TYPE = 'hashtag'
  BANNER_TYPE = 'banner'
  RECENTS_TYPE = 'recents'
  DISPLAY_TYPES = ['small_carusel', 'large_carusel', 'single_view', BANNER_TYPE].freeze
  TYPES = [CATEGORY_TYPE, HASHTAG_TYPE, BANNER_TYPE, RECENTS_TYPE].freeze

  belongs_to :topic_dashboard, required: true
  # start indexing with zero, cause that's how activeadmin sorts stuff :/
  acts_as_list scope: :topic_dashboard, top_of_list: 0

  belongs_to :category
  has_many :category_topics, through: :category, source: :topics, class_name: 'Topic'

  belongs_to :banner
  has_one :banner_dashboard, through: :banner, source: :topic_dashboard, class_name: 'TopicDashboard'

  has_many :taggings, as: :taggable, inverse_of: :taggable, dependent: :destroy
  has_many :hashtags, through: :taggings, source: :hashtag
  has_many :hashtag_topics, -> { distinct }, through: :hashtags, source: :topics, class_name: 'Topic'

  has_many :module_locales, dependent: :destroy
  has_many :topic_locales, through: :module_locales

  validates :title, presence: true
  validates :display_type, inclusion: {in: DISPLAY_TYPES, unless: :banner_type?}
  validates :type, inclusion: {in: TYPES}

  before_validation :set_type
  before_validation :set_banner_display_type

  TYPES.each do |type|
    define_method "#{type}_type?" do
      self.type == type
    end
  end

  def hashtag_topics_newest_first
    hashtag_topics.contains_video.sort_by { |a| a.newest_opinion_upload_time }.reverse
  end

  def category_topics_newest_first
    category_topics.contains_video.sort_by { |a| a.newest_opinion_upload_time }.reverse
  end

  def recent_topics_newest_first
    Topic.contains_video.order_by_last_opinion.limit(topic_limit)
  end

  def available_at?(country_code, latitude, longitude)
      topic_locales.empty? || topic_locales.any? { |tl| tl.applies?(country_code, latitude, longitude) }
  end

  private

  def set_type
    self.type = if recents
                  RECENTS_TYPE
                elsif banner then
                  BANNER_TYPE
                elsif hashtag_ids.count.positive? then
                  HASHTAG_TYPE
                elsif category then
                  CATEGORY_TYPE
                end
  end

  def set_banner_display_type
    self.display_type = BANNER_TYPE if banner_type?
  end
end
