# frozen_string_literal: true
# == Schema Information
#
# Table name: notifications
#
#  id                :integer          not null, primary key
#  type              :string           not null
#  notification_type :string           not null
#  user_id           :integer          not null, indexed
#  subject_id        :integer          not null
#  subject_type      :string           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  read              :boolean          default(FALSE), not null
#

class FollowedOpinionUploadedNotification < Notification
  before_validation :set_notification_type

  def set_notification_type
    self.notification_type = FOLLOWED_OPINION_UPLOADED
  end

  def push_notification_message
    "#{uploader_user.name} uploaded a new opinion in the thread #{topic.title}"
  end

  def highlighted_text(highlighter)
    "#{highlighter.call(uploader_user.name)} uploaded a new opinion in the thread #{highlighter.call(topic.title)}"
  end

  def thumbnail
    uploader_user.profile_picture_url
  end

  def read_highlight_color
    READ_HIGHLIGHT_GREEN
  end

  def push_opinion_id
    subject.id
  end

  def push_opinion
    subject
  end

  delegate :user, to: :subject, prefix: :uploader
  delegate :topic, to: :subject
end
