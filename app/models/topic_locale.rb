# frozen_string_literal: true
# == Schema Information
#
# Table name: topic_locales
#
#  id           :integer          not null, primary key
#  name         :string           not null
#  country_code :string
#  latitude     :decimal(, )
#  longitude    :decimal(, )
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  radius       :decimal(, )
#

class TopicLocale < ApplicationRecord
  has_many :topics

  validates :name, presence: true, uniqueness: true

  scope :country_locales, -> { where.not(country_code: nil) }
  scope :locales_by_country, -> (country_code) { country_locales.where('lower(country_code) = ?', country_code.downcase) }

  scope :location_locales, -> { where('"latitude" IS NOT NULL AND "longitude" IS NOT NULL AND "radius" IS NOT NULL') }
  scope :locales_by_lat_long, -> (lat, lng) { location_locales.where('lat_long_distance(latitude, longitude, ?, ?) < radius', lat, lng) }

  def distance_from(lat, lng)
    return unless location_mode?

    r = 6371
    dlat = (lat - latitude) * Math::PI / 180
    dlon = (lng - longitude) * Math::PI / 180
    a = Math::sin(dlat / 2) * Math::sin(dlat / 2) + Math::cos(latitude * Math::PI / 180) * Math::cos(lat * Math::PI / 180) * Math::sin(dlon / 2) * Math.sin(dlon / 2)
    c = Math::atan2(Math::sqrt(a), Math::sqrt(1 - a))

    r * c
  end

  def location_mode?
    latitude != nil && longitude != nil && radius != nil
  end

  def applies?(country_code, latitude, longitude)
    if location_mode?
      if latitude.nil? || longitude.nil?
        false
      else
        distance_from(latitude, longitude) < radius
      end
    else
      self.country_code.downcase == country_code&.downcase
    end
  end
end
