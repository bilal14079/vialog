# frozen_string_literal: true
# == Schema Information
#
# Table name: topics
#
#  id                   :integer          not null, primary key
#  title                :string           not null, indexed
#  category_id          :integer          indexed
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  opinions_count       :integer          default(0), not null
#  thumbnail_url        :string
#  countries            :string
#  password             :string
#  total_reaction_count :integer          default(0), not null
#  topic_locale_id      :integer          indexed
#  watch_time           :integer          default(0), not null
#  keywords             :string
#  profile_topic        :boolean          default(FALSE)
#  creator_id           :integer          indexed
#  clap_count           :integer
#  follower_count       :integer
#  last_opinion_time    :datetime
#

require 'digest/md5'

class Topic < ApplicationRecord
  include PgSearch
  pg_search_scope :search_by_title_and_keywords, against: [:title, :keywords], using: { tsearch: { prefix: true } }

  belongs_to :category, required: true
  belongs_to :topic_locale, required: false
  belongs_to :creator, class_name: 'User', required: false
  has_many :taggings, as: :taggable, inverse_of: :taggable, dependent: :destroy
  has_many :hashtags, through: :taggings, source: :hashtag

  has_many :followings, as: :followed, dependent: :destroy
  has_many :follower_users, through: :followings, source: :follower, class_name: 'User'

  has_many :opinions, -> { where(processed: true) }, dependent: :destroy

  has_many :opinion_requests, -> { unfullfilled }, dependent: :destroy
  has_many :users_wanted_for_opinion,
           -> { distinct },
           through: :opinion_requests,
           source: :requested_user,
           class_name: 'User'
  # count cannot be called on this because of the inner select
  has_many :users_wanted_for_opinion_ordered_by_request_count,
           (lambda do
             select('users.*, count(opinion_requests.requester_user_id) as opinion_requests_count')
                 .group('users.id')
                 .order('opinion_requests_count desc')
           end),
           through: :opinion_requests,
           source: :requested_user,
           class_name: 'User'

  has_many :topic_opens, dependent: :destroy

  validates :title, presence: true, uniqueness: true

  scope :ordered, -> { reorder('title ASC') }
  scope :not_profile_topic, -> { where(profile_topic: false) }
  scope :recently_followed_first, -> { includes(:followings).order('followings.created_at desc') }
  scope :last_uploaded_to_first, ->(limit) { find_by_sql("SELECT * FROM topics WHERE opinions_count > 0 AND (password = '') IS NOT FALSE ORDER BY (SELECT created_at FROM opinions WHERE topic_id = topics.id AND locked_at IS NULL ORDER BY created_at desc LIMIT 1) DESC LIMIT ?", limit) }
  scope :user_topics_order_by_last_updated, ->(user) { find_by_sql("SELECT * FROM topics WHERE creator_id = #{user.id} ORDER BY (SELECT created_at FROM opinions WHERE topic_id = topics.id AND locked_at IS NULL ORDER BY created_at desc LIMIT 1) DESC") }
  scope :contains_video, -> { where("(SELECT count(id) FROM opinions WHERE topic_id = topics.id AND (password = '') IS NOT FALSE AND locked_at IS NULL) > 0") }

  scope :search_by_query, -> (query) { where("(lower(unaccent(title)) LIKE lower(unaccent(?))) OR (lower(unaccent(keywords)) LIKE lower(unaccent(?))) OR (lower(unaccent((SELECT name FROM users WHERE users.id = topics.creator_id LIMIT 1))) LIKE lower(unaccent(?)))", "%#{query}%", "%#{query}%", "%#{query}%") }
  scope :order_by_last_opinion, ->() { order("(SELECT created_at FROM opinions WHERE opinions.topic_id = topics.id AND locked_at IS NULL ORDER BY opinions.created_at DESC LIMIT 1) DESC") }

  def self.suggestions(current_user, count)
    pool_size = count * 3
    pool = []

    if current_user
      pool += current_user.followed_users.map { |u| u.followed_topics.contains_video }.flatten.uniq.select do |t|
        !current_user.followed_topics.include?(t) && !t.password_hash
      end
    end

    if pool.size < pool_size
      remaining_count = pool_size - pool.size
      pool += contains_video.order(opinions_count: :desc).limit(remaining_count).select do |t|
        if current_user
          !current_user.followed_topics.include?(t) && !t.password_hash
        else
          !t.password_hash
        end
      end
    end

    pool.uniq.sample(count)
  end

  def unlocked_opinions(user_id)
    opinions.unlocked.or(opinions.where(user_id: user_id))
  end

  def host_opinion
    opinions.order(:created_at).first
  end

  def uploaders
    User.where(id: opinions.select(:user_id).distinct)
  end

  def new_opinion_count(user)
    last_open = topic_opens.find_by(user: user)

    if last_open
      opinions.where('created_at > ?', last_open.time).where.not(user: user).count
    else
      0
    end
  end

  def opinions_by_user(user)
    opinions.where(user: user)
  end

  def opinion_requests_by_user(user)
    opinion_requests.where(requester_user: user)
  end

  def featured_opinions
    opinions.where('feature_factor > ?', 0)
  end

  def non_featured_opinions
    opinions.where(feature_factor: 0)
  end

  def refresh_thumbnail_url
    first_opinion = opinions.unlocked.default_ordered(nil, nil, self, nil)
                        .first

    update(thumbnail_url: first_opinion.try(:video_thumbnail_url))
  end

  def followed_by_user?(user)
    follower_users.exists?(user.id)
  end

  def reaction_count
    opinions.map { |opinion| opinion.reaction_count }.inject(0) { |sum, x| sum + x }
  end

  %w(love haha wow sad angry).each do |type|
    mn = "reaction_#{type}_count"

    define_method mn do
      opinions.map { |opinion| opinion.send(mn) }.inject(0) { |sum, x| sum + x }
    end
  end

  def allowed_in_country(country_code)
    country_not_restricted? || country_code == nil || allowed_countries.include?(country_code.downcase)
  end

  def allowed_countries
    @allowed_countries ||= countries.split(',').map { |s| s.strip.downcase }
  end

  def country_not_restricted?
    countries == nil || allowed_countries.count == 0
  end

  def password_hash
    Digest::MD5.hexdigest(password) if password_protected?
  end

  def password_protected?
    password && !password.empty?
  end

  def update_calculated_values
    update_values = {}

    update_values[:total_reaction_count] = reaction_count
    update_values[:clap_count] = clap_count_live
    update_values[:watch_time] = watch_time_live
    update_values[:follower_count] = follower_users.count
    update_values[:last_opinion_time] = newest_opinion_upload_time

    if update_values.size > 0
      update(update_values)
    end
  end

  def newest_opinion_upload_time
    newest_opinion = opinions.unlocked.newest_first.first

    if newest_opinion
      newest_opinion.created_at
    else
      created_at
    end
  end

  def watch_time_live
    opinions.unlocked.sum(:watch_time)
  end

  def update_watch_time
    new_watch_time = watch_time_live
    update(watch_time: new_watch_time)
  end

  def watch_time_text
    if watch_time < 60
      "#{watch_time} s"
    elsif watch_time < 60 * 60
      "#{watch_time / 60} m #{watch_time % 60} s"
    elsif watch_time < 24 * 60 * 60
      hour = watch_time / (60 * 60)
      min = (watch_time - hour * 60 * 60) / 60
      sec = (watch_time - hour * 60 * 60) % 60
      "#{hour} h #{min} m #{sec} s"
    else
      day = watch_time / (24 * 60 * 60)
      hour = (watch_time - day * 24 * 60 * 60) / (60 * 60)
      min = (watch_time - day * 24 * 60 * 60 - hour * 60 * 60) / 60
      sec = (watch_time - day * 24 * 60 * 60 - hour * 60 * 60) % 60
      "#{day} d #{hour} h #{min} m #{sec} s"
    end
  end

  def keyword_list
    if keywords
      keywords.split(/[.,\W]/).select { |w| w.length > 0 }.map { |w| w.downcase }
    else
      []
    end
  end

  def generate_keywords(lang = 'en')
    words = opinions.flat_map do |o|
      t = o.opinion_translations.find_by(language: lang)

      if t
        t.words_list
      else
        []
      end
    end

    filter_words = Word.where(language: lang).map(&:word)
    filtered_words = (words - filter_words)
    threshold = [opinions.count / 10, 1].max

    keywords = filtered_words.each_with_object(Hash.new(0)) { |w, c| c[w] += 1 }
    keywords = keywords.sort_by { |_k, v| v }.reverse
    keywords = keywords.select { |kw| kw[1] >= threshold }

    keywords
  end

  def keywords_hash
    saved_keywords = keyword_list.uniq
    generated_keywords = generate_keywords

    { generated: generated_keywords, constant: saved_keywords - generated_keywords }
  end

  def clap_count_live
    Clap.where(opinion: opinions.unlocked).sum(:duration)
  end
end
