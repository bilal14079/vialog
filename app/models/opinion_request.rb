# frozen_string_literal: true
# == Schema Information
#
# Table name: opinion_requests
#
#  id                :integer          not null, primary key
#  requester_user_id :integer          not null, indexed, indexed => [requested_user_id, topic_id]
#  requested_user_id :integer          not null, indexed, indexed => [requester_user_id, topic_id]
#  topic_id          :integer          not null, indexed, indexed => [requester_user_id, requested_user_id]
#  opinion_id        :integer          indexed
#  fullfilled        :boolean          default(FALSE)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class OpinionRequest < ApplicationRecord
  after_create :notify_about_request_received
  define_model_callbacks :fullfilled, only: :after
  after_fullfilled :notify_about_request_answered

  belongs_to :requester_user, class_name: 'User', required: true
  belongs_to :requested_user, class_name: 'User', required: true
  belongs_to :topic, required: true
  belongs_to :opinion

  has_one :request_received_notification, as: :subject, dependent: :destroy
  has_one :request_answered_notification, as: :subject, dependent: :destroy

  validates :topic_id, uniqueness: { scope: [:requester_user_id, :requested_user_id] }

  scope :unfullfilled, -> { where(fullfilled: false) }

  def mark_as_fullfilled(opinion)
    run_callbacks :fullfilled do
      update(opinion: opinion, fullfilled: true)
    end
  end

  def notify_about_request_received
    create_request_received_notification(user: requested_user)
  end

  def notify_about_request_answered
    create_request_answered_notification(user: requester_user)
  end
end
