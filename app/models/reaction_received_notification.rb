# frozen_string_literal: true
# == Schema Information
#
# Table name: notifications
#
#  id                :integer          not null, primary key
#  type              :string           not null
#  notification_type :string           not null
#  user_id           :integer          not null, indexed
#  subject_id        :integer          not null
#  subject_type      :string           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  read              :boolean          default(FALSE), not null
#

class ReactionReceivedNotification < Notification
  before_validation :set_notification_type

  def set_notification_type
    self.notification_type = REACTION_RECEIVED
  end

  def push_notification_message
    "#{reacting_user.name} reacted to your video about #{reacted_topic.title}"
  end

  def highlighted_text(highlighter)
    "#{highlighter.call(reacting_user.name)} reacted to your video about #{highlighter.call(reacted_topic.title)}"
  end

  def thumbnail
    opinion.video_thumbnail_url
  end

  def read_highlight_color
    READ_HIGHLIGHT_PURPLE
  end

  def target
    opinion
  end

  def push_topic_id
    reacted_topic.id
  end

  delegate :user, to: :subject, prefix: :reacting
  delegate :opinion, to: :subject
  delegate :topic, to: :opinion, prefix: :reacted
end
