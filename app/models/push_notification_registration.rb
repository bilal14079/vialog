# frozen_string_literal: true
# == Schema Information
#
# Table name: push_notification_registrations
#
#  id                :integer          not null, primary key
#  user_id           :integer          not null, indexed
#  notification_type :string           not null, indexed
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class PushNotificationRegistration < ApplicationRecord
  belongs_to :user, required: true

  validates :user_id, presence: true, uniqueness: { scope: [:notification_type] }
  validates :notification_type, inclusion: { in: Notification::NOTIFICATION_TYPES }
end
