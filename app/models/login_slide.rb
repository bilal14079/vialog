# == Schema Information
#
# Table name: login_slides
#
#  id                 :integer          not null, primary key
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  text               :text
#  dot_color          :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  animated           :boolean
#  looped             :boolean
#  display_order      :integer
#

class LoginSlide < ApplicationRecord
  has_attached_file :image
  validates_attachment_content_type :image, content_type: %w[image/png image/gif]

  def html_text
    text.gsub(/\[color (#[0-9a-fA-F]{6})\]/, '<font color=\'\1\'>').gsub('[/color]', '</font>')
  end
end
