# frozen_string_literal: true
# == Schema Information
#
# Table name: words
#
#  id         :integer          not null, primary key
#  language   :string
#  word       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  profanity  :boolean          default(TRUE)
#

class Word < ApplicationRecord
  scope :bad_words, -> () {where(profanity: true)}
end
