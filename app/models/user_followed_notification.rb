# frozen_string_literal: true
# == Schema Information
#
# Table name: notifications
#
#  id                :integer          not null, primary key
#  type              :string           not null
#  notification_type :string           not null
#  user_id           :integer          not null, indexed
#  subject_id        :integer          not null
#  subject_type      :string           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  read              :boolean          default(FALSE), not null
#

class UserFollowedNotification < Notification
  before_validation :set_notification_type

  def set_notification_type
    self.notification_type = USER_FOLLOWED
  end

  def push_notification_message
    "#{follower.name} started following you"
  end

  def highlighted_text(highlighter)
    "#{highlighter.call(follower.name)} started following you"
  end

  def thumbnail
    follower.profile_picture_url
  end

  def read_highlight_color
    READ_HIGHLIGHT_GREEN
  end

  def target
    follower
  end

  delegate :follower, to: :subject
end
