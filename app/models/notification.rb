# frozen_string_literal: true
# == Schema Information
#
# Table name: notifications
#
#  id                :integer          not null, primary key
#  type              :string           not null
#  notification_type :string           not null
#  user_id           :integer          not null, indexed
#  subject_id        :integer          not null
#  subject_type      :string           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  read              :boolean          default(FALSE), not null
#

require 'one_signal_notification'

class Notification < ApplicationRecord
  REQUEST_ANSWERED = 'request_answered'
  REQUEST_RECEIVED = 'request_received'
  REACTION_RECEIVED = 'reaction_received'
  FACEBOOK_FRIEND_JOINED = 'facebook_friend_joined'
  USER_FOLLOWED = 'user_followed'
  TRANSCODING_DONE = 'transcoding_done'
  FOLLOWED_OPINION_UPLOADED = 'followed_opinion_uploaded'
  FOLLOWED_OPINION_TOPIC_UPLOADED = 'followed_opinion_topic_uploaded'
  OPINION_LOCKED = 'opinion_locked'
  HOSTED_TOPIC_NEW_VIDEO = 'hosted_topic_new_video'

  DEFAULT_NOTIFICATION_TYPES = [REQUEST_ANSWERED, REQUEST_RECEIVED, FACEBOOK_FRIEND_JOINED, USER_FOLLOWED, HOSTED_TOPIC_NEW_VIDEO].freeze

  AUTOMATIC_NOTIFICATION_TYPES = [OPINION_LOCKED].freeze

  NOTIFICATION_TYPES = [REQUEST_RECEIVED, REQUEST_ANSWERED,
                        REACTION_RECEIVED, FACEBOOK_FRIEND_JOINED,
                        USER_FOLLOWED, TRANSCODING_DONE, HOSTED_TOPIC_NEW_VIDEO,
                        FOLLOWED_OPINION_UPLOADED, FOLLOWED_OPINION_TOPIC_UPLOADED, OPINION_LOCKED].freeze

  DISABLED_NOTIFICATION_TYPES = [FOLLOWED_OPINION_UPLOADED, FOLLOWED_OPINION_TOPIC_UPLOADED, REACTION_RECEIVED].freeze

  READ_BASE = '#ffffff'
  READ_HIGHLIGHT_YELLOW = '#e9c900'
  READ_HIGHLIGHT_RED = '#e3552e'
  READ_HIGHLIGHT_PURPLE = '#317587'
  READ_HIGHLIGHT_GREEN = '#00f0cf'
  READ_HIGHLIGHT_GREY = '#656565'

  UNREAD_BASE = '#000000'
  UNREAD_HIGHLIGHT = '#ffffff'

  after_create :send_push_notifications

  belongs_to :user, required: true
  belongs_to :subject, polymorphic: true, required: true

  validates :notification_type, inclusion: { in: NOTIFICATION_TYPES }

  scope :ordered, -> { order('created_at DESC') }
  scope :by_notification_type, ->(type) { where(notification_type: type) if type }
  scope :active_types, -> { where.not(notification_type: DISABLED_NOTIFICATION_TYPES) }
  scope :unread, -> { where(read: false) }

  def push_notification_message
    'This is a general notification'
  end

  def notification_long_text
    push_notification_message
  end

  def notification_read_text
    highlight_text(true)
  end

  def notification_unread_text
    highlight_text(false)
  end

  def highlight_text(read)
    if read
      base_color = READ_BASE
      highlight_color = read_highlight_color
    else
      base_color = UNREAD_BASE
      highlight_color = UNREAD_HIGHLIGHT
    end

    highlighter = lambda { |text| "<span style='color: #{highlight_color}; font-family: SFUIText-Bold'>#{text}</span>" }

    "<span style='color: #{base_color}; font-family: SFUIText-Regular; font-size: larger'>#{highlighted_text(highlighter)}</span>"
  end

  def highlighted_text(highlighter)
    notification_long_text
  end

  def read_highlight_color
    READ_HIGHLIGHT_GREEN
  end

  def thumbnail
  end

  def target
    subject
  end

  def push_topic_id
  end

  def push_opinion_id
  end

  def push_user_id
  end

  def push_opinion
  end

  def push_notification_tokens
    if AUTOMATIC_NOTIFICATION_TYPES.include?(notification_type) || user.registered_to_notification?(notification_type)
      user.push_tokens
    else
      []
    end
  end

  def player_ids
    if AUTOMATIC_NOTIFICATION_TYPES.include?(notification_type) || user.registered_to_notification?(notification_type)
      user.player_ids_list
    else
      []
    end
  end

  def push_notification_options(badge)
    { sound: true, # play the default sound that comes with the app
      alert: push_notification_message,
      type: notification_type,
      badge: badge.to_s,
      target_id: target.id,
      target_class: target.class.to_s,
      topic_id: push_topic_id.to_s,
      opinion_id: push_opinion_id.to_s,
      user_id: push_user_id.to_s }.tap do |options|
      options[:quality] = push_opinion.quality if push_opinion
    end
  end

  def send_push_notifications
    return unless ENV['PUSH_NOTIFICATIONS_ENABLED']
    return if DISABLED_NOTIFICATION_TYPES.include?(notification_type)
    return if player_ids.empty?

    unread_count = user.notifications.active_types.unread.count

    OneSignalNotification.new(player_ids, push_notification_message, push_data(unread_count)).send

    # push_notification_tokens.each { |token| APN.notify_async(token, push_notification_options(unread_count)) }
  end

  def push_data(badge)
    {
        type: notification_type,
        badge: badge,
        target_id: target.id,
        target_class: target.class.to_s,
        topic_id: push_topic_id.to_s,
        opinion_id: push_opinion_id.to_s,
        user_id: push_user_id.to_s
    }.tap do |options|
      options[:quality] = push_opinion.quality if push_opinion
    end
  end
end
