# == Schema Information
#
# Table name: action_items
#
#  id          :integer          not null, primary key
#  actor_id    :integer          not null, indexed
#  target_id   :integer          not null
#  target_type :string           not null
#  action_type :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class ActionItem < ApplicationRecord
  belongs_to :actor, class_name: 'User', required: true
  belongs_to :target, polymorphic: true, required: true

  ACTION_USER_FOLLOW = 'user_follow'.freeze
  ACTION_TOPIC_FOLLOW = 'topic_follow'.freeze
  ACTION_ASK = 'ask'.freeze
  ACTION_GHOST_ADD = 'ghost_add'.freeze
  ACTION_VIDEO_UPLOAD = 'video_upload'.freeze

  VALID_TYPES = [ACTION_USER_FOLLOW, ACTION_TOPIC_FOLLOW, ACTION_ASK, ACTION_GHOST_ADD, ACTION_VIDEO_UPLOAD].freeze

  def asked_current_user(current_user)
    return false if current_user.nil?

    action_type == ACTION_ASK && (target.try(:requested_user) == current_user)
  end

  def follows_current_user?(current_user)
    return false if current_user.nil?

    action_type == ACTION_USER_FOLLOW && (target == current_user)
  end
end
