# frozen_string_literal: true
# == Schema Information
#
# Table name: offense_reports
#
#  id         :integer          not null, primary key
#  reason     :string           not null, indexed => [opinion_id, user_id]
#  opinion_id :integer          not null, indexed, indexed => [user_id, reason]
#  user_id    :integer          not null, indexed => [opinion_id, reason], indexed
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class OffenseReport < ApplicationRecord
  belongs_to :opinion, required: true
  counter_culture :opinion

  belongs_to :user, required: true

  validates :reason, presence: true
  validates :opinion_id, uniqueness: { scope: [:user_id, :reason] }
end
