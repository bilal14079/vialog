# frozen_string_literal: true
# == Schema Information
#
# Table name: followings
#
#  id            :integer          not null, primary key
#  follower_id   :integer          not null, indexed => [followed_id, followed_type]
#  followed_id   :integer          not null, indexed => [follower_id, followed_type]
#  followed_type :string           not null, indexed => [follower_id, followed_id]
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Following < ApplicationRecord
  belongs_to :follower, class_name: 'User', required: true
  counter_culture :follower,
                  column_name: (lambda do |following|
                    case following.followed_type
                    when 'Topic' then 'followed_topics_count'
                    when 'User' then 'followed_users_count'
                    end
                  end),
                  column_names: {
                    ['followings.followed_type = ?', 'Topic'] => 'followed_topics_count',
                    ['followings.followed_type = ?', 'User'] => 'followed_users_count'
                  }

  belongs_to :followed, polymorphic: true, required: true
  counter_culture :followed,
                  only: [[:user]],
                  column_name: 'follower_users_count'

  has_one :user_followed_notification, as: :subject, dependent: :destroy
  after_create :notify_about_reaction_received

  validates :follower_id, uniqueness: { scope: [:followed_id, :followed_type] }

  def notify_about_reaction_received
    return unless followed_type == 'User'

    create_user_followed_notification(user: followed)
  end
end
