# frozen_string_literal: true
class RequestsInTopic
  def initialize(topic_id, requests, current_user)
    @topic = Topic.find(topic_id)
    ordered_requests = if current_user
                         requests.sort do |req1, req2|
                           f1 = req1.requester_user.followed_by_user?(current_user)
                           f2 = req2.requester_user.followed_by_user?(current_user)

                           if f1 && f2
                             -(req1.created_at <=> req2.created_at)
                           elsif f1 && !f2
                             -1
                           elsif !f1 && f2
                             1
                           else
                             -(req1.created_at <=> req2.created_at)
                           end
                         end
                       else
                         requests
                       end
    @requesting_users = ordered_requests.map(&:requester_user)

    @requests_data = ordered_requests.map do |req|
      { user: req.requester_user, timestamp: req.created_at, last_opinion: @topic.opinions.where(user: req.requester_user).order(created_at: :desc).first }
    end
  end

  def topic
    @topic
  end

  def requesting_users
    @requesting_users
  end

  def requests_data
    @requests_data
  end

end