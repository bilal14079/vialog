# frozen_string_literal: true
# == Schema Information
#
# Table name: categories
#
#  id           :integer          not null, primary key
#  name         :string           not null, indexed
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  default_mark :boolean
#

class Category < ApplicationRecord
  has_many :topics, dependent: :destroy
  has_many :topic_modules, dependent: :destroy

  validates :name, presence: true, uniqueness: true
end
