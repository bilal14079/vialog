# frozen_string_literal: true
# == Schema Information
#
# Table name: opinion_translations
#
#  id             :integer          not null, primary key
#  opinion_id     :integer          indexed
#  text           :text
#  language       :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  bad_word_count :integer
#

class OpinionTranslation < ApplicationRecord
  LANGUAGES = [
      %w"Afrikaans af",
      %w"Albanian sq",
      %w"Arabic ar",
      %w"Armenian hy",
      %w"Azerbaijani az",
      %w"Basque eu",
      %w"Belarusian be",
      %w"Bengali bn",
      %w"Bosnian bs",
      %w"Bulgarian bg",
      %w"Burmese my",
      %w"Catalan ca",
      %w"Chechen ce",
      %w"Chinese zh",
      %w"Cornish kw",
      %w"Croatian hr",
      %w"Czech cs",
      %w"Danish da",
      %w"Dutch nl",
      %w"English en",
      %w"Esperanto eo",
      %w"Estonian et",
      %w"Faroese fo",
      %w"Finnish fi",
      %w"French fr",
      %w"Georgian ka",
      %w"German de",
      %w"Greek el",
      %w"Haitian ht",
      %w"Hebrew he",
      %w"Hindi hi",
      %w"Hungarian hu",
      %w"Indonesian id",
      %w"Irish ga",
      %w"Icelandic is",
      %w"Italian it",
      %w"Japanese ja",
      %w"Javanese jv",
      %w"Kazakh kk",
      %w"Kongo kg",
      %w"Korean ko",
      %w"Kurdish ku",
      %w"Latin la",
      %w"Luxembourgish lb",
      %w"Lithuanian lt",
      %w"Latvian lv",
      %w"Macedonian mk",
      %w"Malay ms",
      %w"Maltese mt",
      %w"Maori mi",
      %w"Mongolian mn",
      %w"Nepali ne",
      %w"Norwegian no",
      %w"Persian fa",
      %w"Polish pl",
      %w"Portuguese pt",
      %w"Quechua qu",
      %w"Romanian ro",
      %w"Russian ru",
      %w"Sanskrit sa",
      %w"Sardinian sc",
      %w"Samoan sm",
      %w"Serbian sr",
      %w"ScottishGaelic gd",
      %w"Slovak sk",
      %w"Slovene sl",
      %w"Somali so",
      %w"Spanish es",
      %w"Swedish sw",
      %w"Tamil ta",
      %w"Telugu te",
      %w"Thai th",
      %w"Tibetan bo",
      %w"Turkmen tk",
      %w"Tonga to",
      %w"Turkish tr",
      %w"Tahitian ty",
      %w"Ukrainian uk",
      %w"Urdu ur",
      %w"Uzbek uz",
      %w"Vietnamese vi",
      %w"Welsh cy",
      %w"Yiddish yi",
      %w"Zulu zu",
  ]

  def self.language_names
    LANGUAGES.map {|l| l[0]}
  end

  def self.language_codes
    LANGUAGES.map {|l| l[1]}
  end

  belongs_to :opinion

  def language_name
    LANGUAGES.select {|l| l[1] == language}.first[0]
  end

  def words_list
    text.split(/[.,\W]/).select {|w| w.length > 0}.map {|w| w.downcase}
  end

  def update_bad_word_count
    ctr = Word.bad_words.where(language: language).where('word IN (?)', words_list.uniq).count
    update(bad_word_count: ctr)
  end
end
