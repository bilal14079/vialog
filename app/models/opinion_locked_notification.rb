# frozen_string_literal: true
# == Schema Information
#
# Table name: notifications
#
#  id                :integer          not null, primary key
#  type              :string           not null
#  notification_type :string           not null
#  user_id           :integer          not null, indexed
#  subject_id        :integer          not null
#  subject_type      :string           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  read              :boolean          default(FALSE), not null
#


class OpinionLockedNotification < Notification
  before_validation :set_notification_type

  def set_notification_type
    self.notification_type = OPINION_LOCKED
  end

  def push_notification_message
    'Your video is locked. It will be deleted.'
  end

  def notification_long_text
    'Your video is locked. This means only you can see it. It will be deleted after 10 days. Please contact Support to find out more about locked videos.'
  end

  def push_opinion_id
    subject.id
  end
end
