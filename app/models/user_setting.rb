# == Schema Information
#
# Table name: user_settings
#
#  id         :integer          not null, primary key
#  user_id    :integer          indexed
#  key        :string
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UserSetting < ApplicationRecord
  belongs_to :user
end
