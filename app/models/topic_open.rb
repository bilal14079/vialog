# == Schema Information
#
# Table name: topic_opens
#
#  id         :integer          not null, primary key
#  topic_id   :integer          indexed
#  user_id    :integer          indexed
#  time       :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TopicOpen < ApplicationRecord
  belongs_to :topic
  belongs_to :user
end
