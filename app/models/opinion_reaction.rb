# frozen_string_literal: true
# == Schema Information
#
# Table name: opinion_reactions
#
#  id              :integer          not null, primary key
#  opinion_id      :integer          not null, indexed, indexed => [user_id, reaction_type, reaction_second]
#  user_id         :integer          not null, indexed, indexed => [opinion_id, reaction_type, reaction_second]
#  reaction_type   :string           not null, indexed => [opinion_id, user_id, reaction_second]
#  reaction_second :integer          not null, indexed => [opinion_id, user_id, reaction_type]
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class OpinionReaction < ApplicationRecord
  after_create :notify_about_reaction_received

  REACTION_TYPES = %w(love haha wow sad angry).freeze

  belongs_to :opinion, required: true
  counter_culture :opinion,
                  column_name: -> (reaction) { "reaction_#{reaction.reaction_type}_count" },
                  column_names: {
                    ['reactions.reaction_type = ?', 'love'] => 'reaction_love_count',
                    ['reactions.reaction_type = ?', 'haha'] => 'reaction_haha_count',
                    ['reactions.reaction_type = ?', 'wow'] => 'reaction_wow_count',
                    ['reactions.reaction_type = ?', 'sad'] => 'reaction_sad_count',
                    ['reactions.reaction_type = ?', 'angry'] => 'reaction_angry_count'
                  }

  belongs_to :user, required: true

  has_one :reaction_received_notification, as: :subject, dependent: :destroy

  validates :reaction_type, uniqueness: { scope: [:user_id, :opinion_id, :reaction_second] }
  validates :reaction_type, inclusion: { in: REACTION_TYPES }
  validates :reaction_second, numericality: { only_integer: true,
                                              greater_than_or_equal_to: 0,
                                              less_than: 60 }

  def notify_about_reaction_received
    create_reaction_received_notification(user: opinion.user)
  end
end
