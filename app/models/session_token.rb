# frozen_string_literal: true
# == Schema Information
#
# Table name: session_tokens
#
#  id         :integer          not null, primary key
#  provider   :string           not null, indexed => [challenge]
#  challenge  :string           indexed => [provider]
#  token      :string           not null, indexed
#  device_uid :string           not null
#  user_id    :integer          not null, indexed
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SessionToken < ApplicationRecord
  PROVIDER_FACEBOOK = 'facebook'
  PROVIDER_TWITTER = 'twitter'
  PROVIDERS = [PROVIDER_FACEBOOK, PROVIDER_TWITTER].freeze

  has_secure_token

  belongs_to :user, required: true

  validates :device_uid, presence: true
  validates :provider, inclusion: { in: PROVIDERS }
  validates :challenge, presence: true,
                        uniqueness: true,
                        if: ->(token) { token.provider == PROVIDER_FACEBOOK }
  validates :token, presence: true, on: :update
  validates :token, uniqueness: true, if: :token

  delegate :id, :name, :twitter_handle, :profile_picture_url, :email, to: :user, prefix: true

  def facebook?
    provider == PROVIDER_FACEBOOK
  end
end
