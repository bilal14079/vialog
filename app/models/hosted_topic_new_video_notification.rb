# frozen_string_literal: true
# == Schema Information
#
# Table name: notifications
#
#  id                :integer          not null, primary key
#  type              :string           not null
#  notification_type :string           not null
#  user_id           :integer          not null, indexed
#  subject_id        :integer          not null
#  subject_type      :string           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  read              :boolean          default(FALSE), not null
#


class HostedTopicNewVideoNotification < Notification
  before_validation :set_notification_type

  def set_notification_type
    self.notification_type = HOSTED_TOPIC_NEW_VIDEO
  end

  def push_notification_message
    "#{uploader_user.name} responded to your thread #{hosted_topic.title}"
  end

  def highlighted_text(highlighter)
    "#{highlighter.call(uploader_user.name)} responded to your thread #{highlighter.call(hosted_topic.title)}"
  end

  def read_highlight_color
    READ_HIGHLIGHT_YELLOW
  end

  def thumbnail
    subject.video_thumbnail_url
  end

  def target
    subject
  end

  def push_topic_id
    hosted_topic.id
  end

  def push_opinion_id
    subject.id
  end

  def push_user_id
    uploader_user.id
  end

  delegate :user, to: :subject, prefix: :uploader
  delegate :topic, to: :subject, prefix: :hosted
end
