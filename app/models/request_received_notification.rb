# frozen_string_literal: true
# == Schema Information
#
# Table name: notifications
#
#  id                :integer          not null, primary key
#  type              :string           not null
#  notification_type :string           not null
#  user_id           :integer          not null, indexed
#  subject_id        :integer          not null
#  subject_type      :string           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  read              :boolean          default(FALSE), not null
#

class RequestReceivedNotification < Notification
  before_validation :set_notification_type

  def set_notification_type
    self.notification_type = REQUEST_RECEIVED
  end

  def push_notification_message
    "#{requester_user.name} asked you about #{requested_topic.title}"
  end

  def highlighted_text(highlighter)
    "#{highlighter.call(requester_user.name)} asked you about #{highlighter.call(requested_topic.title)}"
  end

  def thumbnail
    requester_user.profile_picture_url
  end

  def read_highlight_color
    READ_HIGHLIGHT_RED
  end

  def push_topic_id
    requested_topic.id
  end

  def target
    requested_topic
  end

  delegate :topic, to: :subject, prefix: :requested
  delegate :requester_user, to: :subject
end
