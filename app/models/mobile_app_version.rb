# frozen_string_literal: true
# == Schema Information
#
# Table name: mobile_app_versions
#
#  id                 :integer          not null, primary key
#  platform           :string           not null, indexed => [version_identifier]
#  update_severity    :string           not null
#  version_identifier :string           not null, indexed => [platform]
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class MobileAppVersion < ApplicationRecord
  PLATFORMS = %w(ios android).freeze
  SOFT_SEVERITY = 'soft'
  HARD_SEVERITY = 'hard'
  UPDATE_SEVERITIES = [SOFT_SEVERITY, HARD_SEVERITY].freeze

  validates :version_identifier, presence: true,
                                 uniqueness: { scope: :platform }
  validates :platform, inclusion: { in: PLATFORMS }
  validates :update_severity, inclusion: { in: UPDATE_SEVERITIES }

  def self.latest_version_for_platform(platform)
    where(platform: platform).order('version_identifier DESC').take
  end
end
