# frozen_string_literal: true
# == Schema Information
#
# Table name: opinions
#
#  id                        :integer          not null, primary key
#  topic_id                  :integer          not null, indexed
#  user_id                   :integer          not null, indexed
#  feature_factor            :integer          default(0), not null
#  video_hls_url             :string
#  video_thumbnail_url       :string
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  offense_reports_count     :integer          default(0), not null
#  reaction_love_count       :integer          default(0), not null
#  reaction_haha_count       :integer          default(0), not null
#  reaction_wow_count        :integer          default(0), not null
#  reaction_sad_count        :integer          default(0), not null
#  reaction_angry_count      :integer          default(0), not null
#  video_file_name           :string
#  video_content_type        :string
#  video_file_size           :integer
#  video_updated_at          :datetime
#  elastic_transcoder_job_id :string           indexed
#  processed                 :boolean          default(FALSE), indexed
#  token                     :string           default(""), not null
#  duration                  :decimal(6, 2)    default(0.0)
#  lat                       :decimal(, )
#  lng                       :decimal(, )
#  locked_at                 :datetime
#  locked_days               :integer          default(-1), not null
#  total_reaction_count      :integer          default(0), not null
#  watch_time                :integer          default(0), not null
#  device_id                 :string
#  bad_word_count            :integer          default(-1)
#  quality                   :string           default("hd")
#  transcoding_round         :integer          default(0)
#  clap_count                :integer
#  cover                     :boolean          default(FALSE)
#

require 'opinion_transcoder'
require 'transcoded_video_deleter'
require 'geocoder_helper'
require 'date'
require 'time_to_text'

class Opinion < ApplicationRecord
  extend OrderAsSpecified

  has_attached_file :video, path: 'raw_opinions/topic_:topic_id/user_:user_id/:id_:style.mov'
  validates_attachment :video, presence: true,
                       content_type: { content_type: 'video/quicktime' }

  has_secure_token
  validates :token, presence: true, on: :update
  validates :token, uniqueness: true, if: :token

  before_video_post_process :fetch_video_length

  #after_save :notify_followers
  after_save :start_transcoding
  after_save :refresh_topic_thumbnail, if: :feature_factor_changed?
  after_save :refresh_topic_thumbnail, if: :locked_at_changed?
  after_save :refresh_topic_thumbnail, if: :cover_changed?
  before_destroy :remove_profile_video_reference
  after_destroy :refresh_topic_thumbnail
  after_destroy :delete_transcoded_video
  define_model_callbacks :transcoding, only: :after
  after_transcoding :refresh_topic_thumbnail
  after_transcoding :notify_about_transcoding_done, if: proc { transcoding_round.zero? }
  after_transcoding :fullfill_requests, if: proc { transcoding_round.zero? }
  after_transcoding :notify_followers, if: proc { transcoding_round.zero? }
  after_transcoding :update_profile_picture
  after_transcoding :notify_topic_host, if: proc { transcoding_round.zero? }

  belongs_to :topic, required: true
  counter_culture :topic # TODO: only processed

  belongs_to :user, required: true
  counter_culture :user # TODO: only processed

  has_many :offense_reports, dependent: :destroy

  has_many :opinion_reactions, dependent: :destroy

  has_many :opinion_translations, dependent: :destroy

  has_many :followed_opinion_uploaded_notifications, as: :subject, dependent: :destroy

  has_many :followed_opinion_topic_uploaded_notifications, as: :subject, dependent: :destroy

  has_many :opinion_locked_notifications, as: :subject, dependent: :destroy

  has_many :opinion_watches, dependent: :destroy

  has_many :claps, dependent: :destroy

  has_one :transcoding_done_notification, as: :subject, dependent: :destroy

  scope :unlocked_opinions, ->(user_id) { where("locked_at IS NULL #{"OR user_id = #{user_id}" if user_id}") }
  scope :unlocked, -> { where(locked_at: nil) }
  scope :featured_first, -> { order('feature_factor DESC') }
  scope :newest_first, -> { order('created_at DESC') }
  scope :oldest_first, -> { order(:created_at) }
  scope :host_video_first, ->(topic) { order("CASE WHEN id = #{topic.host_opinion.id} THEN 0 ELSE 1 END") if topic&.host_opinion }
  scope :highest_ranked_first, ->(topic) {
    if topic&.host_opinion
      order("(CASE WHEN cover THEN feature_factor * 102 WHEN id = #{topic.host_opinion.id} THEN 101 ELSE feature_factor END) DESC")
    else
      order("(CASE WHEN cover THEN feature_factor * 102 ELSE feature_factor END) DESC")
    end
  }
  scope :processed, -> { where(processed: true) }
  scope :featured_opinion_first, ->(opinion_id) { order_as_specified(id: [opinion_id]) if opinion_id }
  scope :featured_user_first, ->(user_id) { order_as_specified(user_id: [user_id]) if user_id }
  scope :followed_users_first, ->(user) { order_as_specified(user_id: user.followed_user_ids) if user }
  scope :most_reacted_first, (lambda do
    reaction_columns = %w(reaction_love_count reaction_haha_count reaction_wow_count
                          reaction_sad_count reaction_angry_count)

    order("#{reaction_columns.join(' + ')} desc")
  end)

  scope :default_ordered, -> (opinion_id, user_id, topic, current_user_id) {
    unlocked_opinions(current_user_id)
        .featured_opinion_first(opinion_id)
        .featured_user_first(user_id)
        .highest_ranked_first(topic)
        .oldest_first
  }

  scope :top_first, -> { order(watch_time: :desc) }

  scope :completion_rate, -> { order('(watch_time / duration) / GREATEST(1, (SELECT count(*) FROM "opinion_watches" WHERE "opinion_watches"."opinion_id" = "opinions"."id")) desc') }

  def remove_profile_video_reference
    user = User.find_by(profile_video: self)
    user.update(profile_video: nil) if user
  end

  def reactions
    { love: reaction_love_count,
      haha: reaction_haha_count,
      wow: reaction_wow_count,
      sad: reaction_sad_count,
      angry: reaction_angry_count }
  end

  def reaction_count
    reactions.values.inject(0) { |sum, x| sum + x }
  end

  def register_reaction(user:, reaction_type:, reaction_second:)
    return if self.user == user

    opinion_reactions.create(user: user,
                             reaction_type: reaction_type,
                             reaction_second: reaction_second)
  end

  def start_transcoding
    OpinionTranscoder.start_transcoding(self, quality == 'sd')
  end

  def mark_as_transcoded(video_hls_url:, video_thumbnail_url:)
    if processed
      delete_transcoded_video
    end

    run_callbacks :transcoding do
      update video_hls_url: video_hls_url,
             video_thumbnail_url: video_thumbnail_url,
             processed: true
    end

    update(transcoding_round: transcoding_round + 1)
  end

  def update_profile_picture
    return unless topic.profile_topic

    user.update(profile_picture_url: video_thumbnail_url)
  end

  def notify_topic_host
    host = topic.creator
    return unless host
    return if host == user || user.guest

    HostedTopicNewVideoNotification.create(subject: self, user: host)
  end

  def refresh_topic_thumbnail
    topic.refresh_thumbnail_url
  end

  def delete_transcoded_video
    return unless video_hls_url.present? && video.options[:storage] == :s3
    TranscodedVideoDeleter.new(video_hls_url).delete
  end

  def notify_about_transcoding_done
    create_transcoding_done_notification(user: user)
  end

  def fullfill_requests
    # TODO: this should be done in the background
    user.received_opinion_requests.where(topic: topic).find_each do |request|
      request.mark_as_fullfilled(self)
    end
  end

  def fetch_video_length
    uploaded_file_path = video.queued_for_write[:original].path
    duration_in_seconds = FFMPEG::Movie.new(uploaded_file_path).duration.to_f

    self.duration = duration_in_seconds
  end

  def notify_followers
    unless topic.password_protected?
      user.follower_users.each { |follower|
        followed_opinion_uploaded_notifications.create(user: follower)

        following_topic = follower.followed_topics.any? { |followed_topic| followed_topic.id == topic.id }
        followed_opinion_topic_uploaded_notifications.create(user: follower) if following_topic
      }
    end
  end

  def location_text
    if lat && lng
      "#{coords} (#{goe_location})"
    end
  end

  def goe_location
    geocoder = GeocoderHelper.new(lat, lng)

    if geocoder.country
      if geocoder.town_name
        "#{geocoder.town_name}, #{geocoder.country}"
      else
        geocoder.country
      end
    else
      'Unavailable'
    end
  end

  def coords
    lats = format_coordinate deg: lat,
                             pos_sign: 'N',
                             neg_sign: 'S'

    lngs = format_coordinate deg: lng,
                             pos_sign: 'E',
                             neg_sign: 'W'

    "#{lats} #{lngs}"
  end

  def format_coordinate(deg:, pos_sign:, neg_sign:)
    d = deg.to_i
    md = (deg - d).abs * 60
    m = md.to_i
    sd = ((md - m) * 60).round(1)
    sign = d < 0 ? neg_sign : pos_sign

    "#{d.abs}º#{m}'#{sd}\"#{sign}"
  end

  def invert_locked
    if locked?
      update(locked_at: nil)
      false
    else
      update(locked_at: Time.now)
      opinion_locked_notifications.create(user: user)
      true
    end
  end

  def locked?
    locked_at != nil
  end

  def days_since_locked
    (Time.now.to_date - locked_at.to_date).round if locked?
  end

  def computed_bad_word_count
    opinion_translations.each { |ot| ot.update_bad_word_count }
    filthiest_translation = opinion_translations.order(bad_word_count: :desc).first

    if filthiest_translation
      filthiest_translation.bad_word_count
    else
      -1
    end
  end

  def update_calculated_values
    update_values = {}

    update_values[:bad_word_count] = computed_bad_word_count
    update_values[:total_reaction_count] = reaction_count
    update_values[:clap_count] = clap_count_live

    if locked?
      if locked_days != days_since_locked
        update_values[:locked_days] = days_since_locked
      end
    else
      if locked_days >= 0
        update_values[:locked_days] = -1
      end
    end

    update(update_values)
  end

  def s3_available?
    processed && !Rails.env.development?
  end

  def original_public_url
    video.s3_object.public_url if s3_available?
  end

  def uuid
    return unless s3_available?

    parts = video_hls_url.split '/'
    parts[parts.size - 2]
  end

  def transcoded_folder_url
    "https://console.aws.amazon.com/s3/buckets/#{video.s3_bucket.name}/transcoded_opinions/#{uuid}/" if s3_available?
  end

  def watch_time_text
    TimeToText.new(watch_time).to_text
  end

  def clap_count_live
    claps.sum(:duration)
  end
end
