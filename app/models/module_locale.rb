# == Schema Information
#
# Table name: module_locales
#
#  id              :integer          not null, primary key
#  topic_module_id :integer          indexed
#  topic_locale_id :integer          indexed
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class ModuleLocale < ApplicationRecord
  belongs_to :topic_module
  belongs_to :topic_locale
end
