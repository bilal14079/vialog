# frozen_string_literal: true
# == Schema Information
#
# Table name: taggings
#
#  id            :integer          not null, primary key
#  hashtag_id    :integer          not null, indexed => [taggable_id, taggable_type]
#  taggable_id   :integer          not null, indexed => [hashtag_id, taggable_type]
#  taggable_type :string           not null, indexed => [hashtag_id, taggable_id]
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Tagging < ApplicationRecord
  belongs_to :taggable, polymorphic: true, required: true
  belongs_to :hashtag, required: true

  validates :hashtag_id, uniqueness: { scope: [:taggable_id, :taggable_type] }
end
