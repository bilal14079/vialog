# == Schema Information
#
# Table name: claps
#
#  id         :integer          not null, primary key
#  opinion_id :integer          indexed
#  user_id    :integer          indexed
#  start      :integer
#  duration   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Clap < ApplicationRecord
  belongs_to :opinion
  belongs_to :user

  scope :intersects, ->(start, duration) { where('(start <= ? AND ? <= start + duration - 1) OR (start <= ? AND ? <= start + duration - 1)', start, start, start + duration, start + duration) }

  def end
    start + duration - 1
  end
end
