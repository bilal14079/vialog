#= require active_admin/base
#= require chosen-jquery

$ ->
  use_chosen()

  $('.button.has_many_add').on 'click', ->
    setTimeout(use_chosen, 0)

use_chosen = () ->
  $('#active_admin_content select').chosen
    allow_single_deselect: true
    no_results_text: 'No results matched'
    width: '240px'
