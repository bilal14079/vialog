#= require jquery
#= require clipboard-1.5.15/clipboard
#= require video-js-5.11.7/video
#= require videojs-contrib-hls-3.6.0/videojs-contrib-hls
#= require mobile-detect-1.3.5/mobile-detect

$ ->
  linkClipboard = new Clipboard('.link-btn', text: ->
    ll('tagEvent', 'Copy video link', {'url': document.URL});
    mixpanel.track('Copy video link', {
      "url": document.URL
    });
    ga('send', {
      hitType: 'event',
      eventCategory: 'Video',
      eventAction: 'Copy video link',
      eventLabel: 'url:' + document.URL
    });
    return document.URL
  )
  embedClipboard = new Clipboard('.embed-btn', text: ->
    ll('tagEvent', 'Copy embed code', {'url': document.URL});
    mixpanel.track('Copy embed code', {
      "url": document.URL
    });
    ga('send', {
      hitType: 'event',
      eventCategory: 'Video',
      eventAction: 'Copy embed code',
      eventLabel: 'url:' + document.URL
    });
    return generateEmbedCode()
  )

  setupMailchimpForm()

  initShareButtons()

  sendVideoWatch()

  handleFooterPosition()
  $(window).resize ->
    handleFooterPosition()

generateEmbedCode = () ->
  pathname = document.location.pathname.substring(1);
  ix = pathname.indexOf('/')
  if ix >= 0
    pathname = pathname.substring(0, ix)

  src = document.location.protocol + '//' + document.location.host + '/' + pathname

  return '<iframe width="360" height="640" src="' + src + '/embed' + '" frameborder="0" allowfullscreen></iframe>'

initShareButtons = () ->
  $('.share-logo').each () ->
    if $(this).hasClass 'tw'
      $(this).parent().attr('href', generateShareUrlForSocialProvider('tw'))
    else if $(this).hasClass 'fb'
      $(this).parent().attr('href', generateShareUrlForSocialProvider('fb'))
    else if $(this).hasClass 'tumblr'
      $(this).parent().attr('href', generateShareUrlForSocialProvider('tumblr'))

generateShareUrlForSocialProvider = (provider) ->
  url = document.URL;
  text = document.getElementsByClassName("page-title")[0].textContent + ' on Vialog'
  title = 'vialog'
  if provider == 'tw'
    return 'https://twitter.com/intent/tweet?url=' + url + '&text=' + text
  else if provider == 'fb'
    return 'http://www.facebook.com/sharer/sharer.php?u=' + url
  else if provider == 'tumblr'
    return 'http://www.tumblr.com/share?v=3&u=' + url + '&t=' + text
  else
    return '#'

setupMailchimpForm = () ->
  md = new MobileDetect(window.navigator.userAgent);
  platform = ''
  if md.os() == 'iOS'
    platform = 'iOS'
  else if md.os() == "AndroidOS"
    platform = 'Android'
  else
    if md.mobile()
      platform = 'Mobile'
    else
      platform = 'Desktop'

  $('#signupfd').val(platform)

handleFooterPosition = () ->
  vh = $(window).height();
  contentHeight = $('.wrapper').height() + $('.footer').height() + 80
  if contentHeight < vh
    $('.footer').not('.fixed').addClass('fixed')
  else
    $('.footer.fixed').removeClass('fixed')

sendVideoWatch = () ->
  opinion_id = $('#opinion_id').val();
  api_subdomain = $('#api_subdomain').val();
  watchtime_url = 'https://' + api_subdomain + '.vialog.cc/v1/opinions/' + opinion_id + '/add_watch_time';

  #https://api-staging.vialog.cc/v1/opinions/123/add_watch_time

  if $('.vjs-tech').get(0)?
    video_paused = $('.vjs-tech').get(0).paused;
    video_ended = $('.vjs-tech').get(0).ended;
    if !video_paused and !video_ended
      $.post(watchtime_url, {watch_time: 6});
  setTimeout sendVideoWatch, 6000

