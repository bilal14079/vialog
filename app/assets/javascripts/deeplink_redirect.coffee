#= require jquery

$ ->
  iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream
  storeLink = $('a.store-link').attr('href')

  if iOS
    link = $('meta[name="apple-itunes-app"]').attr('content')
    start = link.indexOf('app-argument=') + 'app-argument='.length
    link = link.substring(start, link.length)

    $('button.discuss').on 'click', ->
      window.location = link
      setTimeout ->
          window.location = storeLink
      , 500
  else
    $('button.discuss').on 'click', ->
      window.location = storeLink