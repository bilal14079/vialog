# frozen_string_literal: true
class ImageDimensionValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    temp_file = value.queued_for_write[:original]
    return if temp_file.nil?
    check_dimensions(record, attribute, temp_file)
  end

  def check_dimensions(record, attribute, temp_file)
    dimensions = Paperclip::Geometry.from_file(temp_file)

    width = dimensions.width
    height = dimensions.height

    record.errors.add(attribute, "Exact width should be #{width}px") unless width == options[:width]
    record.errors.add(attribute, "Exact height should be #{height}px") unless height == options[:height]
  end
end
