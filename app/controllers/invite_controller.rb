# frozen_string_literal: true
class InviteController < ApplicationController
  layout 'share'

  def show
    @opinion = Opinion.processed.find_by_token!(params[:video])

    if @opinion.locked?
      raise ActiveRecord::RecordNotFound
    end

    @topic = @opinion.topic
    @user  = @opinion.user
    @layoutType = 'ask'

    set_meta_tags(title: meta_title, description: meta_description, og: og_meta_tags,
                  'apple-itunes-app': "app-id=#{ENV['APP_ID']}, app-argument=#{app_argument}")
  end

  private

  def og_meta_tags
    {
      url: og_url, type: 'video', title: meta_title, description: meta_description,
      image: @opinion.video_thumbnail_url,
      video: { url: og_video_url, secure_url: og_video_url, type: 'application/x-shockwave-flash',
               width: 640, height: 480 }
    }
  end

  def og_url
    "#{ENV['OG_CANONICAL_BASE']}/#{@topic.id}/#{@user.id}"
  end

  def og_video_url
    'https://cdn.radiantmediatechs.com/rmp/3.5.0/flash/rmp-dark.swf?' +
        { sharing: true,
          poster: @opinion.video_thumbnail_url,
          documentUrl: og_url,
          src: @opinion.video_hls_url,
          plugin_hls: 'https://cdn.radiantmediatechs.com/rmp/3.5.0/flash/rmp-hls.swf' }.to_param
  end

  def meta_title
    "#{@user.name} asks you about #{@topic.title}"
  end

  def meta_description
    "Discuss this with #{@topic.opinions_count - 1} others on #{ENV['APP_NAME']}."
  end

  def app_argument
    "#{ENV['APP_PROTOCOL']}://?type=ask_opinion&topic_id=#{@topic.id}&user_id=#{@user.id}"
  end
end
