# frozen_string_literal: true
class StaticController < ApplicationController
  def support
    render file: Rails.root.join('static', 'support.html'), layout: false
  end

  def terms
    render file: Rails.root.join('static', 'terms.html'), layout: false
  end

  def robots
    if Rails.env.production?
      render file: Rails.root.join('static', 'robots_production.txt'), layout: false
    else
      render file: Rails.root.join('static', 'robots_staging.txt'), layout: false
    end
  end
end
