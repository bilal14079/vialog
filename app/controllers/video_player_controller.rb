# frozen_string_literal: true
class VideoPlayerController < ApplicationController
  layout 'share'

  def show
    @opinion = Opinion.processed.find_by_token!(params[:video])

    if @opinion.locked?
      raise ActiveRecord::RecordNotFound
    end

    if params[:layoutType].present?
      if params[:layoutType] == 'ask' || params[:layoutType] == 'share' || params[:layoutType] == 'embed'
        @layoutType = params[:layoutType]
      else
        @layoutType = 'share'
      end
    else
      @layoutType = 'share'
    end
  
    set_meta_tags(title: meta_title, description: meta_description, og: og_meta_tags,
                  'apple-itunes-app': "app-id=#{ENV['APP_ID']}, app-argument=#{app_argument}")
  end

  private

  def og_meta_tags
    {
      url: og_url, type: 'video', title: meta_title, description: meta_description,
      image: @opinion.video_thumbnail_url,
      video: { url: og_video_url, secure_url: og_video_url, type: 'application/x-shockwave-flash',
               width: 640, height: 480 }
    }
  end

  def og_url
    "#{ENV['OG_CANONICAL_BASE']}/#{@opinion.token}"
  end

  def meta_title
    if @layoutType == 'share'
      "#{@opinion.user.name} about #{@opinion.topic.title}"
    else
      "#{@opinion.user.name} asks you about #{@opinion.topic.title}"
    end
  end

  def meta_description
    "Discuss this with #{@opinion.topic.opinions_count - 1} others on #{ENV['APP_NAME']}."
  end

  def og_video_url
    'https://cdn.radiantmediatechs.com/rmp/3.5.0/flash/rmp-dark.swf?' +
      { sharing: true,
        poster: @opinion.video_thumbnail_url,
        documentUrl: og_url,
        src: @opinion.video_hls_url,
        plugin_hls: 'https://cdn.radiantmediatechs.com/rmp/3.5.0/flash/rmp-hls.swf' }.to_param
  end

  def app_argument
    "#{ENV['APP_PROTOCOL']}://?type=open_topic&topic_id=#{@opinion.topic.id}&user_id=" \
    "#{@opinion.user.id}&opinion_id=#{@opinion.id}"
  end
end
