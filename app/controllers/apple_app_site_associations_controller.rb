# frozen_string_literal: true
class AppleAppSiteAssociationsController < ApplicationController
  def show
    render json: {
      applinks: {
        apps: [],
        details: [{ appID: ENV.fetch('APPLE_APP_SITE_ASSOCIATION_APP_ID'),
                    paths: ['*'] }]
      }
    }
  end
end
