# frozen_string_literal: true
require 'sns_message_handler'

class SnsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:handle_notification]

  def handle_notification
    message = request.env['sns.message']
    error = request.env['sns.error']

    logger.error error

    if message
      SnsMessageHandler.process(message)
    elsif error
      raise error
    end

    head :ok
  end
end
