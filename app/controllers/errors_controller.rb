class ErrorsController < ApplicationController
  def not_found
    @layoutType = 'error_404'
    render template: 'error_404/show', status: 404
  end
end
