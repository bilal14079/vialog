# frozen_string_literal: true

ActiveAdmin.register OpinionTranslation do
  permit_params :language, :text, :opinion_id

  controller do
    def scoped_collection
      OpinionTranslation.find_each.each do |ot|
        ot.update_bad_word_count
      end
      super
    end
  end

  index do
    selectable_column
    id_column
    column :opinion
    column :language_name
    column :bad_word_count
    column :text
    actions
  end

  show do
    attributes_table do
      row :opinion
      row :language_name
      row :bad_word_count
      row :text
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs 'Translation details' do
      f.input :opinion
      f.input :language, as: :select, collection: OpinionTranslation::LANGUAGES
      f.input :text
    end
    f.actions
  end
end