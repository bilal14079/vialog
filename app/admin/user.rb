# frozen_string_literal: true
ActiveAdmin.register User do
  permit_params :name, :handle, :profile_picture_url

  controller do
    def scoped_collection
      User.find_each.each do |user|
        user.update_stats
      end
      super
    end
  end

  index do
    selectable_column
    column 'id' do |user|
      link_to user.id, user_path(user)
    end
    column 'name', :name
    column 'user' do |user|
      if user.ghost
        status_tag('ghost')
      else
        status_tag('user', :ok)
      end
    end
    column 'watching' do |_user|
      status_tag('NaN')
    end
    column 'seen', :seen_time_text
    column 'videos', :opinions_count
    column 'host', :hosted_issues_count
    column 'followers', :follower_users_count
    column 'following', :followed_users_count
    column 'tracked', :followed_topics_count
    column 'open asks', :open_asks_count
    column 'responded asks', :responded_asks_count
    column 'facebook', :facebook_uid
    column 'twitter', :twitter_handle
    column 'last activity', :last_login
    column 'signup', :created_at
    column 'handle', :handle
    actions
  end

  show do
    attributes_table do
      row 'name', &:name
      row 'user' do |user|
        if user.ghost
          status_tag('ghost')
        else
          status_tag('user', :ok)
        end
      end
      row 'watching' do |_user|
        status_tag('NaN')
      end
      row 'seen', &:seen_time_text
      row 'videos', &:opinions_count
      row 'host', &:hosted_issues_count
      row 'followers', &:follower_users_count
      row 'following', &:followed_users_count
      row 'tracked', &:followed_topics_count
      row 'open asks', &:open_asks_count
      row 'responded asks', &:responded_asks_count
      row 'facebook', &:facebook_uid
      row 'twitter', &:twitter_handle
      row 'last activity', &:last_login
      row 'signup', &:created_at
      row 'handle', &:handle
    end

    if user.ghost
      panel 'Ghost Info' do
        attributes_table_for user.ghost_metadatum do
          row 'invited by', &:creator_user
          row 'asked in topic', &:topic
          row 'ask time', &:ask_time
        end
      end
    end
  end

  filter :name, label: 'name'
  filter :ghost, label: 'user', as: :select, collection: [["Yes", false], ["No", true]]
  filter :seen_time, label: 'seen'
  filter :opinions_count, label: 'videos'
  filter :hosted_issues_count, label: 'host'
  filter :follower_users_count, label: 'followers'
  filter :followed_users_count, label: 'following'
  filter :followed_topics_count, label: 'tracked'
  filter :open_asks_count, label: 'open asks'
  filter :responded_asks_count, label: 'responded asks'
  filter :last_login, label: 'last activity'
  filter :created_at, label: 'signup'
  filter :handle, label: 'handle'

  form do |f|
    f.inputs 'User Details' do
      f.input :name
      f.input :handle
      f.input :profile_picture_url
    end
    f.actions
  end
end
