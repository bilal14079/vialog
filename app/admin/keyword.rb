ActiveAdmin.register_page 'Keywords' do
  content do
    keywords = []

    Topic.all.each do |topic|
      keywords += topic.keyword_list.map { |kw| { topic: topic, word: kw } }
    end

    keywords_data = {}
    keywords.each do |kw|
      if keywords_data.key?(kw[:word])
        data = keywords_data[kw[:word]]
        data[:count] += 1
        data[:topics] += [kw[:topic]]
        keywords_data[kw[:word]] = data
      else
        keywords_data[kw[:word]] = {count: 1, topics: [kw[:topic]]}
      end
    end
    keywords_data = keywords_data.sort_by { |_k, v| v[:count]}.reverse

    table do
      thead do
        tr do
          td do
            span 'Keyword'
          end
          td do
            span 'Count'
          end
          td do
            span 'Threads'
          end
        end
      end
      tbody do
        keywords_data.map do |d|
          tr do
            td do
              span d[0]
            end
            td do
              span d[1][:count]
            end
            td do
              links = d[1][:topics].map { |t| link_to(t.title, topic_path(t)) }

              raw(links.join(' '))
            end
          end
        end
      end
    end
  end
end