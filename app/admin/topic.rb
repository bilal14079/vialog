# frozen_string_literal: true
ActiveAdmin.register Topic, as: 'Thread' do
  permit_params :title, :category_id, :countries, :password, :topic_locale_id, :keywords, hashtag_ids: []

  controller do
    def scoped_collection
      Topic.find_each.each do |topic|
        topic.update_calculated_values
      end
      super
    end
  end

  index do
    selectable_column
    column 'id' do |topic|
      link_to topic.id, thread_path(topic)
    end
    column 'thread', :title
    column 'host', :creator
    column 'watch time', sortable: :watch_time do |topic|
      topic.watch_time_text
    end
    column 'claps', :clap_count
    column 'videos', :opinions_count
    column 'tracked', :follower_count
    column 'created at', :created_at
    column 'new opinion', :last_opinion_time
    column 'category', :category
    column 'keywords', :keywords
    column 'hashtags', :hashtags do |topic|
      topic.hashtags.order('name ASC').pluck(:name).map { |tag| "##{tag}" }.join(' ')
    end
    column 'locale', :topic_locale
    column 'password', :password_protected do |topic|
      status_tag('password protected', :ok) if topic.password_protected?
    end
    actions
  end

  filter :title, label: 'thread'
  filter :creator, label: 'host'
  filter :watch_time, label: 'watch time'
  filter :clap_count, label: 'claps'
  filter :opinions_count, label: 'videos'
  filter :follower_count, label: 'tracked'
  filter :created_at, label: 'created at'
  filter :last_opinion_time, label: 'new opinion'
  filter :category, label: 'category'
  filter :keywords, label: 'keywords'
  filter :hashtags, label: 'hashtags'
  filter :topic_locale, label: 'locale'
  filter :password_protected, label: 'password'

  show do
    attributes_table do
      row 'thread', &:title
      row 'host', &:creator
      row 'watch time', sortable: :watch_time do |topic|
        topic.watch_time_text
      end
      row 'claps', &:clap_count
      row 'videos', &:opinions_count
      row 'tracked', &:follower_count
      row 'created at', &:created_at
      row 'new opinion', &:last_opinion_time
      row 'category', &:category
      row 'keywords', &:keywords
      row 'hashtags', :hashtags do |topic|
        topic.hashtags.order('name ASC').pluck(:name).map { |tag| "##{tag}" }.join(' ')
      end
      row 'locale', &:topic_locale
      row 'password', :password_protected do |topic|
        status_tag('password protected', :ok) if topic.password_protected?
      end
    end
  end

  form do |f|
    f.inputs 'Thread Details' do
      f.input :title
      f.input :category
      f.input :keywords
      f.input :hashtags
      f.input :topic_locale
      f.input :countries
      f.input :password, as: :string
    end
    f.actions
  end
end
