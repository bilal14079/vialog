# frozen_string_literal: true

ActiveAdmin.register Opinion do
  permit_params :topic_id, :user_id, :video, :feature_factor, :cover

  controller do
    def scoped_collection
      Opinion.find_each.each do |opinion|
        opinion.update_calculated_values
      end
      super
    end
  end

  member_action :invert_lock, method: :get do
    if resource.invert_locked
      redirect_to :collection, notice: 'Locked!'
    else
      redirect_to :collection, notice: 'Unlocked!'
    end
  end

  index do
    selectable_column
    column 'id' do |opinion|
      link_to opinion.id, opinion_path(opinion)
    end
    column 'sd/hd' do |opinion|
      if opinion.quality == 'sd'
        status_tag('SD')
      else
        status_tag('HD', :ok)
      end
    end
    column 'thread', :topic
    column 'user', :user
    column 'watched', sortable: :watch_time do |opinion|
      opinion.watch_time_text
    end
    column 'claps', :clap_count
    column 'flagged', :offense_reports_count
    column 'featured', sortable: :feature_factor do |opinion|
      if opinion.cover
        status_tag("#{opinion.feature_factor}", :error)
      else
        opinion.feature_factor
      end
    end
    column 'locked', sortable: :locked_days do |opinion|
      days = opinion.days_since_locked

      if days
        status_tag("#{days} days", :ok)
      end
    end
    column 'uploaded', :created_at
    actions do |opinion|
      message = opinion.locked? ? 'Unlock' : 'Lock'

      link_to message, invert_lock_opinion_path(opinion)
    end
  end

  filter :topic, label: 'thread'
  filter :user, label: 'user'
  filter :watch_time, label: 'watched'
  filter :clap_count, label: 'claps'
  filter :offense_reports_count, label: 'flagged'
  filter :feature_factor, label: 'featured'
  filter :locked_days, label: 'locked'
  filter :created_at, label: 'uploaded'

  form do |f|
    f.inputs 'Opinion Details' do
      f.input :topic
      f.input :user
      f.input :video
      f.input :cover
      f.input :feature_factor
    end
    f.actions
  end

  show do |opinion|
    attributes_table do
      row :id
      row :topic
      row :user
      row :feature_factor do |opinion|
        if opinion.cover
          status_tag("#{opinion.feature_factor}", :error)
        else
          opinion.feature_factor
        end
      end

      row :processed
      row :elastic_transcoder_job_id
      row :token
      row :duration

      row :video_hls_url do |opinion|
        a(href: opinion.video_hls_url, target: '_blank') { opinion.video_hls_url } if opinion.video_hls_url
      end
      row :video_thumbnail_url do |opinion|
        a(href: opinion.video_thumbnail_url, target: '_blank') { opinion.video_thumbnail_url } if opinion.video_thumbnail_url
      end

      row :share_url do |opinion|
        share_url = "https://#{ENV['VIDEO_SUBDOMAIN']}.vialog.cc/#{opinion.token}"

        a(href: share_url, target: '_blank') { share_url }
      end

      row 'Original Video URL' do |opinion|
        a(href: opinion.original_public_url, target: '_blank') { opinion.original_public_url } if opinion.original_public_url
      end

      row 'Transcoded Folder_url' do |opinion|
        a(href: opinion.transcoded_folder_url, target: '_blank') { opinion.transcoded_folder_url } if opinion.transcoded_folder_url
      end

      row :offense_reports_count
      row :bad_word_count do |opinion|
        if opinion.bad_word_count < 0
          'No translation'
        else
          opinion.bad_word_count
        end
      end

      row :location do |opinion|
        if opinion.location_text
          link_to(opinion.location_text, "https://maps.google.com?q=loc:#{opinion.lat}+#{opinion.lng}", target: '_blank')
        else
          'Unavailable'
        end
      end

      row :location do |opinion|
        if opinion.location_text
          iframe(width: 500, height: 500, src: "https://www.google.com/maps/embed/v1/place?key=#{ENV['GOOGLE_API_KEY_2']}&q=#{opinion.lat}+#{opinion.lng}", frameborder: 0)
        else
          'Unavailable'
        end
      end

      opinion.offense_reports.group(:reason).count.each do |reason, count|
        row "\"#{reason}\" offense count" do
          count
        end
      end

      row :reaction_love_count
      row :reaction_haha_count
      row :reaction_wow_count
      row :reaction_sad_count
      row :reaction_angry_count
      row 'Locked' do |opinion|
        days = opinion.days_since_locked

        if days
          status_tag("#{days} days", :ok)
        end
      end
      row 'Watch time', sortable: :watch_time do |opinion|
        opinion.watch_time_text
      end

      row :created_at
      row :updated_at
    end

    panel 'Translations' do
      table_for opinion.opinion_translations do
        column :language_name
        column :bad_word_count
        column :text
        column :actions do |ot|
          links = [
              link_to('Edit', edit_opinion_translation_path(ot)),
              link_to('Delete', opinion_translation_path(ot), 'data-method': :delete, 'data-confirm': 'Are you sure you want to delete this?'),
              link_to('Translate', "https://translate.google.com/?source=osdd##{ot.language}/en/#{ot.text}", target: '_blank')
          ]
          raw(links.join(' '))
        end
      end
      div do
        link_to 'Add translation', new_opinion_translation_path(:opinion_translation => { :opinion_id => opinion })
      end
    end
  end
end
