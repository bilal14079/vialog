# frozen_string_literal: true
ActiveAdmin.register LoginSlide do
  permit_params %i[image text dot_color animated looped display_order]

  config.sort_order = 'display_order_asc'

  index do
    selectable_column
    column :display_order
    column :image do |slide|
      image_tag slide.image.url, height: '128px'
    end
    column :text do |slide|
      raw(slide.html_text)
    end
    column :dot_color do |slide|
      para(style: "color: #{slide.dot_color}") { slide.dot_color } if slide.dot_color
    end
    actions
  end

  show do
    attributes_table do
      row :display_order
      row :image do |slide|
        image_tag slide.image.url, height: '256px'
      end
      row :text do |slide|
        raw(slide.html_text)
      end
      row :dot_color do |slide|
        para(style: "color: #{slide.dot_color}") { slide.dot_color } if slide.dot_color
      end
      row :animated
      row :looped
    end
  end

  form do |f|
    f.inputs 'Login Slide Details' do
      f.input :display_order
      f.input :image
      f.input :text
      f.input :dot_color
      f.input :animated
      f.input :looped
    end

    f.actions
  end
end