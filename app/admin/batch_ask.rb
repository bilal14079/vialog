# frozen_string_literal: true
ActiveAdmin.register_page 'Batch Ask' do
  page_action :batch_ask, method: :post do
    requesting_user = User.find(params[:user_id])
    requested_topic = Topic.find(params[:topic_to_ask_id])
    target_users = case params[:filter_method].to_i
                     when 0 # location
                       lat = params[:lat]
                       lng = params[:lng]
                       radius = params[:radius]
                       if !lat.empty? && !lng.empty? && !radius.empty?
                         User.users_in_range(lat, lng, radius)
                       else
                         User.users_by_country(params[:country_code])
                       end
                     when 1 # topic
                       Topic.find(params[:discussed_topic_id]).opinions.map { |o| o.user }.uniq
                     when 2 # user
                       User.find(params[:followed_user_id]).follower_users
                     else
                       []
                   end

    target_users.each do |u|
      requesting_user.request_opinion_of_user(u, requested_topic)
    end

    redirect_to batch_ask_path, notice: "Asked #{target_users.size} users"
  end

  content do

    def title_text(text)
      para(style: 'font-weight: bold; font-size: larger;') { text }
    end

    def faq_text(text)
      para(style: 'font-style: italic; font-size: smaller') { text }
    end

    form(method: :post, action: batch_ask_batch_ask_path) do
      input(type: :hidden, name: :authenticity_token, value: form_authenticity_token)
      table(style: 'width: 60%; margin: 0 auto;') do
        tbody do

          tr do
            td do
              title_text 'User'
              faq_text 'The user who will ask the other users'
            end

            td do
              select(name: 'user_id') do
                User.find_each.each { |u|
                  option(value: u.id) do
                    u.name
                  end
                }
              end
            end
          end

          tr do
            td do
              title_text 'Thread to ask for'
              faq_text 'The users will be asked about this thread'
            end

            td do
              select(name: 'topic_to_ask_id') do
                Topic.find_each.each { |t|
                  option(value: t.id) do
                    t.title
                  end
                }
              end
            end
          end

          tr do
            td do
              title_text 'Filter method'
              faq_text 'Select how you\'d like to filter the users'
            end

            td do
              select(name: 'filter_method') do
                option(value: 0) { 'Location' }
                option(value: 1) { 'Discussed thread' }
                option(value: 2) { 'Followed User' }
              end
            end
          end

          tr do
            td do
              title_text 'Location Options'
              faq_text 'If you selected \'Location\' for filter method fill this part of the form.'
              faq_text 'You have to either specify a country code, or lat-long-radius.'
            end

            td do
              div do
                para do
                  a(href: 'https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements', target: '_blank') { 'Country code' }
                end
                input(type: :text, name: :country_code)
              end
              hr
              div do
                para 'Latitude'
                input(type: :text, name: :lat)
                para 'Longitude'
                input(type: :text, name: :lng)
                para 'Radius (km)'
                input(type: :text, name: :radius)
              end
            end
          end

          tr do
            td do
              title_text 'Discussed thread selection'
              faq_text 'If you selected \'Discussed thread\' for filter fill this part of the form.'
              faq_text 'The asks will be sent to the users who already uploaded an opinion to this thread.'
            end

            td do
              select(name: 'discussed_topic_id') do
                Topic.find_each.each { |t|
                  option(value: t.id) do
                    t.title
                  end
                }
              end
            end
          end

          tr do
            td do
              title_text 'Followed User selection'
              faq_text 'If you selected \'Followed User\' for filter fill this part of the form.'
              faq_text 'The asks will be sent to the users who are following this user.'
            end

            td do
              select(name: 'followed_user_id') do
                User.find_each.each { |u|
                  option(value: u.id) do
                    u.name
                  end
                }
              end
            end
          end
        end
      end

      input(type: :submit, value: 'Ask users')
    end
  end
end