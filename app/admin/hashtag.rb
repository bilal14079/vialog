# frozen_string_literal: true
ActiveAdmin.register Hashtag do
  permit_params :name

  index do
    selectable_column
    id_column
    column :name
    column :created_at
    actions
  end

  filter :name

  form do |f|
    f.inputs 'Hashtag Details' do
      f.input :name
    end
    f.actions
  end
end
