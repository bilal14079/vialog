# frozen_string_literal: true
ActiveAdmin.register TopicDashboard do
  config.sort_order = 'main_desc'

  permit_params :title,
                :main,
                banner_attributes: [:id, :title, :thumbnail],
                topic_modules_attributes: [:id,
                                           :topic_dashboard_id,
                                           :title,
                                           :title_color,
                                           :title_bg_color,
                                           :position,
                                           :display_type,
                                           :category_id,
                                           :banner_id,
                                           :topic_locale_id,
                                           :_destroy,
                                           :recents,
                                           :topic_limit,
                                           hashtag_ids: [],
                                           topic_locale_ids: []
                ]

  index do
    selectable_column
    column :banner
    column :main
    column :modules_count do |topic_dashboard|
      topic_dashboard.topic_modules.count
    end
    actions
  end

  filter :main
  filter :banner

  form do |f|
    f.inputs 'Thread Dashboard Details' do
      f.input :main
    end

    unless f.object.main?
      f.inputs 'Banner' do
        f.inputs for: [:banner, (f.object.banner || f.object.build_banner)] do |b|
          b.input :title
          b.input :thumbnail
        end
      end
    end

    f.inputs 'Thread modules' do
      f.has_many :topic_modules, allow_destroy: true, sortable: :position do |tm|
        tm.input :title
        tm.input :title_color
        tm.input :title_bg_color
        tm.input :topic_locales, label: 'Locales'
        tm.input :display_type, as: :select, collection: ::TopicModule::DISPLAY_TYPES
        tm.input :recents
        tm.input :topic_limit
        tm.input :category
        tm.input :hashtags
        tm.input :banner
        tm.input :type, input_html: { disabled: true }, hint: 'will be set automatically'
      end
    end
    f.actions
  end

  show do |topic_dashboard|
    attributes_table do
      row :main
    end

    unless topic_dashboard.main?
      panel 'Banner' do
        attributes_table_for topic_dashboard.banner do
          row :title
          row :thumbnail do
            image_tag topic_dashboard.banner.thumbnail_url
          end
          row :thumbnail_content_type
          row :thumbnail_file_size
          row :thumbnail_updated_at
        end
      end
    end

    panel 'Thread modules' do
      table_for topic_dashboard.topic_modules do
        column :title
        column :type
        column :topic_limit
        column :locales do |tm|
          raw(tm.topic_locales.order(:name).map { |tl| link_to tl.name, topic_locale_path(tl) }.join(', '))
        end
        column :display_type
        column :category
        column :hashtags do |topic_module|
          topic_module.hashtags.order('name ASC').pluck(:name).map { |tag| "##{tag}" }.join(' ')
        end
        column :banner_dashboard
        column :title_color do |topic_module|
          para(style: "color: #{topic_module.title_color}") { topic_module.title_color } if topic_module.title_color
        end
        column :title_background_color do |topic_module|
          para(style: "color: #{topic_module.title_bg_color}") { topic_module.title_bg_color } if topic_module.title_bg_color
        end
      end
    end
  end
end
