# frozen_string_literal: true
ActiveAdmin.register MobileAppVersion do
  permit_params :platform, :update_severity, :version_identifier

  index do
    selectable_column
    column :platform
    column :update_severity
    column :version_identifier
    column :created_at
    actions
  end

  filter :platform, as: :select, collection: MobileAppVersion::PLATFORMS
  filter :update_severity, as: :select, collection: MobileAppVersion::UPDATE_SEVERITIES

  form do |f|
    f.inputs 'Mobile App Version Details' do
      f.input :platform, as: :select, collection: MobileAppVersion::PLATFORMS
      f.input :update_severity, as: :select, collection: MobileAppVersion::UPDATE_SEVERITIES
      f.input :version_identifier
    end
    f.actions
  end
end
