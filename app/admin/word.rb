# frozen_string_literal: true

ActiveAdmin.register Word do
  permit_params :language, :word, :profanity

  index do
    selectable_column
    id_column
    column :language
    column :word
    column :profanity
    actions
  end

  show do
    attributes_table do
      row :language
      row :word
      row :updated_at
      row :profanity
    end
  end

  form do |f|
      f.inputs 'Word Details' do
        f.input :language, as: :select, collection: OpinionTranslation::LANGUAGES
        f.input :word
        f.input :profanity
      end
    f.actions
  end
end