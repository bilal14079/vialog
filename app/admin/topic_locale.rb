# frozen_string_literal: true
ActiveAdmin.register TopicLocale do
  permit_params :name, :country_code, :latitude, :longitude, :radius

  index do
    para(style: 'font-style: italic; font-size: smaller;') do
          'Create a locale for every location you want to assign threads for. After you have the desired locale go to \'Threads\' and select it for the ones you would like to display at that region.'
    end
    para(style: 'font-style: italic; font-size: smaller; margin-bottom: 20px;') do
          a(href: 'https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements', target: '_blank') { 'ISO-3166 alpha-2 country list' }
    end
    selectable_column
    id_column
    column :name
    column :country_code
    column :lat_long do |locale|
      if locale.location_mode?
        lats = format_coordinate deg: locale.latitude,
                                 pos_sign: 'N',
                                 neg_sign: 'S'

        lngs = format_coordinate deg: locale.longitude,
                                 pos_sign: 'E',
                                 neg_sign: 'W'

        "#{lats} #{lngs}"
      end
    end
    column :radius do |locale|
      if locale.location_mode?
        "#{locale.radius} km"
      end
    end
    actions
  end

  filter :id
  filter :name
  filter :country_code
  filter :topics

  form do |f|
    f.inputs 'Locale Details' do
      f.input :name
      f.input :country_code
      f.input :latitude
      f.input :longitude
      f.input :radius
    end
    f.actions
  end
end