module ActiveAdmin::ViewsHelper
  def format_coordinate(deg:, pos_sign:, neg_sign:)
    d = deg.to_i
    md = (deg - d).abs * 60
    m = md.to_i
    sd = ((md - m) * 60).round(1)
    sign = d < 0 ? neg_sign : pos_sign

    "#{d.abs}º#{m}'#{sd}\"#{sign}"
  end
end
