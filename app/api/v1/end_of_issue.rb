# frozen_string_literal: true
module V1
  class EndOfIssue < Grape::API
    group :topics do
      route_param :id do
        # GET /v1/topics/:id/eoi_asks
        desc 'Get random users with asks in this topic',
             entity: V1::Entities::User
        params do
          optional :limit, type: Integer, default: 5, desc: 'The number of results to return'
        end
        get :eoi_asks do
          topic = Topic.find(params[:id])
          excluded_users = topic.opinions.select(:user_id).distinct.pluck(:user_id)

          if current_user
            excluded_users += [current_user.id]
            excluded_users += OpinionRequest.select(:requested_user_id).where(topic: topic, requester_user: current_user).distinct.pluck(:requested_user_id)
          end

          user_ids = topic.opinion_requests.select(:requested_user_id).where.not(requested_user_id: excluded_users).distinct
          users = User.where(id: user_ids).order('RANDOM()').limit(params[:limit]).to_a

          if users.count < params[:limit]
            excluded_users += users.map(&:id)
            users += User.where.not(id: excluded_users).order('RANDOM()').limit(params[:limit] - users.count).to_a
          end

          present users, with: V1::Entities::User,
                  current_user: current_user,
                  current_topic: topic,
                  root: :users
        end
      end

      route_param :id do
        # GET /v1/topics/:id/eoi_my_asks
        desc 'Get the asking users of the current user',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
             entity: V1::Entities::OpinionRequests
        params do
          optional :limit, type: Integer, default: 5, desc: 'The number of results to return'
        end
        get :eoi_my_asks do
          authenticate_user!

          asks = current_user.asks_by_topic.select { |ask| ask.topic.id == params[:id].to_i }

          present asks, with: V1::Entities::OpinionRequests,
                  root: :asks,
                  current_user: current_user
        end
      end
    end
  end
end