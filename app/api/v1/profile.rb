# frozen_string_literal: true
module V1
  class Profile < Grape::API
    group :users do
      route_param :id, type: Integer do
        # GET /v1/users/:id
        desc 'Public profile of the user',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info,
             entity: V1::Entities::UserProfile
        get do
          user = User.find(params[:id])
          present user, with: V1::Entities::UserProfile,
                  root: :user,
                  profiled_user: user,
                  current_user: current_user
        end
      end

      route_param :id, type: Integer do
        #GET /v1/users/:id/followed_topics
        desc 'Returns the topics followed by the user',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info,
             entity: V1::Entities::Topic
        params do
          optional :limit, type: Integer, default: 100, desc: 'Max number of results to return'
        end
        get :followed_topics do
          user = User.find(params[:id])

          topics = user.followed_topics
                       .recently_followed_first
                       .contains_video
                       .limit(params[:limit])

          present topics, with: V1::Entities::Topic,
                  root: :topics,
                  current_user: current_user
        end
      end
    end
  end
end
