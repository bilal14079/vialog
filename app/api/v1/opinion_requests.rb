# frozen_string_literal: true
module V1
  class OpinionRequests < Grape::API
    group :me do
      # GET /v1/me/asks
      desc 'Get my asks',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
           entity: V1::Entities::OpinionRequests
      get :asks do
        authenticate_user!

        asks_by_topic = current_user.asks_by_topic

        present asks_by_topic, with: V1::Entities::OpinionRequests,
                root: :asks,
                current_user: current_user
      end
    end

    group :users do
      route_param :id, type: Integer do
        # GET /v1/users/:id/asks
        desc 'Get the asks of the user',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(false),
             entity: V1::Entities::OpinionRequests
        get :asks do
          user = User.find(params[:id])
          asks_by_topic = user.asks_by_topic(current_user)

          present asks_by_topic, with: V1::Entities::OpinionRequests,
                  root: :asks,
                  current_user: current_user
        end
      end
    end
  end
end