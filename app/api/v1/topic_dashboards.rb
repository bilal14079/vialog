# frozen_string_literal: true
module V1
  class TopicDashboards < Grape::API
    namespace :topic_dashboards do
      # GET /v1/topic_dashboards/main
      desc 'Returns the main topic dashboard',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info,
           entity: V1::Entities::TopicDashboard
      params do
        optional :country_code, type: String, desc: 'The country from where the topics are requested'
        optional :lat, type: Float, desc: 'The current latitude of the user'
        optional :lng, type: Float, desc: 'The current longitude of the user'
      end
      get 'main' do
        main_topic_dashboard = TopicDashboard.find_by(main: true)

        local_topics = []
        if params[:lat] && params[:lng]
          locales = TopicLocale.locales_by_lat_long(params[:lat], params[:lng]).sort_by { |l| l.distance_from(params[:lat], params[:lng]) }
          local_topics += locales.flat_map { |l| l.topics }.sort_by { |t| t.newest_opinion_upload_time }.reverse
        end

        country_code = params[:country_code]

        if current_user
          if params[:lat] && params[:lng] && current_user.updated_at < Time.now - 8.hours
            current_user.update_last_location lat: params[:lat],
                                              lng: params[:lng]
          end
          country_code = current_user.last_country if current_user.last_country
        end

        if country_code
          locales = TopicLocale.locales_by_country(country_code)
          local_topics += locales.flat_map { |l| l.topics }.sort_by { |t| t.newest_opinion_upload_time }.reverse
        end

        present main_topic_dashboard,
                with: V1::Entities::TopicDashboard,
                current_user: current_user,
                country_code: country_code,
                latitude: params[:lat],
                longitude: params[:lng],
                local_topics: local_topics,
                include_thumbnails: true
      end

      route_param :id, type: Integer do
        # GET /v1/topic_dashboards/:id
        desc 'Returns a topic dashboard by id',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info,
             entity: V1::Entities::TopicDashboard
        params do
          optional :country_code, type: String, desc: 'The country from where the topics are requested'
        end
        get do
          topic_dashboard = TopicDashboard.find(params[:id])

          present topic_dashboard,
                  with: V1::Entities::TopicDashboard,
                  country_code: params[:country_code],
                  include_thumbnails: true
        end
      end
    end
  end
end
