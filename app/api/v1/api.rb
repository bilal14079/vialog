# frozen_string_literal: true
require 'facebook/authenticator'
require 'twitter/authenticator'

module V1
  class API < Grape::API
    helpers V1::Helpers::Authentication

    use GrapeLogging::Middleware::RequestLogger,
        instrumentation_key: 'api_log_message',
        include: [GrapeLogging::Loggers::Response.new,
                  GrapeLogging::Loggers::FilterParameters.new]

    version 'v1', using: :path

    format :json
    content_type :json, 'application/json'
    default_format :json

    rescue_from Grape::Exceptions::ValidationErrors do |e|
      error!({ error: e.message }, 400)
    end

    rescue_from Facebook::AuthenticationError do |_e|
      error!({ error: 'Invalid facebook access token' }, 400)
    end

    rescue_from Twitter::AuthenticationError do |e|
      error!({ error: e.message }, 400)
    end

    rescue_from ActiveRecord::RecordInvalid do |e|
      error!({ error: e.message }, 422)
    end

    rescue_from ActiveRecord::RecordNotFound do |e|
      error!({ error: e.message }, 404)
    end

    rescue_from Grape::Exceptions::MethodNotAllowed do |e|
      error!({ error: e.message }, 405)
    end

    rescue_from :all do |error|
      Rails.logger.tagged('GRAPE ERROR') do |logger|
        logger.error(error)
        error.backtrace.each { |line| logger.error(line) }
      end

      error!({ error: 'Internal server' }, 500)
    end

    mount ApiInfo unless Rails.env.production?
    mount UserInfo unless Rails.env.production?

    mount FacebookFriends
    mount Follow
    mount ForceUpdate
    mount Login
    mount Logout
    mount Notifications
    mount Opinions
    mount Profile
    mount ReactToOpinion
    mount RegisterForPushNotifications
    mount Report
    mount RequestOpinion
    mount Search
    mount TopicDashboards
    mount Topics
    mount UpdateProfile
    mount UploadOpinion
    mount UserOpinions
    mount UsersWantedForOpinion
    mount UserTopicsNeedingOpinion
    mount UpdateWatchTime
    mount Suggest
    mount MarkOpinionWatched
    mount OpinionTranslations
    mount OpinionRequests
    mount FindAndAsk
    mount Feed
    mount EndOfIssue
    mount Config
    mount UpdateEmail
    mount Ghosts
    mount IntercomApi
    mount Settings

    add_swagger_documentation api_version: 'v1',
                              info: { title: GrapeSwaggerRails.options.app_name },
                              base_path: '/',
                              hide_format: true,
                              hide_documentation_path: true,
                              format: :json unless Rails.env.production?
  end
end
