# frozen_string_literal: true

module V1
  class MarkOpinionWatched < Grape::API
    before {authenticate_user!}

    group :opinions do
      route_param :id, type: Integer do
        # PUT /v1/opinions/:id/mark_watched
        desc 'Mark an opinion watched for the logged in user',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
        put :mark_watched do
          opinion = Opinion.find(params[:id])
          current_user.mark_opinion_watched(opinion)

          {success: true}
        end
      end
    end
  end
end