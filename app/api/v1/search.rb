# frozen_string_literal: true
require 'twitter/ghost_register'

module V1
  class Search < Grape::API
    group :search do
      # GET /v1/search/topics
      desc 'Title based topic search.',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info,
           entity: V1::Entities::TopicSearchResult
      params do
        requires :q, type: String, desc: 'Search string'
        optional :limit, type: Integer, default: 10, desc: 'Max number of results to return'
      end
      get :topics do
        topics = Topic.includes(:category, :hashtags)
                     .not_profile_topic
                     .contains_video
                     .search_by_query(params[:q])
                     .order_by_last_opinion
                     .limit(params[:limit])

        present topics, with: V1::Entities::TopicSearchResult,
                root: :topics,
                current_user: current_user,
                query: params[:q]
      end

      # GET /v1/search/users
      desc 'Name and Twitter handle based user search.',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info,
           entity: V1::Entities::User
      params do
        requires :q, type: String, desc: 'Search string'
        optional :limit, type: Integer, default: 10, desc: 'Max number of results to return'
      end
      get :users do
        excluded_users = if current_user
                           [current_user.id]
                         else
                           []
                         end
        users = User.where.not(id: excluded_users)
                    .search_by_query(params[:q])
                    .order(last_login: :desc)
                    .limit(params[:limit])

        present users, with: V1::Entities::User,
                root: :users,
                current_user: current_user,
                query: params[:q]
      end

      # GET /v1/search/twitter
      desc 'Search a user on Twitter',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(false)
      params do
        requires :handle, type: String, desc: 'The Twitter handle we are searching'
      end
      get :twitter do
        user = User.find_by(twitter_handle: params[:handle])

        if user.present?
          { found: false, already_registered: true }
        else
          gr = Twitter::GhostRegister.new(handle: params[:handle])

          if gr.handle_exists?
            {
                found: true,
                already_registered: false,
                name: gr.twitter_name,
                email: gr.twitter_email,
                handle: gr.twitter_handle,
                profile_picture_url: gr.profile_picture_url
            }
          else
            { found: false, already_registered: false }
          end
        end
      end
    end
  end
end
