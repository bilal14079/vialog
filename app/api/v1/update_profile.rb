# frozen_string_literal: true
require 'mailchimp_subscriber'

module V1
  class UpdateProfile < Grape::API
    before { authenticate_user! }

    group :me do
      # GET /me/enabled_notifications
      desc 'Returns the current user\'s enabled notification types for the current device',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
      get :enabled_notifications do
        notification_types = current_user.push_notification_registrations.pluck(:notification_type)

        { enabled_notifications: notification_types }
      end

      # POST /me
      desc 'Update the profile of the current user',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
           entity: V1::Entities::User
      params do
        optional :name, type: String, desc: 'Name of the user'
        optional :bio, type: String, desc: 'Bio of the user'
      end
      post do
        attrs = {}

        attrs[:handle] = params[:name] if params[:name]
        attrs[:bio] = params[:bio] if params[:bio]

        current_user.update!(attrs)

        present current_user, with: V1::Entities::User,
                root: :users
      end

      # POST /me/update_location
      desc 'Update the location of the current user',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
      params do
        requires :lat, type: Float, desc: 'Latitude of the user'
        requires :lng, type: Float, desc: 'Longitude of the user'
      end
      post :update_location do
        current_user.update_last_location lat: params[:lat],
                                          lng: params[:lng]

        { success: true }
      end

      # PUT /v1/me/select_profile_video
      desc 'Select a video as my new profile video',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
      params do
        requires :id, type: Integer, desc: 'The ID of the opinion to be used as profile video'
      end
      put :select_profile_video do
        opinion = Opinion.find(params[:id])

        if opinion.topic.profile_topic
          current_user.update(profile_video: opinion, profile_picture_url: opinion.video_thumbnail_url)
          { success: true }
        else
          { success: false }
        end
      end
    end
  end
end
