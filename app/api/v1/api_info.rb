# frozen_string_literal: true
module V1
  class ApiInfo < Grape::API
    # GET /v1/env
    desc 'Returns the current API version and environment'
    get 'env' do
      { version: 'v1', environment: Rails.env }
    end

    # GET /v1/errors/invalid_http_params
    desc 'Returns the general respone to api parameter validation errors'
    get 'errors/invalid_http_params', hidden: true do
      raise Grape::Exceptions::ValidationErrors, errors: []
    end

    # GET /v1/errors/invalid_resource_params
    desc 'Returns the general respone to resource validation errors'
    get 'errors/invalid_resource_params', hidden: true do
      raise ActiveRecord::RecordInvalid
    end

    # GET /v1/errors/missing_resource
    desc 'Returns the general respone to missing resource errors'
    get 'errors/missing_resource', hidden: true do
      raise ActiveRecord::RecordNotFound
    end

    # GET /v1/errors/unexpected_server_error
    desc 'Returns the general respone to unexpected server errors'
    get 'errors/unexpected_server_error', hidden: true do
      raise StandardError
    end
  end

  class UserInfo < Grape::API
    before { authenticate_user! }

    # GET /v1/ping_user
    desc 'Returns pong if the given user creditentials are valid',
         headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
    get 'ping_user' do
      { data: 'pong' }
    end
  end
end
