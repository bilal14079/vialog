# frozen_string_literal: true
module V1
  class OpinionTranslations < Grape::API
    before { authenticate_user! }

    group :opinions do
      route_param :id, type: Integer do
        # PUT v1/opinions/:id/add_translation
        desc 'Adds a new translation to the specified opinion',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
        params do
          requires :text,
                   type: String,
                   desc: 'The translation text'
          requires :language,
                   type: String,
                   desc: 'The language of the translation'
        end
        put :add_translation do
          opinion = Opinion.find(params[:id])

          if opinion.user == current_user
            current_translation = opinion.opinion_translations.find_by(language: params[:language])

            if current_translation.present?
              current_translation.update!(text: params[:text])
            else
              opinion.opinion_translations.create!(text: params[:text], language: params[:language])
            end
          end

          {success: true}
        end
      end
    end
  end
end