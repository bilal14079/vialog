# frozen_string_literal: true

module V1
  class Settings < Grape::API
    before { authenticate_user! }

    group :settings do
      # PUT /v1/settings/value
      desc 'Add/update a value to the settings',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
      params do
        requires :key, type: String, desc: 'The key for the value to be saved'
        requires :value, type: String, desc: 'The actual value'
      end
      put :value do
        setting = current_user.user_settings.find_by(key: params[:key])

        if setting
          setting.update(value: params[:value])
        else
          current_user.user_settings.create(key: params[:key], value: params[:value])
        end

        { success: true }
      end

      # GET /v1/settings/keys
      desc 'Return the saved keys for the current_user',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
      get :keys do
        present current_user.user_settings.map(&:key),
                root: :settings
      end

      # GET /v1/settings/values
      desc 'Get the already saved settings for the current user',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
           entity: V1::Entities::UserSetting
      get :values do
        present current_user.user_settings, with: V1::Entities::UserSetting,
                root: :settings
      end

      route_param :key, type: String do
        # DELETE /v1/settings/:key
        desc 'Delete the setting with `key`',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
        delete do
          setting = current_user.user_settings.find_by(key: params[:key])

          setting.destroy if setting

          { success: true }
        end
      end
    end
  end
end