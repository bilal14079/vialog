# frozen_string_literal: true
module V1
  class Config < Grape::API
    group :config do
      # GET /v1/config/login_slides
      desc 'Get the slides to be shown at the login screen',
           entity: V1::Entities::LoginSlide
      get :login_slides do
        slides = LoginSlide.order(:display_order)

        present slides, with: V1::Entities::LoginSlide,
                root: :slides
      end
    end
  end
end