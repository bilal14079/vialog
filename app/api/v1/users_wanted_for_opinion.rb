# frozen_string_literal: true
module V1
  class UsersWantedForOpinion < Grape::API
    group :topics do
      route_param :id, type: Integer do
        # GET /v1/topics/:id/users_wanted_for_opinion
        desc 'Returns the users who have at least one opinion request in the given topic',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info,
             entity: V1::Entities::UserWantedForOpinion
        params do
          optional :limit, type: Integer, default: 100, desc: 'Max number of results to return'
        end
        get :users_wanted_for_opinion do
          topic = Topic.find(params[:id])
          users = topic.users_wanted_for_opinion_ordered_by_request_count
                       .limit(params[:limit])

          present users, with: V1::Entities::UserWantedForOpinion,
                         root: :users,
                         current_topic: topic,
                         current_user: current_user
        end
      end
    end
  end
end
