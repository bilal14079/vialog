# frozen_string_literal: true
module V1
  class Topics < Grape::API
    group :topics do
      # GET /v1/topics/my_topics
      desc 'Get the list of topics created by the current user',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
           entity: V1::Entities::Topic
      get :my_topics do
        authenticate_user!

        topics = Topic.user_topics_order_by_last_updated(current_user)

        present topics, with: V1::Entities::Topic,
                root: :topics,
                current_user: current_user,
                include_thumbnails: true
      end

      route_param :id do
        # GET /v1/topics/:id
        desc 'Topic info',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info,
             entity: V1::Entities::Topic
        get do
          topic = Topic.find(params[:id])

          present topic, with: V1::Entities::Topic,
                  root: :topic,
                  current_user: current_user
        end
      end

      route_param :id do
        # PUT /v1/topics/:id/mark_opened
        desc 'Mark topic opened',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
        put :mark_opened do
          if current_user
            topic = Topic.find(params[:id])
            last_open = TopicOpen.find_by(topic: topic, user: current_user)

            if last_open
              last_open.update(time: Time.now)
            else
              TopicOpen.create(topic: topic, user: current_user, time: Time.now)
            end

            { success: true }
          else
            { success: false }
          end
        end
      end

      route_param :id do
        # GET /v1/topics/:id/asking_users
        desc 'Get the list of users asking about this topic',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(false)
        get :asking_users do
          topic = Topic.find(params[:id])

          users = topic.opinion_requests.map(&:requester_user)

          present users, with: V1::Entities::User,
                  root: :asking_users,
                  current_user: current_user
        end
      end

      # PUT /v1/topics
      desc 'Create a new topic',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
      params do
        requires :name, type: String, desc: 'The name of the new topic'
      end
      put do
        authenticate_user!

        category = Category.find_by(default_mark: true)
        topic = Topic.create!(title: params[:name], category: category, creator: current_user)
        current_user.follow_topic(topic)

        {
            success: true,
            topic_id: topic.id
        }
      end
    end
  end
end
