# frozen_string_literal: true
module V1
  class ReactToOpinion < Grape::API
    before { authenticate_user! }

    group :opinions do
      route_param :id, type: Integer do
        # POST /v1/opinions/:id/react
        desc 'Post a reaction to the given opinion',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
        params do
          requires :reaction_type, type: String, desc: 'Reaction type', values: ::OpinionReaction::REACTION_TYPES
          requires :reaction_second, type: Integer, desc: 'Reaction timestamp in seconds', values: (1...60).to_a
        end
        post :react do
          opinion = Opinion.find(params[:id])
          opinion.register_reaction(user: current_user,
                                    reaction_type: params[:reaction_type],
                                    reaction_second: params[:reaction_second])

          { success: true }
        end

        # POST /v1/opinions/:id/react_in_batch
        desc 'Post multiple reactions to the given opinion',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
        params do
          requires :reactions, type: Array, desc: 'An array of reactions' do
            requires :reaction_type, type: String, desc: 'Reaction type', values: ::OpinionReaction::REACTION_TYPES
            requires :reaction_second, type: Integer, desc: 'Reaction timestamp in seconds', values: (1...60).to_a
          end
        end
        post :react_in_batch do
          opinion = Opinion.find(params[:id])
          params[:reactions].each do |reaction_params|
            opinion.register_reaction(user: current_user,
                                      reaction_type: reaction_params[:reaction_type],
                                      reaction_second: reaction_params[:reaction_second])
          end

          { success: true }
        end

        # POST /v1/opinions/:id/clap
        desc 'Post a clap interval for the given opinion',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
        params do
          requires :start_sec, type: Integer, desc: 'The second where the user started clapping'
          requires :duration, type: Integer, desc: 'The duration of the clap in seconds'
        end
        post :clap do
          opinion = Opinion.find(params[:id])
          start_sec = [0, params[:start_sec]].max
          duration = [params[:duration], opinion.duration.round - start_sec + 1].min

          if opinion.user != current_user
            prev_clap = opinion.claps.where(user: current_user).intersects(start_sec, duration).first

            if prev_clap
              new_start = [prev_clap.start, start_sec].min
              new_end = [prev_clap.end, start_sec + duration - 1].max
              new_duration = new_end - new_start + 1
              other_intersect = opinion.claps.where.not(id: prev_clap.id).intersects(new_start, new_duration).first
              if other_intersect
                new_start = [new_start, other_intersect.start].min
                new_end = [new_end, other_intersect.end].max
                new_duration = new_end - new_start + 1
                other_intersect.destroy
              end
              prev_clap.update(start: new_start, duration: new_duration)
            else
              opinion.claps.create(user: current_user, start: start_sec, duration: duration)
            end
          end

          { success: true }
        end
      end
    end
  end
end
