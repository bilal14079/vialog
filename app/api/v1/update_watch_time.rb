# frozen_string_literals: true

require 'redis'

module V1
  class UpdateWatchTime < Grape::API
    before do
      header 'Access-Control-Allow-Origin', '*'
    end

    group :opinions do
      route_param :id, type: Integer do
        # POST /v1/opinions/:id/add_watch_time
        desc 'Adds to specified amount of seconds to the opinion as watched time',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(false)
        params do
          requires :watch_time, type: Integer, desc: 'The watch time to add to the opinion in seconds'
        end
        post :add_watch_time do
          redis = Redis.new

          redis.hincrby :opinion_watch_times,
                        params[:id],
                        params[:watch_time]

          {success: true}
        end
      end
    end
  end
end