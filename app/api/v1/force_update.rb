# frozen_string_literal: true
module V1
  class ForceUpdate < Grape::API
    # GET /v1/mobile_app_version
    desc 'Provides information about the current mobile app version',
         entity: V1::Entities::MobileAppVersion
    params do
      requires :platform, type: String, desc: 'Mobile app platform', values: ::MobileAppVersion::PLATFORMS
    end
    get :mobile_app_version do
      version = MobileAppVersion.latest_version_for_platform(params[:platform])

      present version, with: V1::Entities::MobileAppVersion
    end
  end
end
