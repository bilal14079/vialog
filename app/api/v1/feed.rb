# frozen_string_literal: true
module V1
  class Feed < Grape::API
    include Grape::Kaminari
    before { authenticate_user! }

    group :me do
      # GET /v1/me/feed
      desc 'Get the current users feed',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
           entity: V1::Entities::ActionItem
      # paginate per_page: 20, max_per_page: 100, offset: 0
      get :feed do
        items = ActionItem.where(actor: current_user.followed_users).order(created_at: :desc)
        invalid_items = items.select { |item| item.target.nil? }.map(&:id)
        filtered_out_items = invalid_items + items.select { |item| item.asked_current_user(current_user) || item.follows_current_user?(current_user) }.map(&:id)
        items = items.where.not(id: filtered_out_items)
        # paginated_items = paginate(items)

        ActionItem.where(id: invalid_items).destroy_all

        # present paginated_items, with: V1::Entities::ActionItem,
        #         current_user: current_user,
        #         root: :feed
      end
    end
  end
end
