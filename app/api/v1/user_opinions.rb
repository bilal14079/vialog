# frozen_string_literal: true
module V1
  class UserOpinions < Grape::API
    #before { authenticate_user! }

    group :opinions do
      route_param :id, type: Integer do
        # DELETE /v1/opinions/:id
        desc 'Delete specified opinion of the current user',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
        delete do
          opinion = current_user.opinions.find(params[:id])
          opinion.destroy

          { success: true }
        end
      end
    end

    group :me do
      # GET /v1/me/opinions
      desc 'My uploaded opinions grouped by topics',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
           entity: V1::Entities::GroupedOpinion
      get :opinions do
        my_topics = Topic.find(current_user.participating_topics)

        present my_topics, with: V1::Entities::GroupedOpinion,
                root: :topics,
                current_user: current_user,
                user: current_user
      end
    end

    group :users do
      route_param :id, type: Integer do
        # GET /v1/users/:id/opinions
        desc 'Uploaded opinions of the specified user grouped by topics',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(false),
             entity: V1::Entities::GroupedOpinion
        get :opinions do
          user = User.find(params[:id])
          topics = Topic.find(user.participating_topics)

          present topics, with: V1::Entities::GroupedOpinion,
                  root: :topics,
                  current_user: current_user,
                  user: user
        end
      end
    end
  end
end
