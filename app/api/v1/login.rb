# frozen_string_literal: true
require 'facebook/authenticator'
require 'mailchimp_subscriber'
require 'date'

module V1
  class Login < Grape::API
    group :login do
      # POST /v1/login/facebook
      desc 'Log in with Facebook',
           entity: V1::Entities::SessionToken
      params do
        requires :fb_access_token, type: String, desc: 'Facebook access token'
        requires :device_uid, type: String, desc: 'Device UID'
      end
      post :facebook do
        authenticator = Facebook::Authenticator.new(access_token: params[:fb_access_token],
                                                    device_uid: params[:device_uid])

        user_email = authenticator.fb_user.email
        if user_email && !user_email.empty?
          MailchimpSubscriber.new.subscribe list_id: ENV['MAILCHIMP_LIST_ID'],
                                            email: user_email,
                                            first_name: authenticator.fb_user.first_name,
                                            last_name: authenticator.fb_user.last_name
        end

        session_token = authenticator.authenticate

        present session_token, with: V1::Entities::SessionToken,
                root: :user
      end

      # POST /v1/login/twitter
      desc 'Log in with Twitter',
           entity: V1::Entities::SessionToken
      params do
        requires :access_token, type: String, desc: 'Twitter access token'
        requires :access_token_secret, type: String, desc: 'Twitter access token secret'
        requires :device_uid, type: String, desc: 'Device UID'
      end
      post :twitter do
        authenticator = Twitter::Authenticator.new(access_token: params[:access_token],
                                                   access_token_secret: params[:access_token_secret],
                                                   device_uid: params[:device_uid])
        user_email = authenticator.twitter_user.email
        if user_email && !user_email.empty?
          MailchimpSubscriber.new.subscribe list_id: ENV['MAILCHIMP_LIST_ID'],
                                            email: user_email,
                                            first_name: authenticator.twitter_user.first_name,
                                            last_name: authenticator.twitter_user.last_name
        end

        session_token = authenticator.authenticate

        present session_token, with: V1::Entities::SessionToken,
                root: :user
      end

      # POST /v1/login/app_start
      desc 'Signal app start of user',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info
      post :app_start do
        current_user.update(last_login: DateTime.now) if current_user

        { success: true }
      end
    end
  end
end
