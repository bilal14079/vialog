# frozen_string_literal: true
module V1
  class Opinions < Grape::API
    include Grape::Kaminari

    group :topics do
      route_param :id, type: Integer do
        # GET /v1/topics/:id/opinions
        desc 'Returns the opinions belonging to the specified topic',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info,
             entity: V1::Entities::Opinion
        # paginate per_page: 20, max_per_page: 100, offset: 0
        params do
          optional :featured_user_id,
                   type: Integer,
                   desc: 'Featured user (his opinions will be in the front)'
          optional :chronological_order,
                   type: Boolean,
                   default: false,
                   desc: 'Display the opinions in chronological order'
          optional :featured_opinion_id,
                   type: Integer,
                   desc: 'Featured opinion (this opinion will be the first)'
        end
        get :opinions do
          topic = Topic.find(params[:id])
          user_id = current_user.try(:id)

          opinions = topic.unlocked_opinions(user_id)
                         .default_ordered(params[:featured_opinion_id], params[:featured_user_id], topic, user_id)

          opinions = opinions.reorder('').featured_first.newest_first if params[:chronological_order]

          # paginated_opinions = paginate(opinions)

          # present paginated_opinions, with: V1::Entities::Opinion,
          #         root: :opinions,
          #         current_user: current_user
        end
      end
    end

    group :users do
      route_param :id, type: Integer do
        # GET /v1/users/:id/locked_opinions
        desc 'Returns the locked opinions of the user',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info,
             entity: V1::Entities::Opinion
        get :locked_opinions do
          user = User.find(params[:id])

          locked_opinions = user.locked_opinions

          present locked_opinions, with: V1::Entities::Opinion,
                  root: :opinions,
                  current_user: current_user
        end
      end

      route_param :id, type: Integer do
        # GET /v1/users/:id/top_videos
        desc 'Returns the top videos of the user',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(false),
             entity: V1::Entities::Opinion
        params do
          optional :limit, type: Integer, desc: 'Max number of results to return', default: 6
        end
        get :top_videos do
          user = User.find(params[:id])

          opinions = user.opinions.unlocked.top_first.limit(params[:limit])

          present opinions, with: V1::Entities::Opinion,
                  root: :opinions,
                  current_user: current_user
        end
      end
    end

    group :me do
      # GET /v1/me/locked_opinions
      desc 'Returns the locked opinions of the current user',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
           entity: V1::Entities::Opinion
      get :locked_opinions do
        locked_opinions = current_user.locked_opinions

        present locked_opinions, with: V1::Entities::Opinion,
                root: :opinions,
                current_user: current_user
      end

      # GET /v1/me/top_videos
      desc 'My top videos',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
           entity: V1::Entities::Opinion
      params do
        optional :limit, type: Integer, desc: 'Max number of results', default: 6
      end
      get :top_videos do
        opinions = current_user.opinions.unlocked.top_first.limit(params[:limit])

        present opinions, with: V1::Entities::Opinion,
                root: :opinions,
                current_user: current_user
      end

      # GET /v1/me/profile_videos
      desc 'My profile videos',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
           entity: V1::Entities::Opinion
      params do
        optional :limit, type: Integer, desc: 'Max number of results to return', default: 100
      end
      get :profile_videos do
        topic = Topic.find_by(profile_topic: true)

        opinions = if topic
                     topic.opinions.where(user: current_user)
                   else
                     []
                   end

        present opinions, with: V1::Entities::Opinion,
                root: :opinions,
                current_user: current_user
      end
    end
  end
end
