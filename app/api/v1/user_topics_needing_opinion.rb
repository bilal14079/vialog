# frozen_string_literal: true
module V1
  class UserTopicsNeedingOpinion < Grape::API
    group :users do
      route_param :id, type: Integer do
        # GET /v1/users/:id/topics_needing_opinion
        desc 'Returns the topics where the given user\'s opinion was requested',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info,
             entity: V1::Entities::TopicWantedForOpinion
        get :topics_needing_opinion do
          user = User.find(params[:id])
          topics = user.topics_needing_opinion_ordered_by_request_count

          present topics, with: V1::Entities::TopicWantedForOpinion,
                          root: :topics,
                          current_user: current_user,
                          profiled_user: user
        end
      end
    end
  end
end
