# frozen_string_literal: true
module V1
  module Helpers
    module DocumentationHelper
      def self.user_auth_headers_info(required = false)
        { V1::Helpers::Authentication::DEVICE_UID_HEADER_KEY => { description: 'Towards app unique device identifier',
                                                                  required: required },
          V1::Helpers::Authentication::SESSION_TOKEN_HEADER_KEY => { description: 'Towards user session token',
                                                                     required: required } }
      end
    end
  end
end
