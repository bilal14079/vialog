# frozen_string_literal: true
module V1
  module Helpers
    module Authentication
      extend Grape::API::Helpers

      SESSION_TOKEN_HEADER_KEY = 'X-Towards-Session-Token'
      DEVICE_UID_HEADER_KEY = 'X-Towards-Device-Uuid'

      def current_session_token
        request.headers[SESSION_TOKEN_HEADER_KEY]
      end

      def current_device_uuid
        request.headers[DEVICE_UID_HEADER_KEY]
      end

      def authenticate_user!
        error! "Missing #{DEVICE_UID_HEADER_KEY}", 403 unless current_session_token
        error! "Invalid #{SESSION_TOKEN_HEADER_KEY}", 403 unless current_user
      end

      def require_facebook_session_token!
        message = "Invalid #{SESSION_TOKEN_HEADER_KEY}, provider is not facebook"
        error! message, 403 unless current_session_ticket.try(:facebook?)
      end

      def current_session_ticket
        SessionToken.find_by(token: current_session_token, device_uid: current_device_uuid)
      end

      def current_user
        current_session_ticket.try(:user)
      end
    end
  end
end
