# frozen_string_literal: true
module V1
  class FacebookFriends < Grape::API
    before do
      authenticate_user!
      require_facebook_session_token!
    end

    group :me do
      # GET /v1/me/facebook_friends
      desc 'Returns the current user\'s facebook friends',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
           entity: V1::Entities::User
      params do
        optional :limit, type: Integer, default: 1000, desc: 'Max number of results to return'
      end
      get :facebook_friends do
        facebook_friend_ids = Facebook::User.new(current_session_ticket.challenge).friend_ids

        users = User.where(facebook_uid: facebook_friend_ids)
                    .ordered
                    .limit(params[:limit])

        present users, with: V1::Entities::User,
                       root: :users,
                       current_user: current_user
      end
    end
  end
end
