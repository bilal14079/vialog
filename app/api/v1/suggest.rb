# frozen_string_literal: true
module V1
  class Suggest < Grape::API
    group :suggest do
      # GET /v1/suggest/topics
      desc 'Suggestions for topics',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info
      params do
        optional :count, type: Integer, default: 5, desc: 'The number of suggestions'
      end
      get :topics do
        topics = Topic.suggestions(current_user, params[:count])

        present topics,
                with: V1::Entities::Topic,
                root: :topics
      end

      # GET /v1/suggest/users
      desc 'Suggestions for users',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info
      params do
        optional :count, type: Integer, default: 5, desc: 'The number of suggestions'
      end
      get :users do
        users = User.suggestions(current_user, params[:count])

        present users,
                with: V1::Entities::User,
                root: :users
      end
    end
  end
end