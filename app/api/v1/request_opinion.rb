# frozen_string_literal: true
require 'twitter/ghost_register'
require 'twitter/ghost_tweeter'

module V1
  class RequestOpinion < Grape::API
    before { authenticate_user! }

    group :users do
      route_param :id, type: Integer do
        # POST /v1/users/:id/request_opinion
        desc 'Request opinion of the given user in a given topic',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
        params do
          requires :topic_id, type: Integer, desc: 'Id of the topic to request the opinion in'
        end
        post :request_opinion do
          user = User.find(params[:id])
          topic = Topic.find(params[:topic_id])
          result = current_user.request_opinion_of_user(user, topic)
          current_user.follow_user(user) if user.ghost
          Twitter::GhostTweeter.send_ghost_tweet(user: user, topic: topic)

          { success: result }
        end
      end

      group :twitter do
        route_param :handle, type: String do
          # POST /v1/users/twitter/:handle/request_opinion
          desc 'Request opinion of the given user in a given topic by twitter handle',
               headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
          params do
            requires :topic_id, type: Integer, desc: 'Id of the topic to request the opinion in'
          end
          post :request_opinion do
            user = User.find_by_twitter(params[:handle])
            topic = Topic.find(params[:topic_id])

            unless user.present?
              user = Twitter::GhostRegister.new(handle: params[:handle]).register
              if user.present?
                GhostMetadatum.create(creator_user: current_user, ghost_user: user, topic: topic, ask_time: Time.now)
                ActionItem.create(actor: current_user, target: user, action_type: ActionItem::ACTION_GHOST_ADD)
              end
            end

            if user.present?
              current_user.request_opinion_of_user(user, topic)
              current_user.follow_user(user) if user.ghost
              Twitter::GhostTweeter.send_ghost_tweet(user: user, topic: topic)

              { success: true }
            else
              { success: false }
            end
          end
        end
      end
    end
  end
end
