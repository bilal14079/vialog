# frozen_string_literal: true
module V1
  class FindAndAsk < Grape::API
    group :topics do
      route_param :id, type: Integer do
        # GET /v1/topics/:id/user_suggestions
        desc 'Get user suggestions in topic',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(false),
             entity: V1::Entities::User
        params do
          optional :limit, type: Integer, default: 5, desc: 'The number of results to return'
        end
        get :user_suggestions do
          topic = Topic.find(params[:id])
          excluded_users = topic.opinions.select(:user_id).distinct.pluck(:user_id)
          excluded_users += [current_user.id] if current_user

          users = User.asked_in_topic(topic).where(guest: false).where.not(id: excluded_users).sort { |u| u.last_request_in_topic(topic) }.reverse

          if current_user
            users = users.reject do |u|
              current_user.requested_opinion_requests.find_by(topic: topic, requested_user: u) != nil
            end
          end

          users = users.first(params[:limit])

          if users.size < params[:limit]
            excluded_users += users.pluck(:id)
            excluded_users += if current_user
                                current_user.requested_opinion_requests.select(:requested_user_id).where(topic: topic).pluck(:requested_user_id)
                              else
                                []
                              end

            users += User.where.not(id: excluded_users, guest: true).order('RANDOM()').limit(params[:limit] - users.size).to_a
          end

          present users, with: V1::Entities::User,
                  current_user: current_user,
                  current_topic: topic,
                  root: :users
        end
      end

      route_param :id, type: Integer do
        # GET /v1/topics/:id/opinion_suggestions
        desc 'Get opinion suggestions in topic',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(false),
             entity: V1::Entities::Opinion
        params do
          optional :followed_limit, type: Integer, default: 5
          optional :followed_watch_limit, type: Integer, default: 5
          optional :random_limit, type: Integer, default: 5
          optional :oldest_limit, type: Integer, default: 5
        end
        get :opinion_suggestions do
          topic = Topic.find(params[:id])

          videos = []

          if current_user
            followed_users = current_user.followed_users.select(:id)
            videos = topic.opinions.unlocked.where(user_id: followed_users).order('RANDOM()').to_a

            followed_watched_videos = OpinionWatch.where(user_id: followed_users).select(:opinion_id)
            videos += Opinion.where.not(id: videos.pluck(:id)).where(id: followed_watched_videos, topic: topic).completion_rate.to_a
          end

          videos += topic.opinions.unlocked.where.not(id: videos.pluck(:id)).order('RANDOM()').to_a
          # videos += topic.opinions.unlocked.where.not(id: videos.pluck(:id)).order(:created_at).to_a

          present videos, with: V1::Entities::Opinion,
                  root: :opinions
        end
      end

      route_param :id, type: Integer do
        # GET /v1/topics/:id/user_search
        desc 'Find users in topic',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(false),
             entity: V1::Entities::User
        params do
          requires :q, type: String, desc: 'Search query'
        end
        get :user_search do
          topic = Topic.find(params[:id])
          excluded_users = topic.opinions.select(:user_id).distinct.pluck(:user_id)
          excluded_users += [current_user.id] if current_user
          all_results = User.where(guest: false).where.not(id: excluded_users).search_by_name_and_handles(params[:q])

          users = []

          if current_user
            asked_users = current_user.asked_in_topic(topic).select(:id)
            users = all_results.where.not(id: asked_users).order_by_ask_count_desc(topic).to_a
          end

          users += all_results.where.not(id: users.pluck(:id)).order_by_ask_count_desc(topic).to_a

          present users, with: V1::Entities::User,
                  current_user: current_user,
                  current_topic: topic,
                  query: params[:q],
                  root: :users
        end
      end

      route_param :id, type: Integer do
        # GET /v1/topics/:id/opinion_search
        desc 'Find opinions in topic',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(false),
             entity: V1::Entities::Opinion
        params do
          requires :q, type: String, desc: 'Search query'
        end
        get :opinion_search do
          topic = Topic.find(params[:id])
          all_results = topic.opinions.where(user_id: User.search_by_name_and_handles(params[:q]).select(:id))

          videos = []

          if current_user
            followed_users = current_user.followed_users.select(:id)
            videos = all_results.where(user_id: followed_users).completion_rate.to_a

            followed_watched_videos = OpinionWatch.where(user_id: followed_users).select(:opinion_id)
            videos += all_results.where.not(id: videos.pluck(:id)).where(id: followed_watched_videos).completion_rate.to_a
          end

          videos += all_results.where.not(id: videos.pluck(:id)).completion_rate.to_a

          present videos, with: V1::Entities::Opinion,
                  query: params[:q],
                  root: :opinions
        end
      end
    end
  end
end