# frozen_string_literal: true

require 'slack-ruby-client'

module V1
  class Report < Grape::API
    group :opinions do
      route_param :id, type: Integer do
        # POST /v1/opinions/:id/report
        desc 'Report specified offensive opinion to moderators',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info
        params do
          requires :reason, type: String, desc: 'Reason string'
        end
        post :report do
          opinion = Opinion.find(params[:id])

          user = current_user
          unless user
            guest_user = User.find_by(guest: true)
            unless guest_user.present?
              guest_user = User.create!(name: 'Guest', profile_picture_url: 'https://twitter.com/askvialog/profile_image?size=normal', guest: true)
            end

            user = guest_user
          end

          offense_report = user.report_opinion(opinion, params[:reason])

          Rails.logger.debug "#{ENV['SLACK_TOKEN']}: #{ENV['SLACK_OFFENSE_CHANNEL']}"

          if ENV['SLACK_OFFENSE_CHANNEL'] && offense_report.valid?
            opinion.reload

            slack_channel = '#' + ENV['SLACK_OFFENSE_CHANNEL']
            slack_message = "New offense report: video *##{params[:id]}* flagged with *#{params[:reason]}* in thread: *#{opinion.topic.title}* (#{opinion.topic.id})\n" +
                "Flagged by: *#{user.name}* (#{user.id}) at *#{Time.now.getutc}*\n" +
                "This is the *#{opinion.offense_reports_count}* on this video by *#{opinion.offense_reports.map {|r| r.user_id}.uniq.count}* number of users."

            slack_message = 'DEVELOPMENT: ' + slack_message unless Rails.env.production?

            slack_client = Slack::Web::Client.new
            slack_client.chat_postMessage(channel: slack_channel, text: slack_message)
          end

          {success: true}
        end
      end
    end
  end
end
