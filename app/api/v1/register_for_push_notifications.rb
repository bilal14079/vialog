# frozen_string_literal: true
module V1
  class RegisterForPushNotifications < Grape::API
    before { authenticate_user! }

    group :push_notifications do
      # POST /v1/push_notifications/register
      desc 'Register for push notification',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
      params do
        requires :notification_type, type: String, desc: 'Notification type'
      end
      post :register do
        current_user.register_for_push_notification(params[:notification_type])

        { success: true }
      end

      # POST /v1/push_notifications/update_token
      desc 'Update the push notification token for all subscribed notifications of the current device',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
      params do
        requires :push_notification_token, type: String, desc: 'Push notification token'
      end
      post :update_token do
        current_user.update_or_create_push_token(current_device_uuid, params[:push_notification_token])

        { success: true }
      end

      # POST /v1/push_notifications/deregister
      desc 'Deregister from push notification',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
      params do
        requires :notification_type, type: String, desc: 'Notification type'
      end
      post :deregister do
        current_user.deregister_from_push_notification(params[:notification_type])

        { success: true }
      end

      # POST /v1/push_notifications/update_player_id
      desc 'Update the player id for all subscribed notifications of the current device',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
      params do
        requires :player_id, type: String, desc: 'OneSignal Player ID'
      end
      post :update_player_id do
        player_id = current_user.player_ids.find_by(device_uid: current_device_uuid)

        if player_id
          player_id.update(player_id: params[:player_id])
        else
          current_user.player_ids.create(player_id: params[:player_id], device_uid: current_device_uuid)
        end

        { success: true }
      end
    end
  end
end
