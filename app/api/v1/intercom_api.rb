# frozen_string_literal: true
require 'intercom_helper'

module V1
  class IntercomApi < Grape::API
    group :intercom do
      # POST /v1/intercom/register_user
      desc 'Register the current user in intercom',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
      post :register_user do
        authenticate_user!

        status = current_user.intercom_status
        intercom_id = current_user.intercom_id

        if status == User::INTERCOM_STATUS_UNREGISTERED
          IntercomHelper.new.register_user(current_user)
        end

        {
            status: status,
            intercom_id: intercom_id
        }
      end

      # POST /v1/intercom/update_attributes
      desc 'Update the intercom attributes of the current user',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
      post :update_attributes do
        authenticate_user!

        if current_user.intercom_status == User::INTERCOM_STATUS_ACTIVE
          IntercomHelper.new.update_attributes(current_user)
        end
      end

      # POST /v1/intercom/webhook
      desc 'Webhook for intercom'
      post :webhook do
        IntercomHelper.handle_notification(params)
      end
    end
  end
end