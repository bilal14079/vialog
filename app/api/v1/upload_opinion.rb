# frozen_string_literal: true
module V1
  class UploadOpinion < Grape::API
    #before { authenticate_user! }

    group :topics do
      route_param :id, type: Integer do
        # POST /v1/topics/:id/upload_opinion
        desc 'Upload an opinion to the given topic',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(false)
        params do
          requires :video, type: File, desc: 'Opinion video'
          optional :lat, type: Float, desc: 'Latitude of the video'
          optional :lng, type: Float, desc: 'Longitude of the video'
          optional :quality, type: String, desc: 'Quality of the uploaded video. Set to \'low\' for low quality upload'
          optional :device_id, type: String, desc: 'The device id of the uploading device'
        end
        post :upload_opinion do
          topic = Topic.find(params[:id])

          user = current_user

          unless user
            guest_user = User.find_by(guest: true)
            unless guest_user.present?
              guest_user = User.create!(name: 'Guest', profile_picture_url: 'https://twitter.com/askvialog/profile_image?size=normal', guest: true)
            end

            user = guest_user
          end

          quality = if params[:quality] == 'low'
                      'sd'
                    else
                      'hd'
                    end

          # opinion = topic.opinions.create!(user: user, video: params[:video], lat: params[:lat], lng: params[:lng], device_id: params[:device_id], quality: quality)
          opinion = Opinion.create!(topic: topic, user: user, video: params[:video], lat: params[:lat], lng: params[:lng], device_id: params[:device_id], quality: quality)
          ActionItem.create(actor: current_user, target: opinion, action_type: ActionItem::ACTION_VIDEO_UPLOAD)

          { opinion_id: opinion.id, success: true }
        end
      end
    end

    group :opinions do
      route_param :id, type: Integer do
        # POST /v1/opinions/:id/reupload
        desc 'Upload a new video for an existing opinion',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
        params do
          requires :video, type: File, desc: 'The new video for the opinion'
          optional :device_id, type: String, desc: 'The device id of the uploading device'
        end
        post :reupload do
          opinion = Opinion.find(params[:id])

          if opinion.user == current_user || (opinion.user.guest && opinion.device_id.present? && opinion.device_id == params[:device_id])
            opinion.quality = 'hd'
            opinion.update_attribute(:video, params[:video])
          end

          { success: true }
        end
      end
    end

    group :me do
      # POST /v1/me/upload_profile_video
      desc 'Upload a video as my profile video',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
      params do
        requires :video, type: File, desc: 'Profile video'
        optional :lat, type: Float, desc: 'Latitude of the video'
        optional :lng, type: Float, desc: 'Longitude of the video'
        optional :quality, type: String, desc: 'Quality of the uploaded video. Set to \'low\' for low quality upload'
        optional :device_id, type: String, desc: 'The device id of the uploading device'
      end
      post :upload_profile_video do
        if current_user
          topic = Topic.find_by(profile_topic: true)

          unless topic
            category = Category.create(name: 'global_profile')

            topic = Topic.create!(title: 'profile_topic', profile_topic: true, category: category)
          end

          quality = if params[:quality] == 'low'
                      'sd'
                    else
                      'hd'
                    end

          # opinion = topic.opinions.create!(user: current_user, video: params[:video], lat: params[:lat], lng: params[:lng], device_id: params[:device_id], quality: quality)
          opinion = Opinion.create!(topic: topic, user: current_user, video: params[:video], lat: params[:lat], lng: params[:lng], device_id: params[:device_id], quality: quality)

          current_user.update(profile_video: opinion)

          { opinion_id: opinion.id, success: true }
        else
          { success: false }
        end
      end
    end
  end
end
