# frozen_string_literal: true

require 'notification_batcher'

module V1
  class Notifications < Grape::API
    include Grape::Kaminari
    before { authenticate_user! }

    group :me do
      # GET /v1/me/notifications
      desc 'Returns the current user\'s notifications',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
           entity: V1::Entities::Notification
      params do
        optional :type, type: String, values: Notification::NOTIFICATION_TYPES, desc: 'Notification type'
      end
      # paginate per_page: 20, max_per_page: 100, offset: 0
      get :notifications do
        user_notifications = current_user.notifications

        notifications_to_delete = user_notifications.select { |noti| noti.subject.nil? }.map(&:id)
        Notification.where(id: notifications_to_delete).destroy_all

        notifications = user_notifications
                            .active_types
                            .by_notification_type(params[:type])
                            .select { |noti| noti.subject != nil }

        updates = notifications.inject({}) do |h, notif|
          h[notif.id] = { :read => true }
          h
        end

        # paginated_notifications = paginate(Kaminari.paginate_array(notifications))

        Notification.update(updates.keys, updates.values)

        # present paginated_notifications, with: V1::Entities::Notification,
        #         root: :notifications,
        #         current_user: current_user
      end

      group :notifications do
        # GET /v1/me/notifications/batched
        desc 'Returns the current user\'s notifications after batching',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
             entity: V1::Entities::NotificationBatch
        params do
          optional :type, type: String, values: Notification::NOTIFICATION_TYPES, desc: 'notification type'
        end
        # paginate per_page: 20, max_per_page: 100, offset: 0
        get :batched do
          user_notifications = current_user.notifications

          notifications_to_delete = user_notifications.select { |noti| noti.subject.nil? }.map(&:id)
          Notification.where(id: notifications_to_delete).destroy_all

          notifications = user_notifications
                              .active_types
                              .by_notification_type(params[:type])
                              .select { |noti| noti.subject != nil }

          updates = notifications.inject({}) do |h, notif|
            h[notif.id] = { :read => true }
            h
          end

          notifications = NotificationBatcher.batch_notifications(notifications).sort_by { |n| n.created_at }.reverse

          # paginated_notifications = paginate(Kaminari.paginate_array(notifications))

          Notification.update(updates.keys, updates.values)

          # present paginated_notifications, with: V1::Entities::NotificationBatch,
          #         root: :notifications,
          #         current_user: current_user
        end

        # GET /v1/me/notifications/unread_count
        desc 'Returns the count of the current user\'s unread notifications',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
        get :unread_count do
          count = current_user.notifications.active_types.where(read: false).count

          { count: count }
        end
      end
    end
  end
end
