# frozen_string_literal: true
module V1
  class Logout < Grape::API
    before { authenticate_user! }

    group :logout do
      # POST /v1/logout
      desc 'Log the current user out',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
      post do
        current_user.logout_device(current_device_uuid)

        { success: true }
      end
    end
  end
end
