# frozen_string_literal: true
module V1
  module Entities
    class GroupedOpinion < Topic
      expose :opinions,
             as: 'profiled_user_opinions',
             using: V1::Entities::Opinion,
             documentation: { type: 'V1::Entites::Opinion',
                              array: true,
                              desc: 'The users opinions in this topic' } do |topic, options|
        topic.opinions.unlocked.where(user: options[:user]).newest_first
      end
    end
  end
end