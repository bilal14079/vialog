# frozen_string_literal: true

module V1
  module Entities
    class UserSetting < Grape::Entity
      expose :key,
             documentation: { type: 'String',
                              desc: 'The key of the setting' }
      expose :value,
             documentation: { type: 'String',
                              desc: 'The value of the setting' }
    end
  end
end