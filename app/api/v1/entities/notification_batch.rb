# frozen_string_literal: true
module V1
  module Entities
    class NotificationBatch < Grape::Entity
      expose :created_at,
             documentation: { type: 'DateTime',
                              desc: 'Creation date of the notification' }
      expose :notification_type,
             documentation: { type: 'String',
                              desc: 'Type of the notification' }
      expose :target_id,
             documentation: { type: 'Integer',
                              desc: 'Target id' } do |notification|
        notification.target.id
      end

      expose :target_class,
             documentation: { type: 'String',
                              desc: 'Target class' } do |notification|
        notification.target.class.name
      end

      expose :opinion_id,
             documentation: { type: 'Integer',
                              desc: 'Opinion id' }

      expose :notification_message,
             as: :message,
             documentation: { type: 'String',
                              desc: 'Message of the notification' }

      expose :notification_long_message,
             as: :long_message,
             documentation: { type: 'String',
                              desc: 'Long message of the notification' }

      expose :notification_read_text,
             as: :read_message,
             documentation: { type: 'String',
                              desc: 'Highlighted message of the read notification' }

      expose :notification_unread_text,
             as: :unread_message,
             documentation: { type: 'String',
                              desc: 'Highlighted message of the unread notification' }

      expose :read,
             documentation: { type: 'Boolean',
                              desc: 'True if the notification have been read before' }

      expose :opinion,
             using: V1::Entities::Opinion,
             documentation: { type: 'V1::Entities::Opinion',
                              desc: 'Opinion' }

      expose :thumbnail,
             documentation: { type: 'String',
                              desc: 'The thumbnail url to be shown' }

      # FacebookFriendJoinedNotification
      expose :facebook_friend,
             as: :user,
             if: ->(notification, _options) { notification.facebook_friend.present? },
             using: V1::Entities::User,
             documentation: { type: 'V1::Entities::User',
                              desc: 'User who joined' }

      # UserFollowedNotification
      expose :follower,
             as: :user,
             if: ->(notification, _options) { notification.follower.present? },
             using: V1::Entities::User,
             documentation: { type: 'V1::Entities::User',
                              desc: 'Follower user' }

      # TranscodingDoneNotification
      expose :opinion_video_thumbnail_url,
             if: ->(notification, _options) { notification.opinion_video_thumbnail_url.present? },
             documentation: { type: 'String',
                              desc: 'Thumbnail url of the opinion' }
      expose :opinioned_topic,
             as: :topic,
             if: ->(notification, _options) { notification.opinioned_topic.present? },
             using: V1::Entities::Topic,
             documentation: { type: 'V1::Entities::Topic',
                              desc: 'Topic of the transcoded opinion' }

      # ReactionReceivedNotification
      expose :reacting_users,
             as: :users,
             if: ->(notification, _options) { notification.reacting_users.present? },
             using: V1::Entities::User,
             documentation: { type: 'V1::Entities::User',
                              array: true,
                              desc: 'The users who reacted to the topic' }

      expose :reacted_topic,
             as: :topic,
             if: ->(notification, _opinions) { notification.reacted_topic.present? },
             using: V1::Entities::Topic,
             documentation: { type: 'V1::Entities::Topic',
                              desc: 'Topic the reaction was received in' }

      # RequestReceivedNotification
      expose :requester_users,
             as: :users,
             if: ->(notification, _options) { notification.requester_users.present? },
             using: V1::Entities::User,
             documentation: { type: 'V1::Entitties::User',
                              array: true,
                              desc: 'Users who requested' }

      expose :requested_topic,
             as: :topic,
             if: ->(notification, _options) { notification.requested_topic.present? },
             using: V1::Entities::Topic,
             documentation: { type: 'V1::Entitties::Topic',
                              desc: 'Topic the request was made in' }

      expose :ask_data,
             as: :asks,
             if: ->(notification, _options) { notification.ask_data.present? },
             using: V1::Entities::OpinionRequests,
             documentation: { type: 'V1::Entities::OpinionRequests',
                              desc: 'The current user\'s requests in the topic' }

      # RequestAnsweredNotification
      # expose :requested_users,
      #        as: :users,
      #        if: ->(notification, _options) { notification.requested_users.present? },
      #        using: V1::Entities::User,
      #        documentation: { type: 'V1::Entities::User',
      #                         array: true,
      #                         desc: 'Users who were requested' }
      expose :requested_user,
             as: :user,
             if: ->(notification, _options) { notification.requested_user.present? },
             using: V1::Entities::User,
             documentation: { type: 'V1::Entities::User',
                              desc: 'The user who answered in the topic' }

      expose :interesting_topic,
             as: :topic,
             if: ->(notification, _options) { notification.interesting_topic.present? },
             using: V1::Entities::Topic,
             documentation: { type: 'V1::Entities::Topic',
                              desc: 'The topic the request was answered in' }

      # OpinionLockedNotification
      expose :locked_opinion,
             as: :opinion,
             if: ->(notification, _options) { notification.locked_opinion.present? },
             using: V1::Entities::Opinion,
             documentation: { type: 'V1::Entities::Opinion',
                              desc: 'The locked opinion' }

      # HostedTopicNewVideoNotification
      expose :hosted_topic,
             as: :topic,
             if: ->(notification, _options) { notification.hosted_topic.present? },
             using: V1::Entities::Topic,
             documentation: { type: 'V1::Entities::Topic',
                              desc: 'The host topic' }

      expose :uploader_user,
             as: :user,
             if: ->(notification, _options) { notification.uploader_user.present? },
             using: V1::Entities::User,
             documentation: { type: 'V1::Entities::User',
                              desc: 'The user who uploaded to the hosted topic' }
    end
  end
end