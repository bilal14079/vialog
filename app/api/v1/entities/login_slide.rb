# frozen_string_literal: true
module V1
  module Entities
    class LoginSlide < Grape::Entity
      expose :display_order,
             documentation: { type: 'Integer',
                              desc: 'The position where it should be displayed' }
      expose :image_url,
             documentation: { type: 'String',
                              desc: 'The url of the image' } do |slide, _options|
        slide.image.url
      end
      expose :text,
             documentation: { type: 'String',
                              desc: 'The original text to be displayed' }
      expose :html_text,
             documentation: { type: 'String',
                              desc: 'HTML formatted text to be displayed' }
      expose :dot_color,
             documentation: { type: 'String',
                              desc: 'The color to be used for the dot' }
      expose :animated,
             documentation: { type: 'Boolean',
                              desc: 'True if the image should be animated' }
      expose :looped,
             documentation: { type: 'Boolean',
                              desc: 'True if the image should be looped' }
    end
  end
end