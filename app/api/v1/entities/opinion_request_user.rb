# frozen_string_literal: true
module V1
  module Entities
    class OpinionRequestUser < Grape::Entity
      expose :user,
             using: V1::Entities::User,
             documentation: { type: 'V1::Entities::User',
                              desc: 'The user who made the opinion request' }
      expose :timestamp,
             documentation: { type: 'DateTime',
                              desc: 'The time the request has been made' }
      expose :last_opinion,
             using: V1::Entities::Opinion,
             documentation: { type: 'V1::Entities::Opinion',
                              desc: 'The last opinion of the user in the given topic' }
    end
  end
end