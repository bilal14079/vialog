# frozen_string_literal: true
module V1
  module Entities
    class SessionToken < Grape::Entity
      expose :token,
             documentation: {type: 'String',
                             desc: 'Session token of the user.'}
      expose :provider,
             documentation: {type: 'String',
                             desc: 'Token provider (facebook/twitter).'}

      # TODO: use expose :user, merge: true when we upgrade to grape-entity 0.5.0+

      expose :user_id,
             as: :id,
             documentation: {type: Integer,
                             desc: 'Id of the user.'}
      expose :user_name,
             as: :name,
             documentation: {type: 'String',
                             desc: 'Name of the user.'}
      expose :user_twitter_handle,
             as: :twitter_handle,
             documentation: {type: 'String',
                             desc: 'Twitter handle of the user.'}
      expose :user_profile_picture_url,
             as: :profile_picture_url,
             documentation: {type: 'String',
                             desc: 'Profile picture url of the user.'}
      expose :user_email,
             as: :email,
             documentation: {type: 'String',
                             desc: 'Email address of the user.'}
    end
  end
end
