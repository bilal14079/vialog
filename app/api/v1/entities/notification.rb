# frozen_string_literal: true
module V1
  module Entities
    class Notification < Grape::Entity
      expose :created_at,
             documentation: { type: 'DateTime',
                              desc: 'Creation date of the notification' }
      expose :notification_type,
             documentation: { type: 'String',
                              desc: 'Type of the notification' }
      expose :target_id,
             documentation: { type: 'Integer',
                              desc: 'Target id' } do |notification|
        notification.target.id
      end

      expose :target_class,
             documentation: { type: 'String',
                              desc: 'Target class' } do |notification|
        notification.target.class.name
      end

      expose :push_opinion_id,
             as: :opinion_id,
             documentation: { type: 'Integer',
                              desc: 'Opinion id' }

      expose :push_notification_message,
             as: :message,
             documentation: { type: 'String',
                              desc: 'Message of the notification' }

      expose :notification_long_text,
             as: :long_message,
             documentation: { type: 'String',
                              desc: 'Long message of the notification' }

      expose :notification_read_text,
             as: :read_message,
             documentation: { type: 'String',
                              desc: 'Highlighted message of the read notification' }

      expose :notification_unread_text,
             as: :unread_message,
             documentation: { type: 'String',
                              desc: 'Highlighted message of the unread notification' }

      expose :read,
             as: :read,
             documentation: { type: 'Boolean',
                              desc: 'True if the notification have been read before' }

      expose :push_opinion,
             as: :opinion,
             using: V1::Entities::Opinion,
             documentation: { type: 'V1::Entities::Opinion',
                              desc: 'Opinion' }

      # FacebookFriendJoinedNotification
      expose :subject,
             as: :user,
             if: ->(instance, _options) { instance.is_a?(FacebookFriendJoinedNotification) },
             using: V1::Entities::User,
             documentation: { type: 'V1::Entities::User',
                              desc: 'User who joined' }

      # UserFollowedNotification
      expose :follower,
             as: :user,
             if: ->(instance, _options) { instance.is_a?(UserFollowedNotification) },
             using: V1::Entities::User,
             documentation: { type: 'V1::Entities::User',
                              desc: 'Follower user' }

      # TranscodingDoneNotification
      expose :opinion_video_thumbnail_url,
             if: ->(instance, _options) { instance.is_a?(TranscodingDoneNotification) },
             documentation: { type: 'String',
                              desc: 'Thumbnail url of the opinion' }
      expose :opinioned_topic,
             as: :topic,
             if: ->(instance, _options) { instance.is_a?(TranscodingDoneNotification) },
             using: V1::Entities::Topic,
             documentation: { type: 'V1::Entities::Topic',
                              desc: 'Topic of the transcoded opinion' }

      # ReactionReceivedNotification
      with_options if: ->(instance, _options) { instance.is_a?(ReactionReceivedNotification) } do
        expose :reacting_user,
               as: :user,
               using: V1::Entities::User,
               documentation: { type: 'V1::Entities::User',
                                desc: 'User who reacted' }

        expose :reacted_topic,
               as: :topic,
               using: V1::Entities::Topic,
               documentation: { type: 'V1::Entities::Topic',
                                desc: 'Topic the reaction was received in' }
      end

      # RequestReceivedNotification
      with_options if: ->(instance, _options) { instance.is_a?(RequestReceivedNotification) } do
        expose :requester_user,
               as: :user,
               using: V1::Entities::User,
               documentation: { type: 'V1::Entities::User',
                                desc: 'User who requested' }
        expose :requested_topic,
               as: :topic,
               using: V1::Entities::Topic,
               documentation: { type: 'V1::Entities::Topic',
                                desc: 'Topic the request was made in' }
      end

      # RequestAnsweredNotification
      with_options if: ->(instance, _options) { instance.is_a?(RequestAnsweredNotification) } do
        expose :requested_user,
               as: :user,
               using: V1::Entities::User,
               documentation: { type: 'V1::Entities::User',
                                desc: 'User who was requested' }
        expose :interesting_topic,
               as: :topic,
               using: V1::Entities::Topic,
               documentation: { type: 'V1::Entities::Topic',
                                desc: 'Topic the request was answered in' }
      end

      # OpinionLockedNotification
      with_options if: ->(instance, _opptions) { instance.is_a?(OpinionLockedNotification) } do
        expose :subject,
               as: :opinion,
               using: V1::Entities::Opinion,
               documentation: { type: 'V1::Entities::Opinion',
                                desc: 'The locked opinion' }

        # HostedTopicNewVideoNotification
        with_options if: ->(instance, _options) { instance.is_a?(HostedTopicNewVideoNotification) } do
          expose :uploader_user,
                 as: :user,
                 using: V1::Entities::User,
                 documentation: { type: 'V1::Entities::User',
                                  desc: 'The user who uploaded to the hosted topic' }

          expose :hosted_topic,
                 as: :topic,
                 using: V1::Entities::User,
                 documentation: { type: 'V1::Entities::Topic',
                                  desc: 'The hosted topic' }
        end
      end
    end
  end
end
