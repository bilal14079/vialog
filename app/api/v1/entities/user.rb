# frozen_string_literal: true
module V1
  module Entities
    class User < Grape::Entity
      expose :id,
             documentation: { type: 'Integer',
                              desc: 'Id of the user.' }

      expose :name,
             documentation: { type: 'String',
                              desc: 'Name of the user.' } do |user|
        if user.handle
          user.handle
        else
          user.name
        end
      end
      expose :highlighted_name,
             documentation: { type: 'String',
                              desc: 'Name of the user with the searched part highlighted' },
             if: ->(_instance, options) { options[:query] } do |user|
        if user.name
          formatter = lambda { |text| "<span style='font-family: SFUIText-Bold; font-weight: bolder; color: #00f0cf'>#{text}</span>" }

          "<span style='color: white; font-family: SFUIText-Regular; font-size: larger'>#{TextHighlighter.new(user.name, options[:query]).highlight(formatter)}</span>"
        end
      end
      expose :handle,
             documentation: { type: 'String',
                              desc: 'Handle of the user.' }
      expose :highlighted_handle,
             documentation: { type: 'String',
                              desc: 'Handle of the user with the searched part highlighted' },
             if: ->(instance, options) { instance.handle && options[:query] } do |user|
        formatter = lambda { |text| "<span style='font-family: SFUIText-Bold; font-weight: bolder; color: #00f0cf'>#{text}</span>" }

        "<span style='color: white; font-family: SFUIText-Regular; font-size: larger'>#{TextHighlighter.new(user.handle, options[:query]).highlight(formatter)}</span>"
      end
      expose :bio,
             documentation: { type: 'String',
                              desc: 'The bio of the user' }
      expose :profile_picture_url,
             documentation: { type: 'String',
                              desc: 'Profile picture url of the user.' }
      expose :profile_video,
             using: V1::Entities::UserlessOpinion,
             documentation: { type: 'V1::Entities::UserlessOpinion',
                              desc: 'The profile video of the user' }
      expose :twitter_handle,
             documentation: { type: 'String',
                              desc: 'Twitter handle of the user.' }
      expose :highlighted_twitter_handle,
             documentation: { type: 'String',
                              desc: 'Twitter handle of the user with the searched part highlighted' },
             if: ->(_instance, options) { options[:query] } do |user|
        if user.twitter_handle
          formatter = lambda { |text| "<span style='font-family: SFUIText-Bold; font-weight: bolder; color: #00f0cf'>#{text}</span>" }

          "<span style='color: white; font-family: SFUIText-Regular; font-size: larger'>#{TextHighlighter.new(user.twitter_handle, options[:query]).highlight(formatter)}</span>"
        end
      end
      expose :is_current_user,
             documentation: { type: 'Boolean',
                              desc: 'Is the user the current user?' },
             if: ->(_instance, options) { options[:current_user] } do |user|
        user == options[:current_user]
      end
      expose :followed_by_current_user,
             documentation: { type: 'Boolean',
                              desc: 'Is the current user following the user?' },
             if: ->(_instance, options) { options[:current_user] } do |user|
        user.followed_by_user?(options[:current_user])
      end
      expose :total_ask_count,
             documentation: { type: 'Integer',
                              desc: 'The number of asks this user received' } do |user|
        OpinionRequest.where(requested_user: user).count
      end
      expose :asks_in_topic,
             documentation: { type: 'Integer',
                              desc: 'The number of asks this user received in the current_topic' },
             if: ->(_instance, options) { options[:current_topic] } do |user|
        user.received_opinion_requests.where(topic: options[:current_topic]).count
      end
      expose :already_asked,
             documentation: { type: 'Boolean',
                              desc: 'Did the current user already asked him in this topic?' },
             if: ->(_instance, options) { options[:current_user] && options[:current_topic] } do |user|
        options[:current_user].requested_opinion_requests.where(topic: options[:current_topic], requested_user: user).count > 0
      end
      expose :ghost,
             as: :is_ghost,
             documentation: { type: 'Boolean',
                              desc: 'Is this user a ghost profile?' }
      expose :guest,
             as: :is_guest,
             documentation: { type: 'Boolean',
                              desc: 'Is this the guest profile?' }

      expose :intercom_status,
             documentation: { type: 'String',
                              desc: 'The intercom status of the user' }

      expose :intercom_id,
             documentation: { type: 'String',
                              desc: 'The id of the user used in intercom' }
    end
  end
end
