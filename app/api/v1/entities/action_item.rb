# frozen_string_literal: true
module V1
  module Entities
    class ActionItem < Grape::Entity
      expose :id
      expose :actor,
             as: :acting_user,
             using: V1::Entities::User,
             documentation: { type: 'V1::Entities::User',
                              desc: 'The user who created the action' }
      expose :action_type,
             as: :type,
             documentation: { type: 'String',
                              desc: 'The type of the action' }

      expose :created_at,
             as: :timestamp,
             documentation: { type: 'DateTime',
                              desc: 'The timestamp of the action' }

      # user follow
      expose :followed_user,
             using: V1::Entities::User,
             documentation: { type: 'V1::Entities::User',
                              desc: 'The user who has been followed' },
             if: ->(instance, _options) { instance.action_type == ::ActionItem::ACTION_USER_FOLLOW } do |instance, _options|
        instance.target
      end

      # ask
      expose :asked_user,
             documentation: { type: 'V1::Entities::User',
                              desc: 'The user who has been asked' },
             if: ->(instance, _options) { instance.action_type == ::ActionItem::ACTION_ASK } do |instance, options|
        User.represent(instance.target.requested_user, options.merge(current_topic: instance.target.topic))
      end
      expose :asked_topic,
             using: V1::Entities::Topic,
             documentation: { type: 'V1::Entities::Topic',
                              desc: 'The topic where `asked_user` have been asked in' },
             if: ->(instance, _options) { instance.action_type == ::ActionItem::ACTION_ASK } do |instance, _options|
        instance.target.topic
      end

      # topic follow
      expose :followed_topic,
             using: V1::Entities::Topic,
             documentation: { type: 'V1::Entities::Topic',
                              desc: 'The topic followed by `acting_user`' },
             if: ->(instance, _options) { instance.action_type == ::ActionItem::ACTION_TOPIC_FOLLOW } do |instance, _options|
        instance.target
      end

      # ghost add
      expose :added_ghost,
             using: V1::Entities::User,
             documentation: { type: 'V1::Entities::User',
                              desc: 'The ghost who has been added by `acting_user`' },
             if: ->(instance, _options) { instance.action_type == ::ActionItem::ACTION_GHOST_ADD } do |instance, _options|
        instance.target
      end

      # video upload
      expose :uploaded_opinion,
             using: V1::Entities::Opinion,
             documentation: { type: 'V1::Entities::Opinion',
                              desc: 'The uploaded opinion' },
             if: ->(instance, _options) { instance.action_type == ::ActionItem::ACTION_VIDEO_UPLOAD } do |instance, _options|
        instance.target
      end
    end
  end
end