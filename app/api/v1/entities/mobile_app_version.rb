# frozen_string_literal: true
module V1
  module Entities
    class MobileAppVersion < Grape::Entity
      expose :platform,
             documentation: { type: 'String',
                              desc: 'Mobile app platform.' }
      expose :update_severity,
             as: :update_type,
             documentation: { type: 'String',
                              desc: 'Version update type.' }
      expose :version_identifier,
             as: :last_version,
             documentation: { type: 'String',
                              desc: 'Mobile app last version.' }
    end
  end
end
