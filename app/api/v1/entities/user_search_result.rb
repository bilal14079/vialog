# frozen_string_literal: true

require 'text_highlighter'

module V1
  module Entities
    class UserSearchResult < User
      expose :highlighted_name,
             documentation: {type: 'String',
                             desc: 'Name of the user with the searched part highlighted'} do |user, options|
        if user.name
          formatter = lambda { |text| "<span style='font-family: SFUIText-Bold; font-weight: bolder; color: #00f0cf'>#{text}</span>" }

          "<span style='color: white; font-family: SFUIText-Regular; font-size: larger'>#{TextHighlighter.new(user.name, options[:query]).highlight(formatter)}</span>"
        end
      end

      expose :highlighted_twitter_handle,
             documentation: {type: 'String',
                             desc: 'Twitter handle of the user with the searched part highlighted'} do |user, options|
        if user.twitter_handle
          formatter = lambda { |text| "<span style='font-family: SFUIText-Bold; font-weight: bolder; color: #00f0cf'>#{text}</span>" }

          "<span style='color: white; font-family: SFUIText-Regular; font-size: larger'>#{TextHighlighter.new(user.twitter_handle, options[:query]).highlight(formatter)}</span>"
        end
      end
    end
  end
end