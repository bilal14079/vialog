# frozen_string_literal: true
module V1
  module Entities
    class UserlessOpinion < Grape::Entity
      expose :id,
             documentation: {type: 'Integer',
                             desc: 'Id of the opinion.'}
      expose :created_at,
             as: :uploaded_at,
             documentation: {type: 'DateTime',
                             desc: 'Uploaded at timestamp of the opinion.'}
      expose :topic,
             using: V1::Entities::Topic,
             documentation: {type: 'V1::Entities::Topic',
                             desc: 'Topic the opinion belongs to.'}
      expose :reactions,
             documentation: {type: 'Object',
                             desc: 'Overall reaction counts for the opinion.'}
      expose :video_thumbnail_url,
             as: :thumbnail_url,
             documentation: {type: 'String',
                             desc: 'Url of the opinion thumbnail.'}
      expose :video_hls_url,
             as: :video_url,
             documentation: {type: 'String',
                             desc: 'Url of the opinion video.'}
      expose :token,
             documentation: {type: 'String',
                             desc: 'Token of the opinion.'}
      expose :duration,
             documentation: {type: 'Decimal',
                             desc: 'Duration of the opinion video in seconds.'}
      expose :watch_time,
             documentation: {type: 'Integer',
                             desc: 'Number of seconds while the opinion was playing'}
      expose :is_locked,
             documentation: {type: 'Boolean',
                             desc: 'True if the opinion is locked'} do |opinion|
        opinion.locked?
      end
    end
  end
end
