# frozen_string_literal: true
module V1
  module Entities
    class TopicProfiled < Topic
      expose :profiled_user_opinions,
             using: V1::Entities::Opinion,
             documentation: { type: 'V1::Entities::Opinion',
                              desc: 'Opinions of the profiled user in the topic.' },
             if: ->(_instance, options) { options[:profiled_user] } do |topic|
        options[:profiled_user].opinions
                               .where(topic: topic)
                               .newest_first
      end
    end
  end
end
