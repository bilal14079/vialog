# frozen_string_literal: true
module V1
  module Entities
    class Topic < Grape::Entity
      expose :id,
             documentation: { type: 'Integer',
                              desc: 'ID of the topic.' }
      expose :title,
             documentation: { type: 'String',
                              desc: 'Title of the topic.' }
      expose :category,
             using: V1::Entities::Category,
             documentation: { type: 'V1::Entities::Category',
                              desc: 'Topic category.' }
      expose :hashtags,
             using: V1::Entities::Hashtag,
             documentation: { type: 'V1::Entities::Hashtag',
                              array: true,
                              desc: 'Topic hashtags.' }
      expose :opinions_count,
             documentation: { type: 'Integer',
                              desc: 'Number of opinions uploaded to the topic' } do |topic, _options|
        topic.opinions.unlocked.count
      end
      expose :own_video_count,
             documentation: { type: 'Integer',
                              desc: 'The number of videos uploaded by the current user' } do |topic, options|
        if options[:current_user]
          topic.opinions_by_user(options[:current_user]).count
        else
          0
        end
      end
      expose :opinion_request_count,
             documentation: { type: 'Integer',
                              desc: 'The number of opinion requests in the topic' } do |topic, _options|
        topic.opinion_requests.count
      end
      expose :own_request_count,
             documentation: { type: 'Integer',
                              desc: 'The number of opinion requests in the topic by the current user' } do |topic, options|
        if options[:current_user]
          topic.opinion_requests_by_user(options[:current_user]).count
        else
          0
        end
      end
      expose :request_count_for_me,
             documentation: { type: 'Integer',
                              desc: 'The number of opinion request the current user has received' } do |topic, options|
        if options[:current_user]
          topic.opinion_requests.where(requested_user: options[:current_user]).count
        else
          0
        end
      end
      expose :follower_count,
             documentation: { type: 'Integer',
                              desc: 'The number of users following this topic' } do |topic, _options|
        topic.follower_users.count
      end
      expose :own_follower_count,
             documentation: { type: 'Integer',
                              desc: 'The number of users I\'m following who follow this topic' } do |topic, options|
        if options[:current_user]
          topic.follower_users.select { |u| options[:current_user].followed_users.include?(u) }.count
        else
          0
        end
      end
      expose :featured_opinion_thumbnails,
             documentation: { type: 'String',
                              array: true,
                              desc: 'The ordered thumbnails of featured opinions' },
             if: ->(_instance, options) { options[:include_thumbnails] } do |topic, options|
        # topic.featured_opinions.default_ordered(nil, nil, topic, options[:current_user].try(:id)).map do |opinion|
        #   { opinion_id: opinion.id, thumbnail_url: opinion.video_thumbnail_url }
        # end
        []
      end
      expose :opinion_thumbnails,
             documentation: { type: 'String',
                              array: true,
                              desc: 'The ordered thumbnails of non-featured opinions' },
             if: ->(_instance, options) { options[:include_thumbnails] } do |topic, _options|
        # topic.non_featured_opinions.default_ordered(nil, nil, topic, options[:current_user].try(:id)).map do |opinion|
        #   { opinion_id: opinion.id, thumbnail_url: opinion.video_thumbnail_url }
        # end
        []
      end
      expose :thumbnail_url,
             documentation: { type: 'String',
                              desc: 'Thumbnail url of the topic' }

      expose :followed_by_current_user,
             documentation: { type: 'Boolean',
                              desc: 'Is the current user following the topic?' },
             if: ->(_instance, options) { options[:current_user] } do |topic|
        topic.followed_by_user?(options[:current_user])
      end
      expose :password_hash,
             documentation: { type: 'String',
                              desc: 'MD5 hashed password for accessing the topic. If the topic is not protected it\'s null' }
      expose :watch_time,
             documentation: { type: 'Integer',
                              desc: 'Number of seconds while the opinions in this topic were playing' }
      expose :new_opinion_count,
             documentation: { type: 'Integer',
                              desc: 'The number of opinions uploaded since the user last opened the topic' } do |topic, options|
        if options[:current_user]
          topic.new_opinion_count(options[:current_user])
        else
          0
        end
      end

      expose :my_response_id,
             documentation: { type: 'Integer',
                              desc: 'The opinion id of the current players first response' },
             if: ->(_instance, options) { options[:current_user] } do |topic, options|
        topic.opinions.unlocked.where(user: options[:current_user]).order(:created_at).first.try(:id)
      end

      expose :clap_count_live,
             as: :clap_count,
             documentation: { type: 'Integer',
                              desc: 'The total number of claps in this topic' }

      expose :creator,
             as: :host,
             documentation: { type: 'V1::Entities::User',
                              desc: 'The user who created the topic' }
    end
  end
end
