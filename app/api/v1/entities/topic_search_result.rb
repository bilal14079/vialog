# frozen_string_literal: true
module V1
  module Entities
    class TopicSearchResult < Topic
      expose :highlighted_title,
             documentation: { type: 'String',
                              desc: 'Title of the topic with the query highlighted' } do |topic, options|
        title = topic.title.dup
        q = options[:query]
        q_lower = q.downcase
        li = 0
        while title.downcase.index(q_lower, li)
          i = title.downcase.index(q_lower, li)
          r = i..(i + q.length - 1)
          sub = "<span style='font-family: SFUIText-Bold; font-weight: bolder; color: #00f0cf'>#{title[r]}</span>"
          title[r] = sub
          li = i + sub.length
        end
        "<span style='color: white; font-family: SFUIText-Regular; font-size: larger'>#{title}</span>"
      end

      expose :found_keywords,
             documentation: { type: 'String',
                              array: true,
                              desc: 'The keywords which match the search term' } do |topic, options|
        q_lower = options[:query].downcase

        topic.keyword_list.select { |w| w.include?(q_lower)}
      end
    end
  end
end