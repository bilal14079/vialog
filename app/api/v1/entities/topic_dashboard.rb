# frozen_string_literal: true
module V1
  module Entities
    class TopicDashboard < Grape::Entity
      expose :id,
             documentation: {type: 'Integer',
                             desc: 'ID of the topic dashboard.'}
      expose :title,
             documentation: {type: 'String',
                             desc: 'Title of the topic dashboard (only relevant if banner).'}
      expose :main,
             documentation: {type: 'Boolean',
                             desc: 'True if the topic dashboard is the main dashboard, false otherwise.'}
      expose :local_topics,
             if: -> (_, options) { options[:local_topics] },
             using: V1::Entities::Topic,
             documentation: {type: 'Topic',
                             array: true,
                             desc: 'Topics near the user'} do |_, options|
        options[:local_topics].sort_by { |t| t.newest_opinion_upload_time }.reverse
      end
      # expose :topics_followed_by_current_user,
      #        using: V1::Entities::Topic,
      #        documentation: {type: 'Topic',
      #                        array: true,
      #                        desc: 'Topics followed by the current user.'},
      #        if: ->(_instance, options) { options[:current_user] } { |_td| options[:current_user].followed_topics.sort_by { |t| t.newest_opinion_upload_time }.reverse }
      expose :topic_modules,
             using: V1::Entities::TopicModule,
             documentation: {type: 'V1::Entities::TopicModule',
                             desc: 'List of the associated topic modules.'} do |dashboard, options|
        dashboard.modules(options[:country_code], options[:latitude], options[:longitude])
      end
    end
  end
end
