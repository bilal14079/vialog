# frozen_string_literal: true
module V1
  module Entities
    class Hashtag < Grape::Entity
      expose :id,
             documentation: { type: 'Integer',
                              desc: 'ID of the hashtag.' }
      expose :name,
             documentation: { type: 'String',
                              desc: 'Name of the hashtag.' }
    end
  end
end
