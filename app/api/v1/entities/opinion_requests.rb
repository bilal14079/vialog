# frozen_string_literal: true
module V1
  module Entities
    class OpinionRequests < Grape::Entity
      expose :topic,
             using: V1::Entities::Topic,
             documentation: { type: 'V1::Entities::Topic',
                              desc: 'The topic of the ask' }
      expose :requests_data,
             using: V1::Entities::OpinionRequestUser,
             documentation: { type: 'V1::Entities::OpinionRequestUser',
                              array: true,
                              desc: 'The list of users requesting in this topic' }
    end
  end
end