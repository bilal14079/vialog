# frozen_string_literal: true
module V1
  module Entities
    class Category < Grape::Entity
      expose :id,
             documentation: { type: 'Integer',
                              desc: 'ID of the category.' }
      expose :name,
             documentation: { type: 'String',
                              desc: 'Name of the category.' }
    end
  end
end
