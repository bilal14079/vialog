# frozen_string_literal: true
module V1
  module Entities
    class Clap < Grape::Entity
      expose :start,
             documentation: { type: 'Integer',
                              desc: 'The start of the clap' }
      expose :duration,
             documentation: { type: 'Integer',
                              desc: 'The duration of the clap' }
      expose :end,
             documentation: { type: 'Integer',
                              desc: 'The end of the clap' }
    end
  end
end