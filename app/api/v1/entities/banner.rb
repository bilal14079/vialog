# frozen_string_literal: true
module V1
  module Entities
    class Banner < Grape::Entity
      expose :title,
             documentation: { type: 'String',
                              desc: 'Title of the banner.' }
      expose :topic_dashboard_id,
             documentation: { type: 'Integer',
                              desc: 'ID of the associated topic dashboard.' }
      expose :thumbnail_url,
             documentation: { type: 'String',
                              desc: 'Thumbnail url of the banner.' }
    end
  end
end
