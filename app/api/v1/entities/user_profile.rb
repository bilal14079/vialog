# frozen_string_literal: true
module V1
  module Entities
    class UserProfile < User
      expose :follower_users_count,
             documentation: { type: 'Integer',
                              desc: 'Count of the follower users of the user.' }
      expose :followed_users_count,
             documentation: { type: 'Integer',
                              desc: 'Count of the followed users by the user.' }
      expose :followed_topics_count,
             documentation: { type: 'Integer',
                              desc: 'Count of the followed topics by the user.' } do |user|
        user.followed_topics.contains_video.count
      end
      expose :opinions_count,
             documentation: { type: 'Integer',
                              desc: 'Count of the opinions of the user.' }
      expose :followed_topics,
             using: V1::Entities::Topic,
             documentation: { type: 'V1::Entities::Topic',
                              array: true,
                              desc: 'Followed topics by the user.' } do |user|
        user.followed_topics
            .recently_followed_first
            .contains_video
      end
      expose :opinioned_topics,
             using: V1::Entities::TopicProfiled,
             documentation: { type: 'V1::Entities::TopicProfiled',
                              array: true,
                              desc: 'Opinioned topics by the user.' }
      expose :topics_needing_opinion_ordered_by_request_count,
             using: V1::Entities::TopicWantedForOpinion,
             documentation: { type: 'V1::Entities::TopicWantedForOpinion',
                              array: true,
                              desc: 'Topics in which the user\'s opinion was requested.' }
    end
  end
end
