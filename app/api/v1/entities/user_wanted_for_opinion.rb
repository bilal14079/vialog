# frozen_string_literal: true
module V1
  module Entities
    class UserWantedForOpinion < User
      expose :opinion_requests_count,
             documentation: { type: 'Integer',
                              desc: 'Number of opinion requests the user received in the current topic.' },
             if: ->(_instance, options) { options[:current_topic] }
      expose :received_opinion_request_from_current_user,
             documentation: { type: 'Boolean',
                              desc: 'Did the current user requested the users\'s opinion in the current topic?' },
             if: ->(_instance, options) { options[:current_topic] && options[:current_user] } do |user|
        user.received_opinion_request_from_user?(options[:current_user], options[:current_topic])
      end
    end
  end
end
