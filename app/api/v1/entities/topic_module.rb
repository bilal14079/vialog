# frozen_string_literal: true
module V1
  module Entities
    class TopicModule < Grape::Entity
      expose :position,
             documentation: { type: 'Integer',
                              desc: 'Position of the topic dashboard (starts at 0).' }
      expose :title,
             documentation: { type: 'String',
                              desc: 'Title of the topic module.' }
      expose :type,
             documentation: { type: 'String',
                              desc: 'Type of the topic module.' }
      expose :display_type,
             documentation: { type: 'String',
                              desc: 'Display type of the topic module.' }

      expose :title_bg_color,
             documentation: { type: 'String',
                              desc: 'The color which should be used as the background color for the title' }

      expose :title_color,
             documentation: { type: 'String',
                              desc: 'The color which should be used as the title color' }

      expose :category_topics_by_country,
             if: -> (_, _) { object.category_type? },
             as: :topics,
             using: V1::Entities::Topic,
             documentation: { type: 'V1::Entities::Topic',
                              desc: 'Topics in the topic module.' } do |_, options|
        object.category_topics_newest_first.select do |topic|
          topic.allowed_in_country(options[:country_code])
        end
      end

      expose :hashtag_topics_by_country,
             if: -> (_, _) { object.hashtag_type? },
             as: :topics,
             using: V1::Entities::Topic,
             documentation: { type: 'V1::Entities::Topic',
                              desc: 'Topics in the topic module.' } do |_, options|
        object.hashtag_topics_newest_first.select do |topic|
          topic.allowed_in_country(options[:country_code])
        end
      end

      expose :recent_topics_by_country,
             if: -> (_, _) { object.recents_type? },
             as: :topics,
             using: V1::Entities::Topic,
             documentation: { type: 'V1::Entities::Topic',
                              desc: 'Topics in the topic module.' } do |_, options|
        object.recent_topics_newest_first.select do |topic|
          topic.allowed_in_country(options[:country_code])
        end
      end

      expose :banner,
             if: -> (_, _) { object.banner_type? },
             using: V1::Entities::Banner,
             documentation: { type: 'V1::Entities::Banner',
                              desc: 'Banner associated with the topic module.' }
    end
  end
end
