# frozen_string_literal: true
module V1
  module Entities
    class TopicWantedForOpinion < Topic
      expose :opinion_requests_count,
             as: :profiled_user_opinion_requests_count,
             documentation: { type: 'Integer',
                              desc: 'Number of opinion requests the profiled user has in the topic' }
      expose :current_user_requested_opinion_from_profiled_user,
             documentation: { type: 'Boolean',
                              desc: 'Did the current user request the profiled users\'s opinion in the topic?' },
             if: ->(_instance, options) { options[:profiled_user] && options[:current_user] } do |topic|
        options[:profiled_user].received_opinion_request_from_user?(options[:current_user], topic)
      end
    end
  end
end
