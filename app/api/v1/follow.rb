# frozen_string_literal: true
module V1
  class Follow < Grape::API
    group :follow do
      group :user do
        route_param :id, type: Integer do
          # POST /v1/follow/user/:id
          desc 'Follow a user',
               headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
          post do
            authenticate_user!
            user = User.find(params[:id])
            current_user.follow_user(user)

            { success: true }
          end
        end
      end

      group :topic do
        route_param :id, type: Integer do
          # POST /v1/follow/topic/:id
          desc 'Follow a topic',
               headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
          post do
            authenticate_user!
            topic = Topic.find(params[:id])
            current_user.follow_topic(topic)

            { success: true }
          end
        end
      end
    end

    group :unfollow do
      group :user do
        route_param :id, type: Integer do
          # POST /v1/unfollow/user/:id
          desc 'Unfollow a user',
               headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
          post do
            authenticate_user!
            user = User.find(params[:id])
            current_user.unfollow(user)

            { success: true }
          end
        end
      end

      group :topic do
        route_param :id, type: Integer do
          # POST /v1/unfollow/topic/:id
          desc 'Unfollow a topic',
               headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required)
          post do
            authenticate_user!
            topic = Topic.find(params[:id])
            current_user.unfollow(topic)

            { success: true }
          end
        end
      end
    end

    group :me do
      # GET /v1/me/followed_topics
      desc 'Returns the topics the current user follows',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
           entity: V1::Entities::Topic
      params do
        optional :limit, type: Integer, default: 100, desc: 'Max number of results to return'
      end
      get :followed_topics do
        authenticate_user!
        topics = current_user.followed_topics
                     .recently_followed_first
                     .contains_video
                     .limit(params[:limit])

        present topics, with: V1::Entities::Topic,
                current_user: current_user,
                root: :topics
      end

      # GET /v1/me/followed_users
      desc 'Returns the users the current user follows',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
           entity: V1::Entities::User
      params do
        optional :limit, type: Integer, default: 100, desc: 'Max number of results to return'
      end
      get :followed_users do
        authenticate_user!
        users = current_user.followed_users
                    .recently_followed_first
                    .limit(params[:limit])

        present users, with: V1::Entities::User,
                root: :users
      end

      # GET /v1/me/followers
      desc 'Returns the followers of the current user',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(:required),
           entity: V1::Entities::User
      params do
        optional :limit, type: Integer, default: 100, desc: 'Max number of results to return'
      end
      get :followers do
        authenticate_user!
        users = current_user.follower_users
                    .recent_followers_first
                    .limit(params[:limit])

        present users, with: V1::Entities::User,
                root: :users
      end
    end

    group :users do
      route_param :id, type: Integer do
        # GET /v1/users/:id/followed_users
        desc 'Returns the users the selected user follows',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(false),
             entity: V1::Entities::User
        params do
          optional :limit, type: Integer, default: 100, desc: 'Max number of results to return'
        end
        get :followed_users do
          user = User.find(params[:id])

          users = user.followed_users
                      .recently_followed_first
                      .limit(params[:limit])

          present users, with: V1::Entities::User,
                  root: :users,
                  current_user: current_user
        end
      end

      route_param :id, type: Integer do
        # GET /v1/users/:id/followers
        desc 'Returns the followers of the selected user',
             headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(false),
             entity: V1::Entities::User
        params do
          optional :limit, type: Integer, default: 100, desc: 'Max number of results to return'
        end
        get :followers do
          user = User.find(params[:id])

          users = user.follower_users
                      .recent_followers_first
                      .limit(params[:limit])

          present users, with: V1::Entities::User,
                  root: :users,
                  current_user: current_user
        end
      end
    end
  end
end
