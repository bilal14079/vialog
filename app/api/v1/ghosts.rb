# frozen_string_literal: true
require 'twitter/ghost_register'

module V1
  class Ghosts < Grape::API
    group :ghosts do
      # PUT /v1/ghosts/add
      desc 'Add a new ghost by Twitter handle',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(false),
           entity: V1::Entities::User
      params do
        requires :handle, type: String, desc: 'The Twitter handle of the user'
      end
      put :add do
        user = User.find_by_twitter(params[:handle])

        unless user.present?
          user = Twitter::GhostRegister.new(handle: params[:handle]).register
          if user.present?
            GhostMetadatum.create(creator_user: current_user, ghost_user: user)
            ActionItem.create(actor: current_user, target: user, action_type: ActionItem::ACTION_GHOST_ADD)
            current_user.follow_user(user)
          end
        end

        present user, with: V1::Entities::User,
                root: :user,
                current_user: current_user
      end
    end
  end
end