# frozen_string_literal
require 'mailchimp_subscriber'

module V1
  class UpdateEmail < Grape::API
    group :me do

      # POST /me/email
      desc 'Update the email address of the current user',
           headers: V1::Helpers::DocumentationHelper.user_auth_headers_info(false),
           entity: V1::Entities::User
      params do
        requires :email, type: String, desc: 'The email address of the user'
        optional :session_token, type: String, desc: 'The session token of the current user'
        optional :device_uid, type: String, desc: 'The uuid of the device'
      end
      post :email do
        user = if params[:session_token] && params[:device_uid]
                 token = SessionToken.find_by(token: params[:session_token], device_uid: params[:device_uid])
                 error! 'User not found', 403 unless token
                 token.try(:user)
               else
                 authenticate_user!
                 current_user
               end

        user.update!(email: params[:email])

        MailchimpSubscriber.new.subscribe list_id: ENV['MAILCHIMP_LIST_ID'],
                                          email: params[:email],
                                          first_name: user.name.split[0],
                                          last_name: user.name.split.size > 1 ? user.name.split[1] : ''

        present user, with: V1::Entities::User,
                root: :users
      end

    end
  end
end