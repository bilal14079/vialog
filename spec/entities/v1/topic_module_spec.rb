# frozen_string_literal: true
require 'rails_helper'

RSpec.describe V1::Entities::TopicModule, type: :entity do
  subject(:entity) { described_class }

  describe 'exposures' do
    it { is_expected.to represent(:position) }
    it { is_expected.to represent(:title) }
    it { is_expected.to represent(:type) }
    it { is_expected.to represent(:display_type) }

    it 'exposes category_topics as topics for category_type topics modules' do
      is_expected.to represent(:category_topics).when(category_type?: true)
        .as(:topics)
        .using(V1::Entities::Topic)
    end

    it 'exposes hashtag_topics as topics for hashtag_type topics modules' do
      is_expected.to represent(:hashtag_topics).when(hashtag_type?: true)
        .as(:topics)
        .using(V1::Entities::Topic)
    end

    it 'exposes banner for banner_type topics modules' do
      is_expected.to represent(:banner).when(banner_type?: true)
        .using(V1::Entities::Banner)
    end
  end
end
