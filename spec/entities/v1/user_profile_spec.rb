# frozen_string_literal: true
require 'rails_helper'

RSpec.describe V1::Entities::UserProfile, type: :entity do
  subject(:entity) { described_class }

  describe 'exposures' do
    it { is_expected.to represent(:id) }
    it { is_expected.to represent(:name) }
    it { is_expected.to represent(:profile_picture_url) }
    it { is_expected.to represent(:twitter_handle) }
    it { is_expected.to represent(:follower_users_count) }
    it { is_expected.to represent(:followed_users_count) }
    it { is_expected.to represent(:followed_topics_count) }
    it { is_expected.to represent(:opinions_count) }
    it { is_expected.to represent(:followed_topics).using(V1::Entities::Topic) }
    it { is_expected.to represent(:opinioned_topics).using(V1::Entities::TopicProfiled) }
    it do
      is_expected.to represent(:topics_needing_opinion_ordered_by_request_count)
        .using(V1::Entities::TopicWantedForOpinion)
    end
    # TODO: followed_by_current_user
  end
end
