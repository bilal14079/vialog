# frozen_string_literal: true
require 'rails_helper'

RSpec.describe V1::Entities::TopicDashboard, type: :entity do
  subject(:entity) { described_class }

  describe 'exposures' do
    it { is_expected.to represent(:id) }
    it { is_expected.to represent(:title) }
    it { is_expected.to represent(:main) }
    it do
      is_expected.to represent(:topic_modules).using(V1::Entities::TopicModule, category_type?: true,
                                                                                hashtag_type?: false,
                                                                                banner_type?: false)
    end
    # TODO: topics_followed_by_current_user
  end
end
