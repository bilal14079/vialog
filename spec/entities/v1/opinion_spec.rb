# frozen_string_literal: true
require 'rails_helper'

RSpec.describe V1::Entities::Opinion, type: :entity do
  subject(:entity) { described_class }

  describe 'exposures' do
    it { is_expected.to represent(:id) }
    it { is_expected.to represent(:updated_at).as(:uploaded_at) }
    it { is_expected.to represent(:topic).using(V1::Entities::Topic) }
    it { is_expected.to represent(:user).using(V1::Entities::User) }
    it { is_expected.to represent(:reactions) }
    it { is_expected.to represent(:video_hls_url).as(:video_url) }
    it { is_expected.to represent(:video_thumbnail_url).as(:thumbnail_url) }
    it { is_expected.to represent(:token) }
    it { is_expected.to represent(:duration) }
  end
end
