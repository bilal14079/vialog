# frozen_string_literal: true
require 'rails_helper'

RSpec.describe V1::Entities::User, type: :entity do
  subject(:entity) { described_class }

  describe 'exposures' do
    it { is_expected.to represent(:id) }
    it { is_expected.to represent(:name) }
    it { is_expected.to represent(:profile_picture_url) }
    it { is_expected.to represent(:twitter_handle) }
    # TODO: followed_by_current_user
  end
end
