# frozen_string_literal: true
require 'rails_helper'

RSpec.describe V1::Entities::SessionToken, type: :entity do
  subject(:entity) { described_class }

  describe 'exposures' do
    it { is_expected.to represent(:token) }
    it { is_expected.to represent(:provider) }
    it { is_expected.to represent(:user_id).as(:id) }
    it { is_expected.to represent(:user_name).as(:name) }
    it { is_expected.to represent(:user_profile_picture_url).as(:profile_picture_url) }
    it { is_expected.to represent(:user_twitter_handle).as(:twitter_handle) }
  end
end
