# frozen_string_literal: true
require 'rails_helper'

RSpec.describe V1::Entities::Category, type: :entity do
  subject(:entity) { described_class }

  describe 'exposures' do
    it { is_expected.to represent(:id) }
    it { is_expected.to represent(:name) }
  end
end
