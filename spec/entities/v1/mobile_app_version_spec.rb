# frozen_string_literal: true
require 'rails_helper'

RSpec.describe V1::Entities::MobileAppVersion, type: :entity do
  subject(:entity) { described_class }

  describe 'exposures' do
    it { is_expected.to represent(:platform) }
    it { is_expected.to represent(:update_severity).as(:update_type) }
    it { is_expected.to represent(:version_identifier).as(:last_version) }
  end
end
