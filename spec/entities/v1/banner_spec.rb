# frozen_string_literal: true
require 'rails_helper'

RSpec.describe V1::Entities::Banner, type: :entity do
  subject(:entity) { described_class }

  describe 'exposures' do
    it { is_expected.to represent(:title) }
    it { is_expected.to represent(:topic_dashboard_id) }
  end
end
