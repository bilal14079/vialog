# frozen_string_literal: true
require 'rails_helper'

RSpec.describe V1::Entities::Topic, type: :entity do
  subject(:entity) { described_class }

  describe 'exposures' do
    it { is_expected.to represent(:id) }
    it { is_expected.to represent(:title) }
    it { is_expected.to represent(:category).using(V1::Entities::Category) }
    it { is_expected.to represent(:hashtags).using(V1::Entities::Hashtag) }
    it { is_expected.to represent(:opinions_count) }
    it { is_expected.to represent(:thumbnail_url) }
    # TODO: is_current_user
    # TODO: followed_by_current_user
  end
end
