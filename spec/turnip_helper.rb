# frozen_string_literal: true
require_relative 'rails_helper'
Dir[Rails.root.join('spec/features/step_definitons/**/*_steps.rb')].each { |f| load f }

def json_request(method, path, params: {}, headers: {})
  header 'Accept', 'application/json'
  headers.each { |k, v| header k, v }

  send method, path, params
end

def last_response_json
  JSON.parse last_response.body
end

def json_values_at(json_path)
  JsonPath.new(json_path).on(last_response_json).to_a
end

def pretty_response
  JSON.pretty_generate last_response_json
end

def current_session_token
  SessionToken.find_by(provider: SessionToken::PROVIDER_FACEBOOK) || create(:session_token,
                                                                            provider: SessionToken::PROVIDER_FACEBOOK,
                                                                            challenge: 'test_fb_token')
end

def twitter_session_token
  SessionToken.find_by(provider: SessionToken::PROVIDER_TWITTER) || create(:session_token,
                                                                           provider: SessionToken::PROVIDER_TWITTER)
end

def current_user
  current_session_token.user
end

def twitter_user_auth_headers
  { V1::Helpers::Authentication::SESSION_TOKEN_HEADER_KEY => twitter_session_token.token,
    V1::Helpers::Authentication::DEVICE_UID_HEADER_KEY => twitter_session_token.device_uid }
end

def correct_user_auth_headers
  { V1::Helpers::Authentication::SESSION_TOKEN_HEADER_KEY => current_session_token.token,
    V1::Helpers::Authentication::DEVICE_UID_HEADER_KEY => current_session_token.device_uid }
end

def user_auth_headers_with_invalid_session_token
  invalid_session_token = "not_#{current_session_token.token}"
  correct_user_auth_headers.merge V1::Helpers::Authentication::SESSION_TOKEN_HEADER_KEY => invalid_session_token
end

def valid_fb_access_token
  'EAAJZAC8pTaMoBAGAsk2e5RmfBWiQKvhVZB9paP5hM1SLnpojBC3RAiu4Sln5BssZBrV' \
  'xa8y71XrydpNDSXvP6nu1Rm2nrZBk7bbu3V8oIhwBYMKxpG5JmPOZBL5Xq2k2x57rOkO' \
  'M6m6F71YDEZATFEmdXOXH57riRkYO5IyZB4wuMUjwIFKaAON'
end

def valid_twitter_access_token
  '707475986-RtM8gPzl0GC1Fo0nIrjOA2MIVhsCkxDvNNxFlTLN'
end

def valid_twitter_access_token_secret
  'Qv7I8QqdI7WqUxoXAgLQGUTZAsE3kMh9d91BzyP39IQFp'
end
