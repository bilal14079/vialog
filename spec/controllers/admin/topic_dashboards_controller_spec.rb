# frozen_string_literal: true
require 'rails_helper'
require_relative '../../support/shared_examples_for_activeadmin'

RSpec.describe Admin::TopicDashboardsController, type: :controller do
  it_behaves_like 'an activeadmin resource' do
    let(:hashtags) { create_list(:hashtag, 2) }

    let(:factory_name) { :topic_dashboard_with_modules }
    let(:resource_name) { :topic_dashboard }
    let(:collection_name) { :topic_dashboards }

    # TODO: do this right with all the attributes...
    let(:create_params) { { main: true } }
    let(:update_params) { { main: false } }
  end
end
