# frozen_string_literal: true
require 'rails_helper'
require_relative '../../support/shared_examples_for_activeadmin'

RSpec.describe Admin::MobileAppVersionsController, type: :controller do
  it_behaves_like 'an activeadmin resource' do
    let(:resource_name) { :mobile_app_version }
    let(:collection_name) { :mobile_app_versions }

    let(:platform1) { MobileAppVersion::PLATFORMS.first }
    let(:platform2) { MobileAppVersion::PLATFORMS.last }

    let(:create_params) do
      { platform: platform1,
        update_severity: MobileAppVersion::SOFT_SEVERITY,
        version_identifier: '0.0.1' }
    end

    let(:update_params) do
      { platform: platform2,
        update_severity: MobileAppVersion::HARD_SEVERITY,
        version_identifier: '0.0.2' }
    end
  end
end
