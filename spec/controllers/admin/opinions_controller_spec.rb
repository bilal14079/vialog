# frozen_string_literal: true
require 'rails_helper'
require_relative '../../support/shared_examples_for_activeadmin'

RSpec.describe Admin::OpinionsController, type: :controller do
  it_behaves_like 'an activeadmin resource' do
    let(:topic1) { create(:topic) }
    let(:topic2) { create(:topic) }
    let(:user1) { create(:user) }
    let(:user2) { create(:user) }
    let(:video) { fixture_file_upload('/files/test.mov', 'video/quicktime') }

    let(:resource_name) { :opinion }
    let(:collection_name) { :opinions }

    let(:create_params) { { user_id: user1.id, topic_id: topic1.id, video: video } }
    let(:update_params) { { user_id: user2.id, topic_id: topic2.id } }
  end
end
