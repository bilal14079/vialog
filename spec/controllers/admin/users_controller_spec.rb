# frozen_string_literal: true
require 'rails_helper'
require_relative '../../support/shared_examples_for_activeadmin'

RSpec.describe Admin::UsersController, type: :controller do
  it_behaves_like 'an activeadmin resource' do
    let(:resource_name) { :user }
    let(:collection_name) { :users }

    let(:create_params) do
      { name: 'some user',
        profile_picture_url: 'http://example.com/example.png' }
    end

    let(:update_params) do
      { name: 'some other user',
        profile_picture_url: 'http://example.com/other_example.png' }
    end
  end
end
