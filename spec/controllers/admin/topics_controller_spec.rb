# frozen_string_literal: true
require 'rails_helper'
require_relative '../../support/shared_examples_for_activeadmin'

RSpec.describe Admin::TopicsController, type: :controller do
  it_behaves_like 'an activeadmin resource' do
    let(:category1) { create(:category) }
    let(:category2) { create(:category) }

    let(:resource_name) { :topic }
    let(:collection_name) { :topics }

    let(:create_params) { { title: 'some topic', category_id: category1.id } }
    let(:update_params) { { title: 'some other topic', category_id: category2.id } }
  end
end
