# frozen_string_literal: true
require 'rails_helper'

RSpec.describe VideoPlayerController, type: :controller do
  let(:video_token) { 'test' }
  let!(:opinion) { create(:opinion, token: video_token) }

  describe 'GET show' do
    it 'renders the show template' do
      process :show, method: :get, params: { video: video_token }
      expect(response).to render_template('show')
    end

    it 'returns a 200 status code' do
      process :show, method: :get, params: { video: video_token }
      expect(response).to have_http_status(200)
    end
  end
end
