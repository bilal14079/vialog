# frozen_string_literal: true
# == Schema Information
#
# Table name: opinion_reactions
#
#  id              :integer          not null, primary key
#  opinion_id      :integer          not null, indexed, indexed => [user_id, reaction_type, reaction_second]
#  user_id         :integer          not null, indexed, indexed => [opinion_id, reaction_type, reaction_second]
#  reaction_type   :string           not null, indexed => [opinion_id, user_id, reaction_second]
#  reaction_second :integer          not null, indexed => [opinion_id, user_id, reaction_type]
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'rails_helper'

RSpec.describe OpinionReaction, type: :model do
  let(:subject) { build(:love_opinion_reaction) }

  describe 'connections' do
    it { is_expected.to belong_to(:opinion) }
    it { is_expected.to belong_to(:user) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:opinion).with_message('must exist') }
    it { is_expected.to validate_presence_of(:user).with_message('must exist') }

    it { is_expected.to validate_inclusion_of(:reaction_type).in_array(OpinionReaction::REACTION_TYPES) }
    it { is_expected.to validate_uniqueness_of(:reaction_type).scoped_to([:user_id, :opinion_id, :reaction_second]) }
    it do
      is_expected.to validate_numericality_of(:reaction_second)
        .only_integer
        .is_greater_than_or_equal_to(0)
        .is_less_than(60)
    end
  end

  describe '#notify_about_reaction_received' do
    let!(:subject) { create(:love_opinion_reaction) }

    it 'has a request received notification' do
      expect(subject.reaction_received_notification).to be_present
    end
  end
end
