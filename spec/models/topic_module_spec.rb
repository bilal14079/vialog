# frozen_string_literal: true
# == Schema Information
#
# Table name: topic_modules
#
#  id                 :integer          not null, primary key
#  type               :string           not null
#  topic_dashboard_id :integer          not null, indexed
#  position           :integer          not null
#  title              :string           not null
#  display_type       :string           not null
#  category_id        :integer          indexed
#  banner_id          :integer          indexed
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  title_bg_color     :string
#  title_color        :string
#  recents            :boolean
#  topic_limit        :integer          default(5)
#

require 'rails_helper'

RSpec.describe TopicModule, type: :model do
  let(:subject) { build(:category_topic_module) }

  describe 'associatons' do
    it { is_expected.to belong_to(:topic_dashboard) }
    it { is_expected.to belong_to(:category) }
    it { is_expected.to have_many(:category_topics).through(:category) }
    it { is_expected.to belong_to(:banner) }
    it { is_expected.to have_one(:banner_dashboard).through(:banner) }
    it { is_expected.to have_many(:taggings).dependent(:destroy) }
    it { is_expected.to have_many(:hashtags).through(:taggings) }
    it { is_expected.to have_many(:hashtag_topics).through(:hashtags) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:topic_dashboard).with_message('must exist') }
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_inclusion_of(:display_type).in_array(TopicModule::DISPLAY_TYPES) }
  end

  describe 'banner display type' do
    let(:subject) { create(:banner_topic_module) }

    it { expect(subject.display_type).to eq TopicModule::BANNER_TYPE }
  end

  describe 'type is set automatically' do
    context 'category topic module' do
      let(:subject) { create(:category_topic_module) }

      it { expect(subject.type).to eq TopicModule::CATEGORY_TYPE }
    end

    context 'hashtags topic module' do
      let(:subject) { create(:hashtag_topic_module) }

      it { expect(subject.type).to eq TopicModule::HASHTAG_TYPE }
    end

    context 'banner topic module' do
      let(:subject) { create(:banner_topic_module) }

      it { expect(subject.type).to eq TopicModule::BANNER_TYPE }
    end
  end
end
