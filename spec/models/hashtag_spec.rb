# frozen_string_literal: true
# == Schema Information
#
# Table name: hashtags
#
#  id         :integer          not null, primary key
#  name       :string           not null, indexed
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Hashtag, type: :model do
  let(:subject) { build(:hashtag) }

  describe 'connections' do
    it { is_expected.to have_many(:taggings).dependent(:destroy) }
    it { is_expected.to have_many(:topics).through(:taggings).source(:taggable) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name) }
  end
end
