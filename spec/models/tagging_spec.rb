# frozen_string_literal: true
# == Schema Information
#
# Table name: taggings
#
#  id            :integer          not null, primary key
#  hashtag_id    :integer          not null, indexed => [taggable_id, taggable_type]
#  taggable_id   :integer          not null, indexed => [hashtag_id, taggable_type]
#  taggable_type :string           not null, indexed => [hashtag_id, taggable_id]
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Tagging, type: :model do
  let(:subject) { build(:category_tagging) }

  describe 'connections' do
    it { is_expected.to belong_to(:hashtag) }
    it { is_expected.to belong_to(:taggable) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:hashtag).with_message('must exist') }
    it { is_expected.to validate_presence_of(:taggable).with_message('must exist') }
    it { is_expected.to validate_uniqueness_of(:hashtag_id).scoped_to([:taggable_id, :taggable_type]) }
  end
end
