# frozen_string_literal: true
# == Schema Information
#
# Table name: opinion_requests
#
#  id                :integer          not null, primary key
#  requester_user_id :integer          not null, indexed, indexed => [requested_user_id, topic_id]
#  requested_user_id :integer          not null, indexed, indexed => [requester_user_id, topic_id]
#  topic_id          :integer          not null, indexed, indexed => [requester_user_id, requested_user_id]
#  opinion_id        :integer          indexed
#  fullfilled        :boolean          default(FALSE)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

require 'rails_helper'

RSpec.describe OpinionRequest, type: :model do
  let(:subject) { build(:opinion_request) }

  describe 'connections' do
    it { is_expected.to belong_to(:requested_user) }
    it { is_expected.to belong_to(:requester_user) }
    it { is_expected.to belong_to(:topic) }
    it { is_expected.to belong_to(:opinion) }
    it { is_expected.to have_one(:request_received_notification).dependent(:destroy) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:requested_user).with_message('must exist') }
    it { is_expected.to validate_presence_of(:requester_user).with_message('must exist') }
    it { is_expected.to validate_presence_of(:topic).with_message('must exist') }
    it { is_expected.to validate_uniqueness_of(:topic_id).scoped_to([:requester_user_id, :requested_user_id]) }
  end

  describe '#notify_about_request_received' do
    let!(:subject) { create(:opinion_request) }

    it 'has a request received notification' do
      expect(subject.request_received_notification).to be_present
    end
  end
end
