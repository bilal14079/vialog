# frozen_string_literal: true
# == Schema Information
#
# Table name: categories
#
#  id           :integer          not null, primary key
#  name         :string           not null, indexed
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  default_mark :boolean
#

require 'rails_helper'

RSpec.describe Category, type: :model do
  let(:subject) { build(:category) }

  describe 'assosiations' do
    it { is_expected.to have_many(:topics).dependent(:destroy) }
    it { is_expected.to have_many(:topic_modules).dependent(:destroy) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name) }
  end
end
