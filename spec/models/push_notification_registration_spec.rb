# frozen_string_literal: true
# == Schema Information
#
# Table name: push_notification_registrations
#
#  id                :integer          not null, primary key
#  user_id           :integer          not null, indexed
#  notification_type :string           not null, indexed
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

require 'rails_helper'

RSpec.describe PushNotificationRegistration, type: :model do
  let(:subject) { build(:push_notification_registration) }

  describe 'connections' do
    it { is_expected.to belong_to(:user) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:user).with_message('must exist') }
    it { is_expected.to validate_presence_of(:push_notification_token) }
    it { is_expected.to validate_uniqueness_of(:push_notification_token).scoped_to([:notification_type, :device_uid]) }
    it { is_expected.to validate_presence_of(:device_uid) }
    it { is_expected.to validate_inclusion_of(:notification_type).in_array(Notification::NOTIFICATION_TYPES) }
  end
end
