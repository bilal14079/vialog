# frozen_string_literal: true
# == Schema Information
#
# Table name: notifications
#
#  id                :integer          not null, primary key
#  type              :string           not null
#  notification_type :string           not null
#  user_id           :integer          not null, indexed
#  subject_id        :integer          not null
#  subject_type      :string           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  read              :boolean          default(FALSE), not null
#

require 'rails_helper'

RSpec.describe Notification, type: :model do
  let(:subject) { build(:notification) }

  describe 'connections' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:subject) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:user).with_message('must exist') }
    it { is_expected.to validate_presence_of(:subject).with_message('must exist') }
    it { is_expected.to validate_inclusion_of(:notification_type).in_array(Notification::NOTIFICATION_TYPES) }
  end

  describe '#push_notification_tokens' do
    let(:device_uid) { 'device_uid' }

    let!(:subject) { create(:notification) }
    let!(:session_token) { create(:session_token, device_uid: device_uid, user: subject.user) }
    let!(:registration_for_notification) do
      create(:push_notification_registration, user: subject.user,
                                              notification_type: subject.notification_type,
                                              device_uid: device_uid)
    end

    it 'returns the correct push notification tokens' do
      expect(subject.push_notification_tokens).to include registration_for_notification.push_notification_token
    end
  end
end
