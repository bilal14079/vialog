# frozen_string_literal: true
# == Schema Information
#
# Table name: users
#
#  id                    :integer          not null, primary key
#  name                  :string           not null, indexed
#  profile_picture_url   :string           not null
#  facebook_uid          :string           indexed
#  twitter_uid           :string           indexed
#  twitter_handle        :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  opinions_count        :integer          default(0), not null
#  follower_users_count  :integer          default(0), not null
#  followed_users_count  :integer          default(0), not null
#  followed_topics_count :integer          default(0), not null
#  email                 :string
#  last_lat              :decimal(, )
#  last_lng              :decimal(, )
#  last_country          :string
#  guest                 :boolean          default(FALSE)
#  ghost                 :boolean          default(FALSE)
#  bio                   :text
#  profile_video_id      :integer          indexed
#  handle                :string
#  intercom_status       :string           default("unregistered")
#  seen_time             :integer
#  hosted_issues_count   :integer
#  open_asks_count       :integer
#  responded_asks_count  :integer
#  last_login            :datetime
#

require 'rails_helper'

RSpec.describe User, type: :model do
  let(:subject) { build(:user) }

  describe 'connections' do
    it { is_expected.to have_many(:session_tokens).dependent(:destroy) }
    it { is_expected.to have_many(:opinions).dependent(:destroy) }
    it { is_expected.to have_many(:followed_followings).dependent(:destroy) }
    it { is_expected.to have_many(:followed_by_followings).dependent(:destroy) }
    it { is_expected.to have_many(:followed_topics).through(:followed_followings).source(:followed) }
    it { is_expected.to have_many(:followed_users).through(:followed_followings).source(:followed) }
    it { is_expected.to have_many(:opinions).dependent(:destroy) }
    it { is_expected.to have_many(:opinioned_topics).through(:opinions).source(:topic) }
    it { is_expected.to have_many(:push_notification_registrations).dependent(:destroy) }
    it { is_expected.to have_many(:requested_opinion_requests).dependent(:destroy) }
    it { is_expected.to have_many(:received_opinion_requests).dependent(:destroy) }
    it { is_expected.to have_many(:topics_needing_opinion).through(:received_opinion_requests).source(:topic) }
    it { is_expected.to have_many(:notifications).dependent(:destroy) }
    it { is_expected.to have_many(:facebook_friend_joined_notifications).dependent(:destroy) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:profile_picture_url) }
    it { is_expected.to validate_uniqueness_of(:facebook_uid) }
    it { is_expected.to validate_uniqueness_of(:twitter_uid) }

    context 'multiple records with no facebook_uid' do
      before { create :user, facebook_uid: nil }
      let(:subject) { build :user, facebook_uid: nil }

      it { is_expected.to be_valid }
    end

    context 'multiple records with no twitter_uid' do
      before { create :user, twitter_uid: nil }
      let(:subject) { build :user, twitter_uid: nil }

      it { is_expected.to be_valid }
    end
  end

  describe 'requested_opinion_requests' do
    let(:subject) { create(:user_with_requested_opinion_requests) }
    let!(:fullfilled_opinion_request) { create(:fullfilled_opinion_request, requester_user: subject) }

    it 'only containt unfullfilled opinion requests' do
      expect(subject.requested_opinion_requests).not_to include(fullfilled_opinion_request)
    end
  end

  describe 'received_opinion_requests' do
    let(:subject) { create(:user_with_received_opinion_requests) }
    let!(:fullfilled_opinion_request) { create(:fullfilled_opinion_request, requested_user: subject) }

    it 'only containt unfullfilled opinion requests' do
      expect(subject.received_opinion_requests).not_to include(fullfilled_opinion_request)
    end
  end

  describe 'topics_needing_opinion' do
    let(:subject) { create(:user) }
    let(:topic) { create(:topic) }
    let!(:opinion_request_1) { create(:opinion_request, requested_user: subject, topic: topic) }
    let!(:opinion_request_2) { create(:opinion_request, requested_user: subject, topic: topic) }

    it 'is does not contain the same topic twice' do
      expect(subject.topics_needing_opinion.count).to eq(1)
    end
  end

  describe 'opinions' do
    let(:subject) { create(:user_with_opinions) }
    let!(:unprocessed_opinion) { create(:unprocessed_opinion, user: subject) }

    it 'only containt processed opinions' do
      expect(subject.opinions).not_to include(unprocessed_opinion)
    end
  end

  describe 'counter cache columns' do
    let(:subject) { create(:user_with_followed_topics_and_users) }

    let(:topic) { create(:topic) }
    let(:user) { create(:user) }
    let(:follower_user) { create(:user_following, followed: subject).follower }

    let(:follow_topic) { create(:topic_following, follower: subject) }
    let(:first_followed_topic_following) { subject.followed_followings.find_by(followed_type: 'Topic') }
    let(:unfollow_topic) { first_followed_topic_following.destroy }

    let(:follow_user) { create(:user_following, follower: subject) }
    let(:first_followed_user_following) { subject.followed_followings.find_by(followed_type: 'User') }
    let(:unfollow_user) { first_followed_user_following.destroy }

    let(:user_being_followed) { follower_user }
    let(:user_being_unfollowed) do
      user_being_followed
      Following.find_by(followed: subject).destroy
    end

    context '#followed_topics_count' do
      it 'is incremented when the user follows a new topic' do
        expect { follow_topic }.to change { subject.reload.followed_topics_count }.by(1)
      end

      it 'is decremented when the user unfollows a topic' do
        expect { unfollow_topic }.to change { subject.reload.followed_topics_count }.by(-1)
      end

      it 'is unchanged when the user follows a new user' do
        expect { follow_user }.not_to change { subject.reload.followed_topics_count }
      end

      it 'is unchanged when the user unfollows a user' do
        expect { unfollow_user }.not_to change { subject.reload.followed_topics_count }
      end

      it 'is unchanged when the user is being followed by another user' do
        expect { user_being_followed }.not_to change { subject.reload.followed_users_count }
      end

      it 'is unchanged when the user is being unfollowed by another user' do
        expect { user_being_unfollowed }.not_to change { subject.reload.followed_users_count }
      end
    end

    context '#followed_users_count' do
      it 'is incremented when the user follows a new user' do
        expect { follow_user }.to change { subject.reload.followed_users_count }.by(1)
      end

      it 'is decremented when the user unfollows a user' do
        expect { unfollow_user }.to change { subject.reload.followed_users_count }.by(-1)
      end

      it 'is unchanged when the user follows a new topic' do
        expect { follow_topic }.not_to change { subject.reload.followed_users_count }
      end

      it 'is unchanged when the user unfollows a topic' do
        expect { unfollow_topic }.not_to change { subject.reload.followed_users_count }
      end

      it 'is unchanged when the user is being followed by another user' do
        expect { user_being_followed }.not_to change { subject.reload.followed_users_count }
      end

      it 'is unchanged when the user is being unfollowed by another user' do
        expect { user_being_unfollowed }.not_to change { subject.reload.followed_users_count }
      end
    end

    context '#follower_users_count' do
      it 'is incremented when the user is being followed by another user' do
        expect { user_being_followed }.to change { subject.reload.follower_users_count }.by(1)
      end

      it 'is decremented when the user is being unfollowed by another user' do
        user_being_followed
        expect(subject.reload.follower_users_count).not_to eq(0)
        expect { user_being_unfollowed }.to change { subject.reload.follower_users_count }.by(-1)
      end

      it 'is unchanged when the user follows a new topic' do
        expect { follow_topic }.not_to change { subject.reload.follower_users_count }
      end

      it 'is unchanged when the user unfollows a topic' do
        expect { unfollow_topic }.not_to change { subject.reload.follower_users_count }
      end

      it 'is unchanged when the user follows a new user' do
        expect { follow_user }.not_to change { subject.reload.follower_users_count }
      end

      it 'is unchanged when the user unfollows a user' do
        expect { unfollow_user }.not_to change { subject.reload.follower_users_count }
      end

      it 'works with follow_user/unfollow' do
        expect do
          user.follow_user(subject)
          user.unfollow(subject)
        end.not_to change { subject.reload.follower_users_count }
      end
    end
  end

  describe '#opinions_count' do
    let(:subject) { create(:user_with_opinions) }

    it 'is incremented when a new opinion is posted to the topic' do
      expect { create(:opinion, user: subject) }.to change { subject.reload.opinions_count }.by(1)
    end

    it 'is decremented when an opinion is removed from the topic' do
      expect { subject.opinions.first.destroy }.to change { subject.reload.opinions_count }.by(-1)
    end
  end

  describe '#request_opinion_of_user' do
    let(:subject) { create(:user) }
    let(:interesting_user) { create(:user) }
    let(:topic) { create(:topic) }

    it 'creates an opinion request ' do
      expect { subject.request_opinion_of_user(interesting_user, topic) }.to change { OpinionRequest.count }.by(1)
    end

    it 'user cannot request opinion from itself' do
      expect { subject.request_opinion_of_user(subject, topic) }.not_to change { OpinionRequest.count }
    end
  end

  describe '#follow_user' do
    let(:subject) { create(:user) }
    let(:interesting_user) { create(:user) }

    it 'adds the interesting_user to followed_users' do
      subject.follow_user(interesting_user)

      expect(subject.followed_users).to include(interesting_user)
    end

    it 'wont add user to his own followed_users' do
      subject.follow_user(subject)

      expect(subject.followed_users).not_to include(subject)
    end
  end

  describe '#unfollow : user' do
    let(:subject) { create(:user) }
    let(:followed_user) { create(:user) }

    before { subject.followed_users << followed_user }

    it 'removes the followed_user from followed_users' do
      expect(subject.followed_users).to include(followed_user)

      subject.unfollow(followed_user)

      expect(subject.reload.followed_users).not_to include(followed_user)
    end
  end

  describe '#follow_topic' do
    let(:subject) { create(:user) }
    let(:interesting_topic) { create(:topic) }

    it 'adds the interesting_topic to followed_users' do
      subject.follow_topic(interesting_topic)

      expect(subject.followed_topics).to include(interesting_topic)
    end
  end

  describe '#unfollow : topic' do
    let(:subject) { create(:user) }
    let(:followed_topic) { create(:topic) }

    before { subject.followed_topics << followed_topic }

    it 'removes the followed_topic from followed_topics ' do
      expect(subject.followed_topics).to include(followed_topic)

      subject.unfollow(followed_topic)

      expect(subject.reload.followed_topics).not_to include(followed_topic)
    end
  end

  describe '#logout_device' do
    let(:subject) { create(:user) }
    let!(:session_token) { create(:session_token, :with_twitter_provider, user: subject) }

    it 'removes all session_tokens with specified device_uid' do
      expect(subject.session_tokens).to include(session_token)

      subject.logout_device(session_token.device_uid)

      expect(subject.session_tokens).not_to include(session_token)
    end
  end

  describe '#report_opinion' do
    let(:subject) { create(:user) }
    let(:opinion) { create(:opinion) }
    let(:reason) { 'reason string' }
    let(:other_reason) { 'other reason string' }

    before { subject.report_opinion(opinion, reason) }

    it 'report an opinion' do
      expect(opinion.offense_reports.last.reason).to eq reason
      expect(opinion.offense_reports.last.user).to eq subject
    end

    it 'report an opinion with other reason' do
      subject.report_opinion(opinion, other_reason)

      expect(opinion.offense_reports.last.reason).not_to eq reason
      expect(opinion.offense_reports.last.reason).to eq other_reason
    end
  end

  describe '#register_for_push_notification' do
    let(:subject) { create(:user) }
    let(:push_notification_token) { 'Token for the push notification' }
    let(:notification_type) { 'Type of the notification' }
    let(:device_uid) { 'Device uid' }

    before { subject.register_for_push_notification(push_notification_token, notification_type, device_uid) }

    it 'creates push notification registration with specified device_uid and notification type' do
      expect(subject.push_notification_registrations.last.push_notification_token).to eq push_notification_token
      expect(subject.push_notification_registrations.last.notification_type).to eq notification_type
      expect(subject.push_notification_registrations.last.device_uid).to eq device_uid
    end
  end

  describe '#deregister_from_push_notification' do
    let(:subject) { create(:user) }
    let!(:push_notification_registration) { create(:push_notification_registration, user: subject) }

    it 'delete the push notification registration with specified device_uid and notification type' do
      expect(subject.push_notification_registrations).to include(push_notification_registration)

      subject.deregister_from_push_notification(push_notification_registration.notification_type,
                                                push_notification_registration.device_uid)

      expect(subject.push_notification_registrations).not_to include(push_notification_registration)
    end
  end
end
