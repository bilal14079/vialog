# frozen_string_literal: true
# == Schema Information
#
# Table name: mobile_app_versions
#
#  id                 :integer          not null, primary key
#  platform           :string           not null, indexed => [version_identifier]
#  update_severity    :string           not null
#  version_identifier :string           not null, indexed => [platform]
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

require 'rails_helper'

RSpec.describe MobileAppVersion, type: :model do
  subject { build(:mobile_app_version) }

  describe 'validations' do
    it { is_expected.to validate_presence_of(:version_identifier) }
    it { is_expected.to validate_uniqueness_of(:version_identifier).case_insensitive.scoped_to(:platform) }
    it { is_expected.to validate_inclusion_of(:platform).in_array(MobileAppVersion::PLATFORMS) }
    it { is_expected.to validate_inclusion_of(:update_severity).in_array(MobileAppVersion::UPDATE_SEVERITIES) }
  end
end
