# frozen_string_literal: true
# == Schema Information
#
# Table name: followings
#
#  id            :integer          not null, primary key
#  follower_id   :integer          not null, indexed => [followed_id, followed_type]
#  followed_id   :integer          not null, indexed => [follower_id, followed_type]
#  followed_type :string           not null, indexed => [follower_id, followed_id]
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Following, type: :model do
  let(:subject) { build(:user_following) }

  describe 'connections' do
    it { is_expected.to belong_to(:follower) }
    it { is_expected.to belong_to(:followed) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:follower).with_message('must exist') }
    it { is_expected.to validate_presence_of(:followed).with_message('must exist') }
    it { is_expected.to validate_uniqueness_of(:follower_id).scoped_to([:followed_id, :followed_type]) }
  end
end
