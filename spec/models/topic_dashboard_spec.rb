# frozen_string_literal: true
# == Schema Information
#
# Table name: topic_dashboards
#
#  id         :integer          not null, primary key
#  main       :boolean          default(FALSE), indexed
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe TopicDashboard, type: :model do
  let(:subject) { build(:topic_dashboard) }

  describe 'connections' do
    it { is_expected.to have_one(:banner).dependent(:destroy) }
    it { is_expected.to have_many(:topic_modules).dependent(:destroy) }
  end

  describe 'validations' do
    describe 'banner presence' do
      context 'main topic dashboard' do
        let(:subject) { build(:topic_dashboard, main: true) }

        it { is_expected.not_to validate_presence_of(:banner) }
      end

      context 'non-main topic dashboard' do
        let(:subject) { build(:topic_dashboard, main: false) }

        it { is_expected.to validate_presence_of(:banner) }
      end
    end

    describe 'there can only be only one main dashboard' do
      let(:subject) { build(:topic_dashboard, main: true) }

      context 'there is a no main dashboard yet' do
        it { is_expected.to be_valid }
      end

      context 'there is a main dashboard already' do
        before { create(:topic_dashboard, main: true) }
        let(:non_main_dashboard) { build(:topic_dashboard, main: false) }

        it { is_expected.not_to be_valid }
        it { expect(non_main_dashboard).to be_valid }
      end
    end
  end
end
