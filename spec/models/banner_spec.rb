# frozen_string_literal: true
# == Schema Information
#
# Table name: banners
#
#  id                     :integer          not null, primary key
#  title                  :string           not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  topic_dashboard_id     :integer
#  thumbnail_file_name    :string
#  thumbnail_content_type :string
#  thumbnail_file_size    :integer
#  thumbnail_updated_at   :datetime
#

require 'rails_helper'

RSpec.describe Banner, type: :model do
  describe 'connections' do
    it { is_expected.to belong_to(:topic_dashboard) }
    it { is_expected.to have_many(:topic_modules).dependent(:destroy) }
  end

  describe 'validations' do
    let(:subject) { build(:banner) }

    before do
      image_dimensions = OpenStruct.new(height: Banner::THUMBNAIL_HEIGHT, width: Banner::THUMBNAIL_WIDTH)
      allow(Paperclip::Geometry).to receive(:from_file).and_return(image_dimensions)
    end

    it { is_expected.to validate_presence_of(:title) }

    it { is_expected.to validate_attachment_presence(:thumbnail) }
    it { is_expected.to validate_attachment_content_type(:thumbnail).allowing(['image/jpeg', 'image/png']) }
  end
end
