# frozen_string_literal: true
# == Schema Information
#
# Table name: session_tokens
#
#  id         :integer          not null, primary key
#  provider   :string           not null, indexed => [challenge]
#  challenge  :string           indexed => [provider]
#  token      :string           not null, indexed
#  device_uid :string           not null
#  user_id    :integer          not null, indexed
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe SessionToken, type: :model do
  let(:subject) { build(:session_token) }

  describe 'connections' do
    it { is_expected.to belong_to(:user) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:user).with_message('must exist') }
    it { is_expected.to validate_presence_of(:device_uid) }
    it { is_expected.to validate_presence_of(:token).on(:update) }
    it { is_expected.to validate_uniqueness_of(:token) }
    it { is_expected.to validate_inclusion_of(:provider).in_array(SessionToken::PROVIDERS) }

    context 'with facebook provider' do
      let(:subject) { build(:session_token, provider: SessionToken::PROVIDER_FACEBOOK) }

      it { is_expected.to validate_presence_of(:challenge) }
      it { is_expected.to validate_uniqueness_of(:challenge) }
    end

    context 'with twitter provider' do
      let(:subject) { build(:session_token, provider: SessionToken::PROVIDER_TWITTER) }

      it { is_expected.not_to validate_presence_of(:challenge) }
      it { is_expected.not_to validate_uniqueness_of(:challenge) }
    end
  end

  describe 'delegations' do
    it { is_expected.to delegate_method(:id).to(:user).with_prefix(true) }
    it { is_expected.to delegate_method(:name).to(:user).with_prefix(true) }
    it { is_expected.to delegate_method(:twitter_handle).to(:user).with_prefix(true) }
    it { is_expected.to delegate_method(:profile_picture_url).to(:user).with_prefix(true) }
  end

  describe 'generate token' do
    let(:subject) { create(:session_token, token: token) }

    context 'token provided' do
      let(:token) { 'some token' }

      it 'does not change the provided token' do
        expect(subject.token).to eq token
      end
    end

    context 'no token provied' do
      let(:token) { nil }

      it 'generates the token' do
        expect(subject.token).to be_present
      end
    end
  end
end
