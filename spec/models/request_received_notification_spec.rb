# frozen_string_literal: true
# == Schema Information
#
# Table name: notifications
#
#  id                :integer          not null, primary key
#  type              :string           not null
#  notification_type :string           not null
#  user_id           :integer          not null, indexed
#  subject_id        :integer          not null
#  subject_type      :string           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  read              :boolean          default(FALSE), not null
#

require 'rails_helper'

RSpec.describe RequestReceivedNotification, type: :model do
  let(:subject) { build(:request_received_notification) }

  describe 'connections' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:subject) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:user).with_message('must exist') }
    it { is_expected.to validate_presence_of(:subject).with_message('must exist') }
  end

  describe '#set_notification_type' do
    let(:subject) { create(:request_received_notification) }
    it 'has the correct notification type' do
      expect(subject.notification_type).to eq Notification::REQUEST_RECEIVED
    end
  end
end
