# frozen_string_literal: true
# == Schema Information
#
# Table name: offense_reports
#
#  id         :integer          not null, primary key
#  reason     :string           not null, indexed => [opinion_id, user_id]
#  opinion_id :integer          not null, indexed, indexed => [user_id, reason]
#  user_id    :integer          not null, indexed => [opinion_id, reason], indexed
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe OffenseReport, type: :model do
  let(:subject) { build(:offense_report) }

  describe 'connections' do
    it { is_expected.to belong_to(:opinion) }
    it { is_expected.to belong_to(:user) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:reason) }
    it { is_expected.to validate_presence_of(:opinion).with_message('must exist') }
    it { is_expected.to validate_presence_of(:user).with_message('must exist') }
    it { is_expected.to validate_uniqueness_of(:opinion_id).scoped_to([:user_id, :reason]) }
  end
end
