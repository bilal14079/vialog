# frozen_string_literal: true
# == Schema Information
#
# Table name: topics
#
#  id                   :integer          not null, primary key
#  title                :string           not null, indexed
#  category_id          :integer          indexed
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  opinions_count       :integer          default(0), not null
#  thumbnail_url        :string
#  countries            :string
#  password             :string
#  total_reaction_count :integer          default(0), not null
#  topic_locale_id      :integer          indexed
#  watch_time           :integer          default(0), not null
#  keywords             :string
#  profile_topic        :boolean          default(FALSE)
#  creator_id           :integer          indexed
#  clap_count           :integer
#  follower_count       :integer
#  last_opinion_time    :datetime
#

require 'rails_helper'

RSpec.describe Topic, type: :model do
  let(:subject) { build(:topic) }

  describe 'connections' do
    it { is_expected.to belong_to(:category) }
    it { is_expected.to have_many(:opinions).dependent(:destroy) }
    it { is_expected.to have_many(:taggings).dependent(:destroy) }
    it { is_expected.to have_many(:hashtags).through(:taggings).source(:hashtag) }
    it { is_expected.to have_many(:followings).dependent(:destroy) }
    it { is_expected.to have_many(:follower_users).through(:followings).source(:follower) }
    it { is_expected.to have_many(:opinion_requests).dependent(:destroy) }
    it { is_expected.to have_many(:users_wanted_for_opinion).through(:opinion_requests).source(:requested_user) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:category).with_message('must exist') }
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_uniqueness_of(:title) }

    context 'new record with hashtags' do
      let(:hashtag) { create(:hashtag) }
      let(:subject) { build(:topic, hashtag_ids: [hashtag.id]) }

      it { is_expected.to be_valid }
    end
  end

  describe 'opinions' do
    let(:subject) { create(:topic_with_opinions) }
    let!(:unprocessed_opinion) { create(:unprocessed_opinion, topic: subject) }

    it 'only containt processed opinions' do
      expect(subject.opinions).not_to include(unprocessed_opinion)
    end
  end

  describe '#opinions_count' do
    let(:subject) { create(:topic_with_opinions) }

    it 'is incremented when a new opinion is posted to the topic' do
      expect { create(:opinion, topic: subject) }.to change { subject.reload.opinions_count }.by(1)
    end

    it 'is decremented when an opinion is removed from the topic' do
      expect { subject.opinions.first.destroy }.to change { subject.reload.opinions_count }.by(-1)
    end
  end

  describe 'opinion_requests' do
    let(:subject) { create(:topic_with_opinion_requests) }
    let!(:fullfilled_opinion_request) { create(:fullfilled_opinion_request, topic: subject) }

    it 'only containt unfullfilled opinion requests' do
      expect(subject.opinion_requests).not_to include(fullfilled_opinion_request)
    end
  end

  describe 'users_wanted_for_opinion' do
    let(:subject) { create(:topic) }
    let(:user) { create(:user) }
    let!(:opinion_request_1) { create(:opinion_request, requested_user: user, topic: subject) }
    let!(:opinion_request_2) { create(:opinion_request, requested_user: user, topic: subject) }

    it 'is does not contain the same user twice' do
      expect(subject.users_wanted_for_opinion.count).to eq(1)
    end
  end
end
