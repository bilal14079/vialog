# frozen_string_literal: true
# == Schema Information
#
# Table name: opinions
#
#  id                        :integer          not null, primary key
#  topic_id                  :integer          not null, indexed
#  user_id                   :integer          not null, indexed
#  feature_factor            :integer          default(0), not null
#  video_hls_url             :string
#  video_thumbnail_url       :string
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  offense_reports_count     :integer          default(0), not null
#  reaction_love_count       :integer          default(0), not null
#  reaction_haha_count       :integer          default(0), not null
#  reaction_wow_count        :integer          default(0), not null
#  reaction_sad_count        :integer          default(0), not null
#  reaction_angry_count      :integer          default(0), not null
#  video_file_name           :string
#  video_content_type        :string
#  video_file_size           :integer
#  video_updated_at          :datetime
#  elastic_transcoder_job_id :string           indexed
#  processed                 :boolean          default(FALSE), indexed
#  token                     :string           default(""), not null
#  duration                  :decimal(6, 2)    default(0.0)
#  lat                       :decimal(, )
#  lng                       :decimal(, )
#  locked_at                 :datetime
#  locked_days               :integer          default(-1), not null
#  total_reaction_count      :integer          default(0), not null
#  watch_time                :integer          default(0), not null
#  device_id                 :string
#  bad_word_count            :integer          default(-1)
#  quality                   :string           default("hd")
#  transcoding_round         :integer          default(0)
#  clap_count                :integer
#  cover                     :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe Opinion, type: :model do
  let(:subject) { build(:opinion) }

  describe 'attachments' do
    it { is_expected.to have_attached_file(:video) }
  end

  describe 'connections' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:topic) }
    it { is_expected.to have_many(:offense_reports).dependent(:destroy) }
    it { is_expected.to have_many(:opinion_reactions).dependent(:destroy) }
    it { is_expected.to validate_presence_of(:token).on(:update) }
    it { is_expected.to validate_uniqueness_of(:token) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:user).with_message('must exist') }
    it { is_expected.to validate_presence_of(:topic).with_message('must exist') }

    it { is_expected.to validate_attachment_presence(:video) }
    it { is_expected.to validate_attachment_content_type(:video).allowing('video/quicktime') }
  end

  # TODO: write spec about the _count fields
end
