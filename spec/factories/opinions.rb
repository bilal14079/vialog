# frozen_string_literal: true
# == Schema Information
#
# Table name: opinions
#
#  id                        :integer          not null, primary key
#  topic_id                  :integer          not null, indexed
#  user_id                   :integer          not null, indexed
#  feature_factor            :integer          default(0), not null
#  video_hls_url             :string
#  video_thumbnail_url       :string
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  offense_reports_count     :integer          default(0), not null
#  reaction_love_count       :integer          default(0), not null
#  reaction_haha_count       :integer          default(0), not null
#  reaction_wow_count        :integer          default(0), not null
#  reaction_sad_count        :integer          default(0), not null
#  reaction_angry_count      :integer          default(0), not null
#  video_file_name           :string
#  video_content_type        :string
#  video_file_size           :integer
#  video_updated_at          :datetime
#  elastic_transcoder_job_id :string           indexed
#  processed                 :boolean          default(FALSE), indexed
#  token                     :string           default(""), not null
#  duration                  :decimal(6, 2)    default(0.0)
#  lat                       :decimal(, )
#  lng                       :decimal(, )
#  locked_at                 :datetime
#  locked_days               :integer          default(-1), not null
#  total_reaction_count      :integer          default(0), not null
#  watch_time                :integer          default(0), not null
#  device_id                 :string
#  bad_word_count            :integer          default(-1)
#  quality                   :string           default("hd")
#  transcoding_round         :integer          default(0)
#  clap_count                :integer
#  cover                     :boolean          default(FALSE)
#

FactoryGirl.define do
  factory :opinion do
    association :topic
    association :user

    transient do
      video_name %w(beasts beasts_mark_kermode beasts_ricquetta).sample
    end

    video_file_name 'test.mov'
    video_content_type 'video/quicktime'
    video_file_size 10.kilobytes
    video_updated_at { Time.current }

    video_hls_url { "https://s3-eu-west-1.amazonaws.com/towards-dev/beasts/#{video_name}/#{video_name}.m3u8" }
    video_thumbnail_url { "https://s3-eu-west-1.amazonaws.com/towards-dev/beasts/#{video_name}/thumb.png" }

    processed true

    factory :unprocessed_opinion do
      video_hls_url ''
      video_thumbnail_url ''
      processed false
    end
  end
end
