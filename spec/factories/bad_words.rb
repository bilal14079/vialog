# == Schema Information
#
# Table name: bad_words
#
#  id         :integer          not null, primary key
#  language   :string
#  word       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  profanity  :boolean          default(TRUE)
#

FactoryGirl.define do
  factory :bad_word do
    language "MyString"
    word "MyString"
  end
end
