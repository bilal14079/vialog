# == Schema Information
#
# Table name: user_settings
#
#  id         :integer          not null, primary key
#  user_id    :integer          indexed
#  key        :string
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :user_setting do
    user nil
    setting_key "MyString"
    settings_value "MyString"
  end
end
