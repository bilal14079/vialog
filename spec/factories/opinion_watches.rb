# == Schema Information
#
# Table name: opinion_watches
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null, indexed
#  opinion_id :integer          not null, indexed
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :opinion_watch do
    user nil
    opinion nil
  end
end
