# frozen_string_literal: true
# == Schema Information
#
# Table name: opinion_reactions
#
#  id              :integer          not null, primary key
#  opinion_id      :integer          not null, indexed, indexed => [user_id, reaction_type, reaction_second]
#  user_id         :integer          not null, indexed, indexed => [opinion_id, reaction_type, reaction_second]
#  reaction_type   :string           not null, indexed => [opinion_id, user_id, reaction_second]
#  reaction_second :integer          not null, indexed => [opinion_id, user_id, reaction_type]
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  trait :opinion_reaction_base do
    association :user
    association :opinion

    reaction_second { rand(0...60) }
  end

  factory :love_opinion_reaction, class: OpinionReaction, traits: [:opinion_reaction_base] do
    reaction_type 'love'
  end

  factory :haha_opinion_reaction, class: OpinionReaction, traits: [:opinion_reaction_base] do
    reaction_type 'haha'
  end

  factory :wow_opinion_reaction, class: OpinionReaction, traits: [:opinion_reaction_base] do
    reaction_type 'wow'
  end

  factory :sad_opinion_reaction, class: OpinionReaction, traits: [:opinion_reaction_base] do
    reaction_type 'sad'
  end

  factory :angry_opinion_reaction, class: OpinionReaction, traits: [:opinion_reaction_base] do
    reaction_type 'angry'
  end
end
