# == Schema Information
#
# Table name: opinion_translations
#
#  id             :integer          not null, primary key
#  opinion_id     :integer          indexed
#  text           :text
#  language       :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  bad_word_count :integer
#

FactoryGirl.define do
  factory :opinion_translation do
    opinion nil
    text "MyString"
    language "MyText"
  end
end
