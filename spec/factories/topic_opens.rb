# == Schema Information
#
# Table name: topic_opens
#
#  id         :integer          not null, primary key
#  topic_id   :integer          indexed
#  user_id    :integer          indexed
#  time       :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :topic_open do
    topic nil
    user nil
    time "2017-06-22 11:59:46"
  end
end
