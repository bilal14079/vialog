# frozen_string_literal: true
# == Schema Information
#
# Table name: topic_modules
#
#  id                 :integer          not null, primary key
#  type               :string           not null
#  topic_dashboard_id :integer          not null, indexed
#  position           :integer          not null
#  title              :string           not null
#  display_type       :string           not null
#  category_id        :integer          indexed
#  banner_id          :integer          indexed
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  title_bg_color     :string
#  title_color        :string
#  recents            :boolean
#  topic_limit        :integer          default(5)
#

FactoryGirl.define do
  trait :topic_module_basics do
    association :topic_dashboard
    sequence(:title) { |n| "topic module #{n}" }
    display_type { TopicModule::DISPLAY_TYPES.first }
  end

  factory :category_topic_module, class: TopicModule, traits: [:topic_module_basics] do
    association :category
  end

  factory :hashtag_topic_module, class: TopicModule, traits: [:topic_module_basics] do
    transient do
      hashtags { create_list :hashtag, 2 }
    end

    hashtag_ids { hashtags.pluck(:id) }
  end

  factory :banner_topic_module, class: TopicModule, traits: [:topic_module_basics] do
    association :banner
  end
end
