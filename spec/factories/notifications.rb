# frozen_string_literal: true
# == Schema Information
#
# Table name: notifications
#
#  id                :integer          not null, primary key
#  type              :string           not null
#  notification_type :string           not null
#  user_id           :integer          not null, indexed
#  subject_id        :integer          not null
#  subject_type      :string           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  read              :boolean          default(FALSE), not null
#

FactoryGirl.define do
  trait :notification_base do
    association :user
  end

  factory :notification, traits: [:notification_base] do
    association :subject, factory: :user
    type 'Notification'
    notification_type { Notification::NOTIFICATION_TYPES.sample }
  end

  factory :reaction_received_notification, traits: [:notification_base],
                                           class: 'ReactionReceivedNotification' do
    association :subject, factory: :love_opinion_reaction
  end

  factory :facebook_friend_joined_notification, traits: [:notification_base],
                                                class: 'FacebookFriendJoinedNotification' do
    association :subject, factory: :user
  end

  factory :request_received_notification, traits: [:notification_base],
                                          class: 'RequestReceivedNotification' do
    association :subject, factory: :opinion_request
  end

  factory :request_answered_notification, traits: [:notification_base],
                                          class: 'RequestAnsweredNotification' do
    association :subject, factory: :fullfilled_opinion_request
  end

  factory :transcoding_done_notification, traits: [:notification_base],
                                          class: 'TranscodingDoneNotification' do
    association :subject, factory: :opinion
  end

  factory :user_followed_notification, traits: [:notification_base],
                                       class: 'UserFollowedNotification' do
    association :subject, factory: :user_following
  end
end
