# == Schema Information
#
# Table name: action_items
#
#  id          :integer          not null, primary key
#  actor_id    :integer          not null, indexed
#  target_id   :integer          not null
#  target_type :string           not null
#  action_type :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :action_item do
    actor nil
    target nil
    type ""
  end
end
