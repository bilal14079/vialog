# frozen_string_literal: true
# == Schema Information
#
# Table name: hashtags
#
#  id         :integer          not null, primary key
#  name       :string           not null, indexed
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :hashtag do
    sequence(:name) { |n| "tag #{n}" }
  end
end
