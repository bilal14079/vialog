# frozen_string_literal: true
# == Schema Information
#
# Table name: banners
#
#  id                     :integer          not null, primary key
#  title                  :string           not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  topic_dashboard_id     :integer
#  thumbnail_file_name    :string
#  thumbnail_content_type :string
#  thumbnail_file_size    :integer
#  thumbnail_updated_at   :datetime
#

FactoryGirl.define do
  factory :banner do
    sequence(:title) { |n| "banner #{n}" }

    thumbnail_file_name 'test.jpeg'
    thumbnail_content_type 'image/jpeg'
    thumbnail_file_size 10.kilobytes
    thumbnail_updated_at { Time.current }
  end
end
