# frozen_string_literal: true
# == Schema Information
#
# Table name: session_tokens
#
#  id         :integer          not null, primary key
#  provider   :string           not null, indexed => [challenge]
#  challenge  :string           indexed => [provider]
#  token      :string           not null, indexed
#  device_uid :string           not null
#  user_id    :integer          not null, indexed
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :session_token do
    association :user
    provider 'facebook'
    sequence(:challenge) { |n| "facebook_uid_#{n}" }
    sequence(:device_uid) { |n| "device_uid_#{n}" }

    trait :with_twitter_provider do
      provider 'twitter'
      sequence(:challenge) { |n| "twitter_uid_#{n}" }
    end
  end
end
