# == Schema Information
#
# Table name: topic_locales
#
#  id           :integer          not null, primary key
#  name         :string           not null
#  country_code :string
#  latitude     :decimal(, )
#  longitude    :decimal(, )
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  radius       :decimal(, )
#

FactoryGirl.define do
  factory :topic_locale do
    name "MyString"
    country_code "MyString"
    latitude "9.99"
    longitude "9.99"
  end
end
