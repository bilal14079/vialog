# frozen_string_literal: true
# == Schema Information
#
# Table name: followings
#
#  id            :integer          not null, primary key
#  follower_id   :integer          not null, indexed => [followed_id, followed_type]
#  followed_id   :integer          not null, indexed => [follower_id, followed_type]
#  followed_type :string           not null, indexed => [follower_id, followed_id]
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryGirl.define do
  trait :following_base do
    association :follower, factory: :user
  end

  factory :user_following, class: Following, traits: [:following_base] do
    association :followed, factory: :user
  end

  factory :topic_following, class: Following, traits: [:following_base] do
    association :followed, factory: :topic
  end
end
