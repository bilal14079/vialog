# frozen_string_literal: true
# == Schema Information
#
# Table name: opinion_requests
#
#  id                :integer          not null, primary key
#  requester_user_id :integer          not null, indexed, indexed => [requested_user_id, topic_id]
#  requested_user_id :integer          not null, indexed, indexed => [requester_user_id, topic_id]
#  topic_id          :integer          not null, indexed, indexed => [requester_user_id, requested_user_id]
#  opinion_id        :integer          indexed
#  fullfilled        :boolean          default(FALSE)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

FactoryGirl.define do
  factory :opinion_request do
    association :requester_user, factory: :user
    association :requested_user, factory: :user
    association :topic

    fullfilled false

    factory :fullfilled_opinion_request do
      fullfilled true
      opinion { create(:opinion, topic: topic, user: requested_user) }
    end
  end
end
