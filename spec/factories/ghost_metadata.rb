# == Schema Information
#
# Table name: ghost_metadata
#
#  id              :integer          not null, primary key
#  creator_user_id :integer          indexed
#  ghost_user_id   :integer          indexed
#  topic_id        :integer          indexed
#  ask_time        :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :ghost_metadatum do
    creator_user nil
    ghost_user nil
    topic nil
    ask_time "2017-04-28 13:47:38"
  end
end
