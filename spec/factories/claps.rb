# == Schema Information
#
# Table name: claps
#
#  id         :integer          not null, primary key
#  opinion_id :integer          indexed
#  user_id    :integer          indexed
#  start      :integer
#  duration   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :clap do
    opinion nil
    user nil
    start 1
    duration 1
  end
end
