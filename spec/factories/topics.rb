# frozen_string_literal: true
# == Schema Information
#
# Table name: topics
#
#  id                   :integer          not null, primary key
#  title                :string           not null, indexed
#  category_id          :integer          indexed
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  opinions_count       :integer          default(0), not null
#  thumbnail_url        :string
#  countries            :string
#  password             :string
#  total_reaction_count :integer          default(0), not null
#  topic_locale_id      :integer          indexed
#  watch_time           :integer          default(0), not null
#  keywords             :string
#  profile_topic        :boolean          default(FALSE)
#  creator_id           :integer          indexed
#  clap_count           :integer
#  follower_count       :integer
#  last_opinion_time    :datetime
#

FactoryGirl.define do
  factory :topic do
    association :category
    sequence(:title) { |n| "topic #{n}" }

    factory :topic_with_opinions do
      transient do
        generated_opinions_count 2
      end

      after(:create) do |topic, evaluator|
        create_list :opinion, evaluator.generated_opinions_count.to_i, topic: topic
      end
    end

    factory :topic_with_opinion_requests do
      transient do
        generated_opinion_requests_count 4
      end

      after(:create) do |topic, evaluator|
        create_list :opinion_request, evaluator.generated_opinion_requests_count.to_i, topic: topic
      end
    end
  end
end
