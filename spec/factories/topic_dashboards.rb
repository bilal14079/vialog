# frozen_string_literal: true
# == Schema Information
#
# Table name: topic_dashboards
#
#  id         :integer          not null, primary key
#  main       :boolean          default(FALSE), indexed
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :topic_dashboard do
    main false
    association :banner

    trait :main do
      main :true
      banner nil
    end

    factory :topic_dashboard_with_modules do
      after(:create) do |topic_dashboard|
        topic_dashboard.topic_modules << create(:category_topic_module)
        topic_dashboard.topic_modules << create(:hashtag_topic_module)
        topic_dashboard.topic_modules << create(:banner_topic_module)
      end
    end
  end
end
