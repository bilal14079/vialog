# frozen_string_literal: true
# == Schema Information
#
# Table name: mobile_app_versions
#
#  id                 :integer          not null, primary key
#  platform           :string           not null, indexed => [version_identifier]
#  update_severity    :string           not null
#  version_identifier :string           not null, indexed => [platform]
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

FactoryGirl.define do
  factory :mobile_app_version do
    platform { MobileAppVersion::PLATFORMS.first }
    sequence(:version_identifier) { |n| "1.0.#{n}" }
    update_severity { MobileAppVersion::SOFT_SEVERITY }
  end
end
