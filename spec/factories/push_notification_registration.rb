# frozen_string_literal: true
# == Schema Information
#
# Table name: push_notification_registrations
#
#  id                      :integer          not null, primary key
#  user_id                 :integer          not null, indexed, indexed => [notification_type, device_uid]
#  push_notification_token :string           not null
#  notification_type       :string           not null, indexed, indexed => [user_id, device_uid]
#  device_uid              :string           not null, indexed => [user_id, notification_type]
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

FactoryGirl.define do
  factory :push_notification_registration do
    association :user
    sequence(:push_notification_token) { |n| "push_notification_token#{n}" }
    sequence(:device_uid) { |n| "device_uid_#{n}" }
    notification_type { Notification::NOTIFICATION_TYPES.sample }
  end
end
