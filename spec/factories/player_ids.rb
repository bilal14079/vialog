# == Schema Information
#
# Table name: player_ids
#
#  id         :integer          not null, primary key
#  user_id    :integer          indexed
#  device_uid :string
#  player_id  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :player_id do
    user nil
    device_uid "MyString"
    player_id "MyString"
  end
end
