# frozen_string_literal: true
# == Schema Information
#
# Table name: taggings
#
#  id            :integer          not null, primary key
#  hashtag_id    :integer          not null, indexed => [taggable_id, taggable_type]
#  taggable_id   :integer          not null, indexed => [hashtag_id, taggable_type]
#  taggable_type :string           not null, indexed => [hashtag_id, taggable_id]
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryGirl.define do
  trait :tagging_base do
    association :hashtag
  end

  factory :category_tagging, class: Tagging, traits: [:tagging_base] do
    association :taggable, factory: :category
  end
end
