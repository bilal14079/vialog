# frozen_string_literal: true
# == Schema Information
#
# Table name: users
#
#  id                    :integer          not null, primary key
#  name                  :string           not null, indexed
#  profile_picture_url   :string           not null
#  facebook_uid          :string           indexed
#  twitter_uid           :string           indexed
#  twitter_handle        :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  opinions_count        :integer          default(0), not null
#  follower_users_count  :integer          default(0), not null
#  followed_users_count  :integer          default(0), not null
#  followed_topics_count :integer          default(0), not null
#  email                 :string
#  last_lat              :decimal(, )
#  last_lng              :decimal(, )
#  last_country          :string
#  guest                 :boolean          default(FALSE)
#  ghost                 :boolean          default(FALSE)
#  bio                   :text
#  profile_video_id      :integer          indexed
#  handle                :string
#  intercom_status       :string           default("unregistered")
#  seen_time             :integer
#  hosted_issues_count   :integer
#  open_asks_count       :integer
#  responded_asks_count  :integer
#  last_login            :datetime
#

FactoryGirl.define do
  factory :user do
    sequence(:name) { |n| "towards user #{n}" }
    sequence(:profile_picture_url) { |n| "https://robohash.org/towards-#{n}.png" }
    sequence(:facebook_uid) { |n| "facebook_uid_#{n}" }
    sequence(:twitter_uid) { |n| "twitter_uid_#{n}" }

    trait :with_real_facebook_uid do
      name 'David Horvath'
      facebook_uid '1412114918802057'
      profile_picture_url "https://graph.facebook.com/#{facebook_uid}/picture?width=500&height=500"
    end

    trait :with_real_twitter_uid do
      name 'reden87'
      twitter_uid '35710905'
      twitter_handle 'reden87'
      profile_picture_url "https://twitter.com/#{twitter_handle}/profile_image?size=normal"
    end

    factory :user_with_opinions do
      after(:create) do |user|
        create_list :opinion, 2, user: user
      end
    end

    trait :with_followed_users do
      after(:create) do |user|
        create_list :user_following, 2, follower: user
      end
    end

    trait :with_followed_topics do
      after(:create) do |user|
        create_list :topic_following, 2, follower: user
      end
    end

    trait :with_requested_opinion_requests do
      after(:create) do |user|
        create_list :opinion_request, 2, requester_user: user
      end
    end

    trait :with_received_opinion_requests do
      after(:create) do |user|
        create_list :opinion_request, 2, requested_user: user
      end
    end

    trait :with_notifications do
      after(:create) do |user|
        create :reaction_received_notification, user: user
        create :request_received_notification, user: user
        create :request_answered_notification, user: user
        create :facebook_friend_joined_notification, user: user
        create :user_followed_notification, user: user
        create :transcoding_done_notification, user: user
      end
    end

    factory :user_with_requested_opinion_requests, traits: [:with_requested_opinion_requests]
    factory :user_with_received_opinion_requests, traits: [:with_received_opinion_requests]
    factory :user_with_followed_topics, traits: [:with_followed_topics]
    factory :user_with_followed_users, traits: [:with_followed_users]
    factory :user_with_followed_topics_and_users, traits: [:with_followed_topics, :with_followed_users]
    factory :user_with_notifications, traits: [:with_notifications]
  end
end
