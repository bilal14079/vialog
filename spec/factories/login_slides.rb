# == Schema Information
#
# Table name: login_slides
#
#  id                 :integer          not null, primary key
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  text               :text
#  dot_color          :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  animated           :boolean
#  looped             :boolean
#  display_order      :integer
#

FactoryGirl.define do
  factory :login_slide do
    image ""
    text "MyText"
    dot_color "MyString"
  end
end
