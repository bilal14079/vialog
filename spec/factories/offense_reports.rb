# frozen_string_literal: true
# == Schema Information
#
# Table name: offense_reports
#
#  id         :integer          not null, primary key
#  reason     :string           not null, indexed => [opinion_id, user_id]
#  opinion_id :integer          not null, indexed, indexed => [user_id, reason]
#  user_id    :integer          not null, indexed => [opinion_id, reason], indexed
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :offense_report do
    association :opinion
    association :user
    reason 'Reason of the reoprt'
  end
end
