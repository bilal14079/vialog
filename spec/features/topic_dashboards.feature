Feature: Topic dashboard by id
  Background:
    Given I have a topic dashboard with id "42424242" with various topic modules

  Scenario: 'non-existent topic dashboard'
    When I send a GET JSON request to "v1/topic_dashboards/333" with
      """
      {}
      """
    Then the response status code will be "404"

  Scenario: 'unathenticated request'
    When I send a GET JSON request to "v1/topic_dashboards/42424242" with
      """
      {}
      """
    Then the response status code will be "200"
    And the JSON response will have "1" "$.topic_modules" key

  Scenario: 'athenticated request'
    When I send a user authenticated GET JSON request to "v1/topic_dashboards/42424242" with
      """
      {}
      """
    Then the response status code will be "200"
    And the JSON response will have "1" "$.topic_modules" key
