Feature: Topic search
  Background:
    Given I have the following "topic"s:
    | title          |
    | yay for oscars |
    | debate         |
    | purple one     |
    | purple two     |
    | purple three   |

  Scenario: 'search string missing'
    When I send a GET JSON request to "v1/search/topics" with
      """
      {}
      """
    Then the response status code will be "400"
    And the JSON response will have "$.error" with the value "q is missing"

  Scenario: 'search topic by title'
    When I send a GET JSON request to "v1/search/topics" with
      """
      {
        "q": "oscar"
      }
      """
    Then the response status code will be "200"
    And the JSON response will have "1" "$.topics" key
    And the JSON response will have "$.topics[0].title" with the value "yay for oscars"

  Scenario: 'search wiht limit'
    When I send a GET JSON request to "v1/search/topics" with
      """
      {
        "q": "purple",
        "limit": 2
      }
      """
    Then the response status code will be "200"
    And the JSON response will have "2" "$..title" key

  Scenario: 'authenticated search'
    When I send a user authenticated GET JSON request to "v1/search/topics" with
      """
      {
        "q": "debate"
      }
      """
    Then the response status code will be "200"
    And the JSON response will have "1" "$..followed_by_current_user" key
