Feature: Profile
  Scenario: 'update the profile of the current user'
    Given I send a user authenticated POST JSON request to "v1/me" with
      """
      {
        "name": "George Smith"
      }
      """
    Then the response status code will be "201"
    And the JSON response will have "$user.name" with the value "George Smith"

  Scenario: 'get non-existent user\'s profile'
    Given I send a user authenticated GET JSON request to "v1/users/666" with
      """
        {}
      """
    Then the response status code will be "404"

  Scenario: 'get existent user\'s profile'
    Given I have the following "user_with_opinions":
    | id   | name         | profile_picture_url                | twitter_handle |
    | 2323 | George Smith | https://robohash.org/towards-1.png | GeorgeSmith    |
    And I send a user authenticated GET JSON request to "v1/users/2323" with
      """
        {}
      """
    Then the response status code will be "200"
    And the JSON response will have "$user.name" with the value "George Smith"
    And the JSON response will have "$user.profile_picture_url" with the value "https://robohash.org/towards-1.png"
    And the JSON response will have "$user.twitter_handle" with the value "GeorgeSmith"
    And the JSON response will have "1" "$..follower_users_count" key
    And the JSON response will have "1" "$..followed_users_count" key
    And the JSON response will have "1" "$..followed_topics_count" key
    And the JSON response will contain some "$..opinions_count" keys
    And the JSON response will have "1" "$..followed_topics" key
    And the JSON response will have "1" "$..opinioned_topics" key
    And the JSON response will contain some "$..followed_by_current_user" keys
    And the JSON response will contain some "$..profiled_user_opinions" keys
