Feature: Logout
  Scenario: 'with registered user with valid session token'
    Given I have a registered facebook user with name "John Smith" and with token "12345"
    And I send a user authenticated POST JSON request to "v1/logout" with
      """
        {}
      """
    Then the response status code will be "201"
    And the JSON response will have "$.success" with the value "true"
