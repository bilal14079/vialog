Feature: users wanted for opinion in topic
  Background:
    Given there is user with a device uid "test_device" and a token "test_token"
    Given I have the following "topic_with_opinion_requests"s:
    | id       | generated_opinion_requests_count |
    | 42424242 | 4                                |

  Scenario: 'non-existent topic'
    Given I send a GET JSON request to "v1/topics/333/users_wanted_for_opinion" with
      """
        {}
      """
    Then the response status code will be "404"

  Scenario: 'existing topic - unauthenticated request'
    Given I send a GET JSON request to "v1/topics/42424242/users_wanted_for_opinion" with
      """
        {}
      """
    Then the response status code will be "200"
    And the JSON response will have "1" "$.users" key
    And the JSON response will contain some "$..opinion_requests_count" keys

  Scenario: 'existing topic - with limit'
    Given I send a GET JSON request to "v1/topics/42424242/users_wanted_for_opinion" with
      """
        {
          "limit": 2
        }
      """
    Then the response status code will be "200"
    And the JSON response will have "1" "$.users" key
    And the JSON response will have "2" "$..id" keys

  Scenario: 'existing topic - authenticated request'
    Given I send a user authenticated GET JSON request to "v1/topics/42424242/users_wanted_for_opinion" with
      """
        {}
      """
    Then the response status code will be "200"
    And the JSON response will have "1" "$.users" key
    And the JSON response will contain some "$..opinion_requests_count" keys
    And the JSON response will contain some "$..received_opinion_request_from_current_user" keys
