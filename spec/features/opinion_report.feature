Feature: Opinion report
  Scenario: 'properly authenticated request - report an opinion with reason'
    Given  I have the following "opinion":
    | id   |
    | 2323 |
    And I send a user authenticated POST JSON request to "v1/opinions/2323/report" with
      """
        {
          "reason": "Reason of the report"
        }
      """
    Then the response status code will be "201"
    And the JSON response will have "$.success" with the value "true"

  Scenario: 'properly authenticated request - report a non-existent opinion'
    Given  I send a user authenticated POST JSON request to "v1/opinions/5656/report" with
      """
        {
          "reason": "Reason of the report"
        }
      """
    Then the response status code will be "404"

  Scenario: 'properly authenticated request - report an opinion user without reason'
    Given  I have the following "opinion":
    | id   |
    | 2323 |
    And I send a user authenticated POST JSON request to "v1/opinions/2323/report" with
      """
        {}
      """
    Then the response status code will be "400"

  Scenario: 'not authenticated request - report an opinion with reason'
    Given  I have the following "opinion":
    | id   |
    | 2323 |
    And I send a POST JSON request to "v1/opinions/2323/report" with
      """
        {
          "reason": "Reason of the report"
        }
      """
    Then the response status code will be "403"
