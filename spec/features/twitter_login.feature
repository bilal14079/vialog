Feature: Twitter login
  Scenario: 'with a new user'
    Given I send a POST JSON request to "v1/login/twitter" with valid twitter access token
    Then the response status code will be "201"
    And the JSON response will have "$user.name" with the value "Lilla Tothh"
    And the JSON response will have "$user.profile_picture_url" with the value "https://twitter.com/TothhLilla/profile_image?size=normal"

  Scenario: 'with registered user but with invalid session token'
    Given  I have the following "user"s:
    | id | name        | twitter_uid | profile_picture_url                |
    | 1  | Lilla Tothh | 707475986   | https://robohash.org/towards-1.png |
    And I send a POST JSON request to "v1/login/twitter" with valid twitter access token
    Then the response status code will be "201"
    And the JSON response will have "$user.name" with the value "Lilla Tothh"
    And the JSON response will have "$user.profile_picture_url" with the value "https://robohash.org/towards-1.png"

  Scenario: 'with registered user with valid session token'
    Given I have a registered twitter user with name "John Smith" and with token "12345"
    And I send a POST JSON request to "v1/login/twitter" with valid twitter access token
    Then the response status code will be "201"
    And the JSON response will have "$user.name" with the value "John Smith"
    And the JSON response will have "$user.token" with the value "12345"

  Scenario: 'with a new user but with invalid twitter acces token'
    Given I send a POST JSON request to "v1/login/twitter" with
        """
          {"access_token": "twitter_acces_token",
           "access_token_secret": "twitter_acces_token_secret",
           "device_uid": "device_uid_test"}
        """
    Then the response status code will be "400"
