Feature: user facebook friends
  Background:
    Given there is user with a device uid "test_device" and a token "test_token"
    Given I have the following "user"s:
    | name | facebook_uid    |
    | joe  | 137298110060607 |

  Scenario: 'missing device uid'
    Given I send a GET JSON request to "v1/me/facebook_friends" with
      """
        {}
      """
    Then the response status code will be "403"
    And the JSON response will have "$.error" with the value "Missing X-Towards-Device-Uuid"

  Scenario: 'invalid session token'
    Given I send a user authenticated GET JSON request with invalid session token to "v1/me/facebook_friends" with
      """
        {}
      """
    Then the response status code will be "403"
    And the JSON response will have "$.error" with the value "Invalid X-Towards-Session-Token"

  Scenario: 'twitter session token'
    Given I send a twitter user authenticated GET JSON request to "v1/me/facebook_friends" with
      """
        {}
      """
    Then the response status code will be "403"
    And the JSON response will have "$.error" with the value "Invalid X-Towards-Session-Token, provider is not facebook"


  Scenario: 'properly authenticated request'
    Given I send a user authenticated GET JSON request to "v1/me/facebook_friends" with
      """
        {}
      """
    Then the response status code will be "200"
    And the JSON response will have "1" "$.users" key

