Feature: Facebook login
  Scenario: 'with a new user'
    Given I send a POST JSON request to "v1/login/facebook" with valid facebook access token
    Then the response status code will be "201"
    And the JSON response will have "$user.name" with the value "Open User"
    And the JSON response will have "$user.profile_picture_url" with the value "https://graph.facebook.com/116640498794166/picture?width=500&height=500"

  Scenario: 'with registered user but with invalid session token'
    Given  I have the following "user"s:
    | id | name       | facebook_uid    | profile_picture_url                |
    | 1  | Open User  | 116640498794166 | https://robohash.org/towards-1.png |
    And I send a POST JSON request to "v1/login/facebook" with valid facebook access token
    Then the response status code will be "201"
    And the JSON response will have "$user.name" with the value "Open User"
    And the JSON response will have "$user.profile_picture_url" with the value "https://robohash.org/towards-1.png"

  Scenario: 'with registered user with valid session token'
    Given I have a registered facebook user with name "John Smith" and with token "12345"
    And I send a POST JSON request to "v1/login/facebook" with valid facebook access token
    Then the response status code will be "201"
    And the JSON response will have "$user.name" with the value "John Smith"
    And the JSON response will have "$user.token" with the value "12345"

  Scenario: 'with a new user but with invalid facebok acces token'
    Given I send a POST JSON request to "v1/login/facebook" with
        """
          {"fb_access_token": "fb_acces_token",
           "device_uid": "device_uid_test"}
        """
    Then the response status code will be "400"
