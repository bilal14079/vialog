Feature: Opinion delete
  Scenario: 'properly authenticated request - delete opinion of current user'
    Given I have the following "opinion" for the current user:
    | id   |
    | 2323 |
    And I send a user authenticated DELETE JSON request to "v1/opinions/2323" with
      """
        {}
      """
    Then the response status code will be "200"
    And the JSON response will have "$.success" with the value "true"

  Scenario: 'properly authenticated request - delete opinion of other user'
    Given I have the following "user":
    | id     |
    | 565656 |
    And I have the following "opinion":
    | id   | user_id |
    | 7474 | 565656  |
    And I send a user authenticated DELETE JSON request to "v1/opinions/7474" with
      """
        {}
      """
    Then the response status code will be "404"
