Feature: User search
  Background:
    Given I have the following "user"s:
    | name        |
    | mary ann    |
    | kate        |
    | perry one   |
    | perry two   |
    | perry three |

  Scenario: 'search string missing'
    When I send a GET JSON request to "v1/search/users" with
      """
      {}
      """
    Then the response status code will be "400"
    And the JSON response will have "$.error" with the value "q is missing"

  Scenario: 'search user by name'
    When I send a GET JSON request to "v1/search/users" with
      """
      {
        "q": "ann"
      }
      """
    Then the response status code will be "200"
    And the JSON response will have "1" "$.users" key
    And the JSON response will have "$.users[0].name" with the value "mary ann"

  Scenario: 'search wiht limit'
    When I send a GET JSON request to "v1/search/users" with
      """
      {
        "q": "perry",
        "limit": 2
      }
      """
    Then the response status code will be "200"
    And the JSON response will have "2" "$..name" key
