Feature: unfollow user
  Background:
    Given there is user with a device uid "test_device" and a token "test_token"
    Given I have the following "user"s:
    | id        | name |
    | 42424242  | joe  |

  Scenario: 'missing device uid'
    Given I send a POST JSON request to "v1/unfollow/user/42424242" with
      """
        {}
      """
    Then the response status code will be "403"
    And the JSON response will have "$.error" with the value "Missing X-Towards-Device-Uuid"

  Scenario: 'invalid session token'
    Given I send a user authenticated POST JSON request with invalid session token to "v1/unfollow/user/42424242" with
      """
        {}
      """
    Then the response status code will be "403"
    And the JSON response will have "$.error" with the value "Invalid X-Towards-Session-Token"

  Scenario: 'properly authenticated request - unfollow non-existent user'
    Given I send a user authenticated POST JSON request to "v1/unfollow/user/333" with
      """
        {}
      """
    Then the response status code will be "404"

  Scenario: 'properly authenticated request - unfollow existing user'
    Given I send a user authenticated POST JSON request to "v1/unfollow/user/42424242" with
      """
        {}
      """
    Then the response status code will be "201"
    And the JSON response will have "$.success" with the value "true"
