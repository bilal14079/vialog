Feature: request opinion of user
  Background:
    Given there is user with a device uid "test_device" and a token "test_token"
    And I have the following "user"s:
    | id        |
    | 42424242  |
    And I have the following "topic"s:
    | id |
    | 23232323 |

  Scenario: 'missing device uid'
    Given I send a POST JSON request to "v1/users/42424242/request_opinion" with
      """
        {
          "topic_id": 23232323
        }
      """
    Then the response status code will be "403"
    And the JSON response will have "$.error" with the value "Missing X-Towards-Device-Uuid"

  Scenario: 'invalid session token'
    Given I send a user authenticated POST JSON request with invalid session token to "v1/users/42424242/request_opinion" with
      """
        {
          "topic_id": 23232323
        }
      """
    Then the response status code will be "403"
    And the JSON response will have "$.error" with the value "Invalid X-Towards-Session-Token"

  Scenario: 'properly authenticated request - non-existent user'
    Given I send a user authenticated POST JSON request to "v1/users/333/request_opinion" with
      """
        {
          "topic_id": 23232323
        }
      """
    Then the response status code will be "404"

  Scenario: 'properly authenticated request - non-existent topic'
    Given I send a user authenticated POST JSON request to "v1/users/42424242/request_opinion" with
      """
        {
          "topic_id": 444
        }
      """
    Then the response status code will be "404"

  Scenario: 'properly authenticated request'
    Given I send a user authenticated POST JSON request to "v1/users/42424242/request_opinion" with
      """
        {
          "topic_id": 23232323
        }
      """
    Then the response status code will be "201"
    And the JSON response will have "$.success" with the value "true"
