Feature: Swagger docs
  Scenario: 'swagger_doc.json is generated correctly'
    Given I send a GET JSON request to "v1/swagger_doc.json" with
      """
        {}
      """
    Then the response status code will be "200"
    And the JSON response will have "1" "$.info" key
    And the JSON response will have "1" "$.swagger" key
    And the JSON response will have "1" "$.paths" key
    And the JSON response will have "1" "$.definitions" key
