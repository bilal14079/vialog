# frozen_string_literal: true
step 'I have the following :factory_name(s):' do |factory_name, table|
  table.hashes.each do |attributes|
    create factory_name, attributes
  end
end

step 'I have the following :factory_name(s) for the current user:' do |factory_name, table|
  table.hashes.each do |attributes|
    create factory_name, attributes.merge(user: current_user)
  end
end

step 'I have a main topic dashboard with various topic modules' do
  create :topic_dashboard_with_modules, :main
end

step 'I have a topic dashboard with id :id with various topic modules' do |id|
  create :topic_dashboard_with_modules, id: id.to_i
end

step 'I have a topic with id :id with :opinions_count opinion(s)' do |id, opinion_count|
  create :topic_with_opinions, id: id.to_i, generated_opinions_count: opinion_count.to_i
end

step 'there is user with a device uid :device_uid and a token :token' do |device_uid, token|
  create :session_token, device_uid: device_uid,
                         token: token,
                         provider: SessionToken::PROVIDER_FACEBOOK,
                         challenge: token
end

step 'there is a current user with notifications' do
  user = create :user_with_notifications
  create :session_token, user: user
end

step 'I have a registered facebook user with name :name and with token :token' do |name, token|
  user = create :user, name: name, facebook_uid: '116640498794166'
  create :session_token, token: token, user: user
end

step 'I have a registered twitter user with name :name and with token :token' do |name, token|
  user = create :user, name: name, twitter_uid: '707475986'
  create :session_token, provider: SessionToken::PROVIDER_TWITTER, token: token, user: user
end
