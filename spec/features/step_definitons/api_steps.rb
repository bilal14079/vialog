# frozen_string_literal: true
step 'I send a :method JSON request to :path with valid twitter access token' do |method, path|
  data = { access_token: valid_twitter_access_token,
           access_token_secret: valid_twitter_access_token_secret,
           device_uid: twitter_session_token.device_uid }

  json_request method.downcase, "//#{ENV['API_SUBDOMAIN']}.towards.dev/#{path}", params: data
end

step 'I send a :method JSON request to :path with valid facebook access token' do |method, path|
  data = { fb_access_token: valid_fb_access_token,
           device_uid: current_session_token.device_uid }

  json_request method.downcase, "//#{ENV['API_SUBDOMAIN']}.towards.dev/#{path}", params: data
end

step 'I send a :method JSON request to :path with' do |method, path, data|
  json_request method.downcase, "//#{ENV['API_SUBDOMAIN']}.towards.dev/#{path}", params: JSON.parse(data)
end

step 'the response status code will be :status_code' do |status_code|
  expect(last_response.status).to eq(status_code.to_i)
end

step 'the response will have a :header_name header' do |header_name|
  expect(last_response.headers).to have_key(header_name)
end

step 'the response will have a :header_name header with value :value' do |header_name, value|
  expect(last_response.headers[header_name]).to eq(value)
end

step 'the JSON response will be' do |data|
  expected = JSON.parse data
  actual = last_response_json

  expect(actual).to eq(expected)
end

step 'the JSON response will have :node with the value :value' do |json_path, value|
  expect(json_values_at(json_path).map(&:to_s)).to include(value)
end

step 'the JSON response will not have :node with the value :value' do |json_path, value|
  expect(json_values_at(json_path).map(&:to_s)).not_to include(value)
end

step 'the JSON response will have :count :json_path key(s)' do |count, json_path|
  expect(json_values_at(json_path).count).to eq(count.to_i)
end

step 'the JSON response will contain some :json_path keys' do |json_path|
  expect(json_values_at(json_path).count).not_to eq(0)
end

step 'show me the JSON response' do
  puts pretty_response
end

step 'show me the response headers' do
  pp last_response.headers
end

step 'dump the JSON response to :filename' do |filename|
  return unless ENV['DUMP_API_RESPONSES']

  File.open("doc/api/#{filename}", 'w') { |f| f << pretty_response }
end

step 'I send a user authenticated :method JSON request to :path with' do |method, path, data|
  json_request method.downcase, "//#{ENV['API_SUBDOMAIN']}.towards.dev/#{path}",
               params: JSON.parse(data), headers: correct_user_auth_headers
end

step 'I send a user authenticated POST JSON request to :path with a video' do |path|
  json_request :post, "//#{ENV['API_SUBDOMAIN']}.towards.dev/#{path}",
               params: { video: fixture_file_upload('/files/test.mov', 'video/quicktime') },
               headers: correct_user_auth_headers
end

step 'I send a twitter user authenticated :method JSON request to :path with' do |method, path, data|
  json_request method.downcase, "//#{ENV['API_SUBDOMAIN']}.towards.dev/#{path}",
               params: JSON.parse(data), headers: twitter_user_auth_headers
end

step 'I send a user authenticated :method JSON request with invalid session token to :path with' do |method, path, data|
  json_request method.downcase,
               "//#{ENV['API_SUBDOMAIN']}.towards.dev/#{path}",
               params: JSON.parse(data),
               headers: user_auth_headers_with_invalid_session_token
end
