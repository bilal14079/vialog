Feature: Push notification registration
  Scenario: 'properly authenticated request - register for push notification'
    Given I send a user authenticated POST JSON request to "v1/push_notifications/register" with
      """
      {
        "push_notification_token": "Token of push notification",
        "notification_type": "Type of the notification"
      }
      """
    Then the response status code will be "201"
    And the JSON response will have "$.success" with the value "true"

  Scenario: 'not authenticated request - register for push notification'
    Given I send a POST JSON request to "v1/push_notifications/register" with
      """
      {
        "push_notification_token": "Token of push notification",
        "notification_type": "Type of the notification"
      }
      """
    Then the response status code will be "403"

  Scenario: 'properly authenticated request - deregister for push notification'
    Given I send a user authenticated POST JSON request to "v1/push_notifications/deregister" with
      """
      {
        "notification_type": "Type of the notification"
      }
      """
    Then the response status code will be "201"
    And the JSON response will have "$.success" with the value "true"

  Scenario: 'not authenticated request - deregister for push notification'
    Given I send a POST JSON request to "v1/push_notifications/deregister" with
      """
      {
        "notification_type": "Type of the notification"
      }
      """
    Then the response status code will be "403"

