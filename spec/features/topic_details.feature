Feature: Topic details
  Scenario: 'get non-existent topic'
    Given I send a user authenticated GET JSON request to "v1/topics/666" with
      """
        {}
      """
    Then the response status code will be "404"

  Scenario: 'existing topic'
    Given I have the following "topic":
    | id       |
    | 42424242 |
    And I send a user authenticated GET JSON request to "v1/topics/42424242" with
      """
        {}
      """
    Then the response status code will be "200"
    And the JSON response will have "$topic.id" with the value "42424242"
