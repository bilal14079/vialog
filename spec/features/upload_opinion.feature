Feature: follow topic
  Background:
    Given there is user with a device uid "test_device" and a token "test_token"
    Given I have the following "topic"s:
    | id        |
    | 42424242  |

  Scenario: 'unauthenticated request'
    Given I send a POST JSON request to "v1/topics/42424242/upload_opinion" with
      """
        {}
      """
    Then the response status code will be "403"

  Scenario: 'properly authenticated request - non-existent topic'
    Given I send a user authenticated POST JSON request to "v1/topics/333/upload_opinion" with a video
    Then the response status code will be "404"

  Scenario: 'properly authenticated request - existing topic'
    Given I send a user authenticated POST JSON request to "v1/topics/42424242/upload_opinion" with a video
    Then the response status code will be "201"
    And the JSON response will have "$.success" with the value "true"
