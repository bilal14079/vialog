Feature: user followed users
  Background:
    Given there is user with a device uid "test_device" and a token "test_token"

  Scenario: 'missing device uid'
    Given I send a GET JSON request to "v1/me/followed_users" with
      """
        {}
      """
    Then the response status code will be "403"
    And the JSON response will have "$.error" with the value "Missing X-Towards-Device-Uuid"

  Scenario: 'invalid session token'
    Given I send a user authenticated GET JSON request with invalid session token to "v1/me/followed_users" with
      """
        {}
      """
    Then the response status code will be "403"
    And the JSON response will have "$.error" with the value "Invalid X-Towards-Session-Token"

  Scenario: 'properly authenticated request'
    Given I send a user authenticated GET JSON request to "v1/me/followed_users" with
      """
        {}
      """
    Then the response status code will be "200"
    And the JSON response will have "1" "$.users" key
