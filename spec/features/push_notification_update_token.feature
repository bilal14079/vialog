Feature: Push notification token update
  Scenario: 'properly authenticated request - update token for push notifications'
    Given I send a user authenticated POST JSON request to "v1/push_notifications/update_token" with
      """
      {
        "push_notification_token": "Token of push notification"
      }
      """
    Then the response status code will be "201"
    And the JSON response will have "$.success" with the value "true"

  Scenario: 'not authenticated request - update token for push notifications'
    Given I send a POST JSON request to "v1/push_notifications/update_token" with
      """
      {
        "push_notification_token": "Token of push notification"
      }
      """
    Then the response status code will be "403"
