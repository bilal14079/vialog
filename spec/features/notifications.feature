Feature: List topic opinions
  Background:
    Given there is a current user with notifications

  Scenario: 'unauthenticated request'
    When I send a GET JSON request to "v1/me/notifications" with
      """
      {}
      """
    Then the response status code will be "403"

  Scenario: 'authenticated request'
    When I send a user authenticated GET JSON request to "v1/me/notifications" with
      """
      {}
      """
    Then the response status code will be "200"
    And the response will have a "X-Total" header
    And the response will have a "X-Total-Pages" header
    And the response will have a "X-Per-Page" header with value "20"
    And the response will have a "X-Page" header with value "1"
    And the response will have a "X-Next-Page" header
    And the response will have a "X-Prev-Page" header
    And the response will have a "X-Offset" header with value "0"
    And the JSON response will have "1" "$.notifications" key
