Feature: List topic opinions
  Background:
    Given I have a topic with id "42424242" with "2" opinions

  Scenario: 'non-existent topic'
    When I send a GET JSON request to "v1/topics/333/opinions" with
      """
      {}
      """
    Then the response status code will be "404"

  Scenario: 'existing topic - unauthenticated request'
    When I send a GET JSON request to "v1/topics/42424242/opinions" with
      """
      {}
      """
    Then the response status code will be "200"
    And the response will have a "X-Total" header
    And the response will have a "X-Total-Pages" header
    And the response will have a "X-Per-Page" header with value "20"
    And the response will have a "X-Page" header with value "1"
    And the response will have a "X-Next-Page" header
    And the response will have a "X-Prev-Page" header
    And the response will have a "X-Offset" header with value "0"
    And the JSON response will have "1" "$.opinions" key

  Scenario: 'existing topic - authenticated request'
    When I send a user authenticated GET JSON request to "v1/topics/42424242/opinions" with
      """
      {}
      """
    Then the response status code will be "200"
    And the response will have a "X-Total" header
    And the response will have a "X-Total-Pages" header
    And the response will have a "X-Per-Page" header with value "20"
    And the response will have a "X-Page" header with value "1"
    And the response will have a "X-Next-Page" header
    And the response will have a "X-Prev-Page" header
    And the response will have a "X-Offset" header with value "0"
    And the JSON response will have "1" "$.opinions" key

  Scenario: 'pagination'
    Given I have a topic with id "44444444" with "61" opinions
    When I send a GET JSON request to "v1/topics/44444444/opinions" with
      """
      {
        "page": 2,
        "per_page": 10
      }
      """
    And the response will have a "X-Total" header with value "61"
    And the response will have a "X-Total-Pages" header with value "7"
    And the response will have a "X-Per-Page" header with value "10"
    And the response will have a "X-Page" header with value "2"
    And the response will have a "X-Next-Page" header with value "3"
    And the response will have a "X-Prev-Page" header with value "1"
    And the response will have a "X-Offset" header with value "0"


