Feature: Main topic dashboard
  Background:
    Given I have a main topic dashboard with various topic modules

  Scenario: 'main dashboard - unathenticated request'
    When I send a GET JSON request to "v1/topic_dashboards/main" with
      """
      {}
      """
    Then the response status code will be "200"
    And the JSON response will have "$.main" with the value "true"
    And the JSON response will have "1" "$.topic_modules" key

  Scenario: 'main dashboard - athenticated request'
    When I send a user authenticated GET JSON request to "v1/topic_dashboards/main" with
      """
      {}
      """
    Then the response status code will be "200"
    And the JSON response will have "$.main" with the value "true"
    And the JSON response will have "1" "$.topics_followed_by_current_user" key
    And the JSON response will have "1" "$.topic_modules" key
