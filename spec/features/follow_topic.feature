Feature: follow topic
  Background:
    Given there is user with a device uid "test_device" and a token "test_token"
    Given I have the following "topic"s:
    | id        | title    |
    | 42424242  | hey jude |

  Scenario: 'missing device uid'
    Given I send a POST JSON request to "v1/follow/topic/42424242" with
      """
        {}
      """
    Then the response status code will be "403"
    And the JSON response will have "$.error" with the value "Missing X-Towards-Device-Uuid"

  Scenario: 'invalid session token'
    Given I send a user authenticated POST JSON request with invalid session token to "v1/follow/topic/42424242" with
      """
        {}
      """
    Then the response status code will be "403"
    And the JSON response will have "$.error" with the value "Invalid X-Towards-Session-Token"

  Scenario: 'properly authenticated request - follow non-existent topic'
    Given I send a user authenticated POST JSON request to "v1/follow/topic/333" with
      """
        {}
      """
    Then the response status code will be "404"

  Scenario: 'properly authenticated request - follow existing topic'
    Given I send a user authenticated POST JSON request to "v1/follow/topic/42424242" with
      """
        {}
      """
    Then the response status code will be "201"
    And the JSON response will have "$.success" with the value "true"
