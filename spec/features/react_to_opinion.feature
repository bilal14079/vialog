Feature: React to opinion
  Background:
    Given  I have the following "opinion":
    | id       |
    | 42424242 |

  Scenario: 'single reaction - unauthenticated request'
    When I send a POST JSON request to "v1/opinions/42424242/react" with
      """
        {
          "reaction_type": "love",
          "reaction_second": 21
        }
      """
    Then the response status code will be "403"

  Scenario: 'single reaction - properly authenticated request - react to a non-existent opinion'
    When I send a user authenticated POST JSON request to "v1/opinions/333/react" with
      """
        {
          "reaction_type": "love",
          "reaction_second": 22
        }
      """
    Then the response status code will be "404"

  Scenario: 'single reaction - properly authenticated request'
    When I send a user authenticated POST JSON request to "v1/opinions/42424242/react" with
      """
        {
          "reaction_type": "love",
          "reaction_second": 23
        }
      """
    Then the response status code will be "201"
    And the JSON response will have "$.success" with the value "true"

  Scenario: 'batch reaction - unauthenticated request'
    When I send a POST JSON request to "v1/opinions/42424242/react_in_batch" with
      """
        {
          "reactions": [
            {
              "reaction_type": "wow",
              "reaction_second": 22
            },
            {
              "reaction_type": "angry",
              "reaction_second": 44
            }
          ]
        }
      """
    Then the response status code will be "403"

  Scenario: 'batch reaction - properly authenticated request - react to a non-existent opinion'
    When I send a user authenticated POST JSON request to "v1/opinions/333/react_in_batch" with
      """
        {
          "reactions": [
            {
              "reaction_type": "wow",
              "reaction_second": 23
            },
            {
              "reaction_type": "angry",
              "reaction_second": 44
            }
          ]
        }
      """
    Then the response status code will be "404"

  Scenario: 'batch reaction - properly authenticated request'
    When I send a user authenticated POST JSON request to "v1/opinions/42424242/react_in_batch" with
      """
        {
          "reactions": [
            {
              "reaction_type": "wow",
              "reaction_second": 24
            },
            {
              "reaction_type": "angry",
              "reaction_second": 46
            }
          ]
        }
      """
    Then the response status code will be "201"
    And the JSON response will have "$.success" with the value "true"
