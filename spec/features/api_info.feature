Feature: API information
  Scenario: 'environment info'
    Given I send a GET JSON request to "v1/env" with
      """
        {}
      """
    Then the response status code will be "200"
    And the JSON response will have "$.version" with the value "v1"
    And the JSON response will have "$.environment" with the value "test"
    And the JSON response will not have "$.environment" with the value "development"
    And the JSON response will have "1" "$.version" key
    And the JSON response will be
      """
        {
          "version" : "v1",
          "environment":"test"
        }
      """

  Scenario: 'api parameter validation error handling'
    Given I send a GET JSON request to "v1/errors/invalid_http_params" with
      """
        {}
      """
    Then the response status code will be "400"

  Scenario: 'invalid resource error handling'
    Given I send a GET JSON request to "v1/errors/invalid_resource_params" with
      """
        {}
      """
    Then the response status code will be "422"

  Scenario: 'missing resource error handling'
    Given I send a GET JSON request to "v1/errors/missing_resource" with
      """
        {}
      """
    Then the response status code will be "404"

  Scenario: 'unexpected server error handling'
    Given I send a GET JSON request to "v1/errors/unexpected_server_error" with
      """
        {}
      """
    Then the response status code will be "500"

  Scenario: 'method not allowed error handling'
    Given I send a POST JSON request to "v1/errors/unexpected_server_error" with
      """
        {}
      """
    Then the response status code will be "405"
