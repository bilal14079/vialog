Feature: topics needing opinion - any user
  Background:
    Given there is user with a device uid "test_device" and a token "test_token"
    And I have the following "user_with_received_opinion_requests":
    | id       |
    | 42424242 |

  Scenario: 'non-existent user'
    Given I send a GET JSON request to "v1/users/333/topics_needing_opinion" with
      """
        {}
      """
    Then the response status code will be "404"

  Scenario: 'existing user - unauthenticated request'
    Given I send a GET JSON request to "v1/users/42424242/topics_needing_opinion" with
      """
        {}
      """
    Then the response status code will be "200"
    And the JSON response will have "1" "$.topics" key
    And the JSON response will contain some "$..profiled_user_opinion_requests_count" keys

  Scenario: 'existing user - authenticated request'
    Given I send a user authenticated GET JSON request to "v1/users/42424242/topics_needing_opinion" with
      """
        {}
      """
    Then the response status code will be "200"
    And the JSON response will have "1" "$.topics" key
    And the JSON response will contain some "$..profiled_user_opinion_requests_count" keys
    And the JSON response will contain some "$..current_user_requested_opinion_from_profiled_user" keys
