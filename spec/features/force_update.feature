Feature: Mobile app version info
  Scenario: 'platform parameter missing'
    Given I send a GET JSON request to "v1/mobile_app_version" with
      """
        {}
      """
    Then the response status code will be "400"
    And the JSON response will have "$.error" with the value "platform is missing, platform does not have a valid value"

  Scenario: 'platform parameter is invalid'
    Given I send a GET JSON request to "v1/mobile_app_version" with
      """
        {"platform": "not_a_valid_platform"}
      """
    Then the response status code will be "400"
    And the JSON response will have "$.error" with the value "platform does not have a valid value"

  Scenario: 'proper use of the endpoint'
    Given I have the following "mobile_app_version"s:
    | platform | update_severity | version_identifier |
    | ios      | soft            | 1.0.0              |
    | android  | soft            | 1.0.3              |
    | ios      | hard            | 1.1.0              |
    And I send a GET JSON request to "v1/mobile_app_version" with
      """
        {"platform": "ios"}
      """
    Then the response status code will be "200"
    And the JSON response will have "$.platform" with the value "ios"
    And the JSON response will have "$.update_type" with the value "hard"
    And the JSON response will have "$.last_version" with the value "1.1.0"

