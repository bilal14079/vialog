# frozen_string_literal: true
require 'rack/test'

module ApiHelper
  include Rack::Test::Methods

  def app
    Towards::Application
  end
end
