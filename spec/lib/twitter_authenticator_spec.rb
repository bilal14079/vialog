# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Twitter::Authenticator do
  let(:access_token) { 'twitter_access_token' }
  let(:access_token_secret) { 'twitter_access_token_secret' }
  let(:device_uid) { 'device_uid' }

  let(:subject) do
    Twitter::Authenticator.new(access_token: access_token,
                               access_token_secret: access_token_secret,
                               device_uid: device_uid)
  end

  describe '#authenticate' do
    context 'with wrong access_token' do
      it 'raise Twitter::AuthenticationError' do
        expect { subject.authenticate }.to raise_error(Twitter::AuthenticationError)
      end
    end

    context 'with good access_token' do
      let(:twitter_user_hash) do
        { id: '35710905',
          name: 'reden87',
          twitter_handle: 'reden87',
          profile_picture_url: 'https://twitter.com/reden87/profile_image?size=normal' }
      end

      before do
        allow(subject).to receive(:twitter_user).and_return(OpenStruct.new(twitter_user_hash))
      end

      let(:token) { subject.authenticate }
      let(:token_user) { token.user }

      it 'returns a SessionToken' do
        expect(token).to be_kind_of(SessionToken)
      end

      it 'returns a SessionToken with provider twitter' do
        expect(token.provider).to eq('twitter')
      end

      it 'does not return a SessionToken with provider facebook' do
        expect(token.provider).to_not eq('facebook')
      end

      it 'returns a SessionToken with a user' do
        expect(token.user).to be_kind_of(User)
      end

      it 'returns a SessionToken with a user with correct values' do
        expect(token_user.twitter_uid).to eq(twitter_user_hash[:id])
        expect(token_user.name).to eq(twitter_user_hash[:name])
        expect(token_user.twitter_handle).to eq(twitter_user_hash[:twitter_handle])
        expect(token_user.profile_picture_url).to eq(twitter_user_hash[:profile_picture_url])
      end

      it 'does not return a SessionToken with a user with wrong values' do
        expect(token_user.twitter_uid).to_not eq('wrong_id')
        expect(token_user.name).to_not eq('wrong_user_name')
        expect(token_user.twitter_handle).to_not eq('wrong_twitter_handle')
        expect(token_user.profile_picture_url).to_not eq('wrong_profile_picture_url')
      end

      context 'there is no exsisting user yet' do
        it 'creates a new user' do
          expect { subject.authenticate }.to change { User.count }.by(1)
        end

        it 'creates a new session token' do
          expect { subject.authenticate }.to change { SessionToken.count }.by(1)
        end
      end

      context 'there is an exsisting user' do
        let!(:user) { create :user, twitter_uid: twitter_user_hash[:id] }

        it 'doesn\'t create a new user' do
          expect { subject.authenticate }.not_to change { User.count }
        end

        it 'creates a new session token' do
          expect { subject.authenticate }.to change { SessionToken.count }.by(1)
        end

        context 'user already has a session token for the given device_uid' do
          let!(:session_token) { create :session_token, provider: 'twitter', device_uid: device_uid, user: user }

          it 'doesn\'t create a new session token' do
            expect { subject.authenticate }.not_to change { SessionToken.count }
          end
        end
      end
    end
  end
end
