# frozen_string_literal: true
require_relative '../../lib/facebook/user'

RSpec.describe Facebook::User do
  let(:access_token) { 'some_access_token' }
  let(:subject) { described_class.new(access_token) }

  describe '#friend_ids' do
    let(:limit) { nil }
    let(:friend_ids) { limit ? subject.friend_ids(limit) : subject.friend_ids }

    context 'access_token without user_friends permission' do
      let(:access_token) do
        'EAAJZAC8pTaMoBAOcjW5J715AJy5a7UrmJrvk0ckyTyY9JYZAuZCA91w0oZA4oYS0G5eGvBFElGKUYol' \
          'xEZCc3oCMW7muvrWJv2NsOgYfHeTtAOyja8hJUdSQKQz5s2qXX8KUZCGKZBSxiAzebMKirq7HhT2nPt2' \
          'hpnkWA0BojyIRQZDZD'
      end

      it 'returns an empty array' do
        expect(subject.friend_ids).to eq([])
      end
    end

    context 'access_token with user_friends permission and 9 friends' do
      let(:access_token) do
        'EAAJZAC8pTaMoBAFHKsrP9644b1ZC79rIsTonJpnxLD371INcg9SYhBL7Vy4Oy0FZATZCTXToJ9kcItK3' \
          'vQxJtufAyxJYQIj9ucy6K4ZAq47Njk7imvmnfMtWWxiRWyrzNCIqNUurItd2xPAKZCS5l11JWlHRu6vjx' \
          'aDAQHZB8qEnwZDZD'
      end

      context 'default limit' do
        it 'returns all 9 friend ids' do
          expect(friend_ids.size).to eq(9)
        end
      end

      context 'given limit' do
        let(:limit) { 2 }

        it 'returns at most :limit resulst' do
          expect(friend_ids.size).to eq(2)
        end
      end
    end
  end
end
