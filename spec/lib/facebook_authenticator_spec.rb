# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Facebook::Authenticator do
  let(:access_token) { 'fb_access_token' }
  let(:device_uid) { 'device_uid' }

  let(:subject) { Facebook::Authenticator.new(access_token: access_token, device_uid: device_uid) }

  describe '#authenticate' do
    context 'with wrong access_token' do
      it 'raise Facebook::AuthenticationError' do
        expect { subject.authenticate }.to raise_error(Facebook::AuthenticationError)
      end
    end

    context 'with good access_token' do
      let(:fb_user_hash) do
        { id: '1412114918802057',
          name: 'David Horvath',
          profile_picture_url: 'https://graph.facebook.com/1412114918802057/picture?width=500&height=500' }
      end

      before do
        allow(subject).to receive(:fb_user).and_return(OpenStruct.new(fb_user_hash))
      end

      let(:token) { subject.authenticate }
      let(:token_user) { token.user }

      it 'returns a SessionToken' do
        expect(token).to be_kind_of(SessionToken)
      end

      it 'returns a SessionToken with provider facebook' do
        expect(token.provider).to eq('facebook')
      end

      it 'does not return a SessionToken with provider twitter' do
        expect(token.provider).to_not eq('twitter')
      end

      it 'returns a SessionToken with a user' do
        expect(token.user).to be_kind_of(User)
      end

      it 'returns a SessionToken with a user with correct values' do
        expect(token_user.facebook_uid).to eq(fb_user_hash[:id])
        expect(token_user.name).to eq(fb_user_hash[:name])
        expect(token_user.profile_picture_url).to eq(fb_user_hash[:profile_picture_url])
      end

      it 'does not return a SessionToken with a user with wrong values' do
        expect(token_user.facebook_uid).to_not eq('wrong_id')
        expect(token_user.name).to_not eq('wrong_user_name')
        expect(token_user.profile_picture_url).to_not eq('wrong_profile_picture_url')
      end

      context 'there is no exsisting user yet' do
        it 'creates a new user' do
          expect { subject.authenticate }.to change { User.count }.by(1)
        end

        it 'creates a new session token' do
          expect { subject.authenticate }.to change { SessionToken.count }.by(1)
        end
      end

      context 'there is an exsisting user' do
        let!(:user) { create :user, facebook_uid: fb_user_hash[:id] }

        it 'doesn\'t create a new user' do
          expect { subject.authenticate }.not_to change { User.count }
        end

        it 'creates a new session token' do
          expect { subject.authenticate }.to change { SessionToken.count }.by(1)
        end

        context 'user already has a session token for the given device_uid' do
          let!(:session_token) { create :session_token, provider: 'facebook', device_uid: device_uid, user: user }

          it 'doesn\'t create a new session token' do
            expect { subject.authenticate }.not_to change { SessionToken.count }
          end

          it 'updates the already exsisting session token' do
            subject.authenticate

            expect(session_token.reload.challenge).to eq access_token
          end
        end
      end
    end
  end

  describe '#notify_friends' do
    let(:facebook_uid) { 2345 }
    let(:fb_user_hash) do
      { id: '1412114918802057',
        name: 'David Horvath',
        profile_picture_url: 'https://graph.facebook.com/1412114918802057/picture?width=500&height=500',
        friend_ids: [facebook_uid] }
    end
    let!(:fb_friend) { create(:user, facebook_uid: facebook_uid) }

    before do
      allow(subject).to receive(:fb_user).and_return(OpenStruct.new(fb_user_hash))
    end

    before do
      subject.authenticate
      subject.notify_friends
    end

    it 'notify facebook friends about joining' do
      expect(fb_friend.notifications.last.subject).to eq subject.authenticate.user
      expect(subject.authenticate.user.facebook_friend_joined_notifications.last.user).to eq fb_friend
    end
  end
end
