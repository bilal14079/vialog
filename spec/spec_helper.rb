# frozen_string_literal: true
require 'factory_girl'
require 'database_cleaner'
require_relative 'api_helper'
require 'vcr'

if ENV['COVERAGE']
  require 'simplecov'

  SimpleCov.minimum_coverage 80

  SimpleCov.profiles.define 'rails_wihtout_abstracts' do
    load_profile 'rails'
    add_filter 'channels/application_cable/channel.rb'
    add_filter 'channels/application_cable/connection.rb'

    add_filter 'app/jobs/application_job.rb'

    add_filter 'app/mailers/application_mailer.rb'

    add_group 'Admin', 'app/admin'
    add_group('V1 API') do |src|
      src.filename.include?('app/api/v1') && !src.filename.include?('app/api/v1/entities')
    end
    add_group 'V1 Entities', 'app/api/v1/entities'
  end

  SimpleCov.start 'rails_wihtout_abstracts'
end

if ENV['COVERAGE'] && ENV['CODECLIMATE_REPO_TOKEN']
  require 'codeclimate-test-reporter'
  CodeClimate::TestReporter.start
end

VCR.configure do |config|
  config.cassette_library_dir = 'spec/vcr'
  config.hook_into :webmock
end

RSpec.configure do |config|
  config.include ApiHelper
  config.include FactoryGirl::Syntax::Methods

  config.expect_with :rspec do |expectations|
    expectations.syntax = :expect
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  # Add VCR to all tests
  config.around(:each) do |example|
    options = example.metadata[:vcr] || {}
    if options[:record] == :skip
      VCR.turned_off(&example)
    else
      name = example.metadata[:example_group][:full_description]
      VCR.use_cassette(name, options, &example)
    end
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.filter_run_when_matching :focus

  config.example_status_persistence_file_path = 'spec/examples.txt'

  config.disable_monkey_patching!

  config.default_formatter = 'doc' if config.files_to_run.one?

  config.order = :random
  Kernel.srand config.seed
end
