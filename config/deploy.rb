# frozen_string_literal: true
# config valid only for current version of Capistrano

lock '3.6.0'

set :application,  'towards'
set :user,         'ubuntu'
set :repo_url,     'git@bitbucket.org:bilal14079/vialog.git'
set :puma_threads,    [4, 16]
set :puma_workers,    0
set :deploy_to,     "/home/#{fetch(:user)}/apps/#{fetch(:application)}/#{fetch(:stage)}"
set :keep_releases, 5
set :pty, true

# set :rvm_ruby_version, '2.3.1@towards'

set :puma_worker_timeout, 60
set :puma_init_active_record, true
set :puma_preload_app, true

# set :linked_files, fetch(:linked_files, []).push('.env',
#                                                  ".env.#{fetch(:stage)}",
#                                                  'config/database.yml',
#                                                  "config/certs/apn_#{fetch(:stage)}.pem")

set :linked_dirs, fetch(:linked_dirs, []).push('log',
                                               'tmp/pids',
                                               'tmp/cache',
                                               'tmp/sockets',
                                               'vendor/bundle',
                                               'public/static',
                                               'public/system')
