# frozen_string_literal: true
server '134.209.66.52', user: fetch(:user), roles: %w(web app db)

set :branch, 'development'

set :log_level, :debug

set :rails_env, 'production'
set :puma_env, 'production'

set :puma_bind,       "unix://#{shared_path}/tmp/sockets/puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"

set :nginx_application_name, 'towards_production'
set :nginx_domains, 'admin.vialog.cc api.vialog.cc v.vialog.cc sns.vialog.cc'
set :nginx_use_ssl, true
