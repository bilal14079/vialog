# frozen_string_literal: true
Rails.application.config.middleware.use Heroic::SNS::Endpoint,
                                        topics: proc { true },
                                        auto_unsubscribe: true
