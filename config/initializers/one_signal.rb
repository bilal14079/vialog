# frozen_string_literal: true
require 'one_signal'

OneSignal::OneSignal.api_key = ENV['ONE_SIGNAL_API_KEY']
