# frozen_string_literal: true
module Paperclip
  class HashieMashUploadedFileAdapter < AbstractAdapter
    def initialize(target)
      @tempfile = target.tempfile
      @content_type = target.type
      @size = target.tempfile.size
      self.original_filename = target.filename
    end
  end
end

Paperclip.io_adapters.register(Paperclip::HashieMashUploadedFileAdapter) do |target|
  target.is_a?(Hashie::Mash)
end
