# frozen_string_literal: true
Paperclip.interpolates :topic_id do |attachment, _style|
  attachment.instance.topic_id
end

Paperclip.interpolates :user_id do |attachment, _style|
  attachment.instance.user_id
end
