# frozen_string_literal: true
ActiveSupport::Notifications.subscribe('api_log_message') do |_name, _starts, _ends, _notification_id, payload|
  Rails.logger.tagged('GRAPE') { |logger| logger.info payload }
end
