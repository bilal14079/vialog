# frozen_string_literals: true
require 'twitter'

$twitter = Twitter::REST::Client.new do |config|
  config.consumer_key = ENV['TWITTER_KEY_2']
  config.consumer_secret = ENV['TWITTER_SECRET_2']
  config.access_token = ENV['TWITTER_ACCESS_TOKEN_4']
  config.access_token_secret = ENV['TWITTER_ACCESS_SECRET_4']
end