# frozen_string_literal: true
require 'apn'

APN.certificate_name = "apn_#{Rails.env}.pem" # certificate filename
APN.host = 'gateway.push.apple.com'
# APN.password = 'certificate_password'
# APN.pool_size = 1 # number of connections on the pool
# APN.pool_timeout = 5 # timeout in seconds for connection pool
APN.logger = Logger.new(File.join(Rails.root, 'log', "apn_#{Rails.env}.log"))
APN.backend = :sidekiq
