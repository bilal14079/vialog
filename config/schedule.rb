# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
every 5.minutes do
  rake 'opinions:update_watch_times'
end

every 1.hours do
  rake 'apns_feedback:invalidate_tokens'
end

every 6.hours do
  rake 'topics:generate_keywords'
end

every 1.day, at: '4am' do
  rake 'opinion_requests:fix_fullfill'
end

every 1.hours do
  rake 'intercom:update_attributes'
end