# frozen_string_literal: true
Rails.application.routes.draw do

  get '/robots.txt' => 'static#robots'

  get '/404', to: 'errors#not_found'

  constraints subdomain: ENV['ADMIN_SUBDOMAIN'] do
    ActiveAdmin.routes(self)
    devise_for :admins, ActiveAdmin::Devise.config
  end

  constraints subdomain: ENV['API_SUBDOMAIN'] do
    mount V1::API => '/'
    mount GrapeSwaggerRails::Engine => '/docs' unless Rails.env.production?

    # get '/apple-app-site-association', to: 'apple_app_site_associations#show'
  end

  constraints subdomain: ENV['SNS_SUBDOMAIN'] do
    post '/', to: 'sns#handle_notification'
  end

  constraints subdomain: ENV['VIDEO_SUBDOMAIN'] do
    # get '/apple-app-site-association', to: 'apple_app_site_associations#show'

    get '/:video' => 'video_player#show'
    get '/:video/:layoutType' => 'video_player#show'
  end

  constraints subdomain: ENV['INVITE_SUBDOMAIN'] do
    get '/apple-app-site-association', to: 'apple_app_site_associations#show'

    get '/:topic_id/:user_id' => 'invite#show'
  end

  constraints subdomain: ENV['ASK_SUBDOMAIN'] do
    get '/apple-app-site-association', to: 'apple_app_site_associations#show'

    get '/:video' => 'invite#show'
    get '/:video/:layoutType' => 'video_player#show'
  end

  constraints subdomain: ENV['SUPPORT_SUBDOMAIN'] do
    root 'static#support'
  end

  constraints subdomain: ENV['TERMS_SUBDOMAIN'] do
    root 'static#terms'
  end
end
