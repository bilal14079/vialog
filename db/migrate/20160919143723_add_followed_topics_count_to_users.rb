class AddFollowedTopicsCountToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :followed_topics_count, :integer, null: false, default: 0
  end
end
