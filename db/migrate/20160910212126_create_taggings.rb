class CreateTaggings < ActiveRecord::Migration[5.0]
  def change
    create_table :taggings do |t|
      t.integer :hashtag_id, null: false
      t.integer :taggable_id, null: false
      t.string :taggable_type, null: false

      t.index [:hashtag_id, :taggable_id, :taggable_type], unique: true

      t.timestamps
    end

    add_foreign_key :taggings, :hashtags, on_delete: :cascade
  end
end
