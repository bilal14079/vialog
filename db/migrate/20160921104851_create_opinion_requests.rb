class CreateOpinionRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :opinion_requests do |t|
      t.integer :requester_user_id, null: false, index: true
      t.integer :requested_user_id, null: false, index: true
      t.integer :topic_id, null: false, index: true

      t.integer :opinion_id, index: true

      t.boolean :fullfilled, default: false

      t.index [:requester_user_id, :requested_user_id, :topic_id],
              unique: true,
              where: '(fullfilled = false)',
              name: 'index_opinion_requests_unfullfilled_unique'

      t.timestamps
    end

    add_foreign_key :opinion_requests, :users, column: :requester_user_id, on_delete: :cascade
    add_foreign_key :opinion_requests, :users, column: :requested_user_id, on_delete: :cascade
    add_foreign_key :opinion_requests, :topics, on_delete: :cascade
    add_foreign_key :opinion_requests, :opinions, on_delete: :cascade
  end
end
