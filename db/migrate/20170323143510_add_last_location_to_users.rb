class AddLastLocationToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :last_lat, :decimal
    add_column :users, :last_lng, :decimal
  end
end
