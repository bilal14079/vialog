class CreateFollowings < ActiveRecord::Migration[5.0]
  def change
    create_table :followings do |t|
      t.integer :follower_id, null: false
      t.integer :followed_id, null: false
      t.string :followed_type, null: false

      t.index [:follower_id, :followed_id, :followed_type], unique: true, name: 'index_followings_unique'

      t.timestamps
    end

    add_foreign_key :followings, :users, column: :follower_id
  end
end
