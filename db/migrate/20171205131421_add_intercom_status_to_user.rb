class AddIntercomStatusToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :intercom_status, :string, default: User::INTERCOM_STATUS_UNREGISTERED
  end
end
