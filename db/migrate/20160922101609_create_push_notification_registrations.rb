class CreatePushNotificationRegistrations < ActiveRecord::Migration[5.0]
  def change
    create_table :push_notification_registrations do |t|
      t.integer :user_id, null: false, index: true

      t.string :push_notification_token, null: false
      t.string :notification_type, null: false, index: true
      t.string :device_uid, null: false

      t.timestamps
    end

    add_index :push_notification_registrations, [:user_id, :notification_type, :device_uid],
              unique: true, name: :push_notification_registrations_unique_index
    add_foreign_key :push_notification_registrations, :users, on_delete: :cascade
  end
end
