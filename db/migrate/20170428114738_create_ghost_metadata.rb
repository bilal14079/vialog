class CreateGhostMetadata < ActiveRecord::Migration[5.0]
  def change
    create_table :ghost_metadata do |t|
      t.references :creator_user, foreign_key: {to_table: :users}
      t.references :ghost_user, foreign_key: {to_table: :users}
      t.references :topic, foreign_key: true
      t.datetime :ask_time

      t.timestamps
    end
  end
end
