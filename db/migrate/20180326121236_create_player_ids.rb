class CreatePlayerIds < ActiveRecord::Migration[5.0]
  def change
    create_table :player_ids do |t|
      t.references :user, foreign_key: true
      t.string :device_uid
      t.string :player_id

      t.timestamps
    end
  end
end
