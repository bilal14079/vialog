class CreateSessionTokens < ActiveRecord::Migration[5.0]
  def change
    create_table :session_tokens do |t|
      t.string :provider, null: false
      t.string :challenge, null: false
      t.string :token, null: false, index: { unique: true }
      t.string :device_uid, null: false
      t.integer :user_id, null: false, index: true

      t.timestamps
    end

    add_index :session_tokens, [:provider, :challenge], unique: true
    add_foreign_key :session_tokens, :users, on_delete: :cascade
  end
end
