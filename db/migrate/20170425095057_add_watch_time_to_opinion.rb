class AddWatchTimeToOpinion < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :watch_time, :integer, null: false, default: 0
  end
end
