class AddCoverToOpinion < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :cover, :boolean, default: false
    Topic.find_each(&:refresh_thumbnail_url)
  end
end
