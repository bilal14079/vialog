class CreateTopicLocales < ActiveRecord::Migration[5.0]
  def change
    create_table :topic_locales do |t|
      t.string :name, null: false
      t.string :country_code
      t.decimal :latitude
      t.decimal :longitude

      t.timestamps
    end
  end
end
