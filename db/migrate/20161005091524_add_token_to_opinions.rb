class AddTokenToOpinions < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :token, :string, null: false, default: ''
  end
end
