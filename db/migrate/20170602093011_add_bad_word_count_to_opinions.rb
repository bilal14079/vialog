class AddBadWordCountToOpinions < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :bad_word_count, :integer, default: -1
  end
end
