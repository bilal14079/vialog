class AddThumbnailToBanners < ActiveRecord::Migration[5.0]
  def up
    add_attachment :banners, :thumbnail
  end

  def down
    remove_attachment :banners, :thumbnail
  end
end
