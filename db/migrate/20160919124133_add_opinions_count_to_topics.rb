class AddOpinionsCountToTopics < ActiveRecord::Migration
  def change
    add_column :topics, :opinions_count, :integer, null: false, default: 0
  end
end
