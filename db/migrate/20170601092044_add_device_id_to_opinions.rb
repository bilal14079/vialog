class AddDeviceIdToOpinions < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :device_id, :string
  end
end
