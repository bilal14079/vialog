class AddDurationToOpinions < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :duration, :decimal, precision: 6, scale: 2, default: 0
  end
end
