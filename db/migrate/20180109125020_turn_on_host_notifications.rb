class TurnOnHostNotifications < ActiveRecord::Migration[5.0]
  def up
    PushNotificationRegistration.select(:user_id).group(:user_id).each do |reg|
      PushNotificationRegistration.create(user_id: reg.user_id, notification_type: Notification::HOSTED_TOPIC_NEW_VIDEO)
    end
  end
end
