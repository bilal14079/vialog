class AddTitleColorsToTopicModule < ActiveRecord::Migration[5.0]
  def change
    add_column :topic_modules, :title_bg_color, :string
    add_column :topic_modules, :title_color, :string
  end
end
