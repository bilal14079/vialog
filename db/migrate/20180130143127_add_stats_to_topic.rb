class AddStatsToTopic < ActiveRecord::Migration[5.0]
  def change
    add_column :topics, :clap_count, :integer
    add_column :topics, :follower_count, :integer
    add_column :topics, :last_opinion_time, :datetime
  end
end
