class AddDefaultMarkToCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :default_mark, :boolean
  end
end
