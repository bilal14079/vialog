class CreateOpinionTranslations < ActiveRecord::Migration[5.0]
  def change
    create_table :opinion_translations do |t|
      t.references :opinion, foreign_key: true
      t.text :text
      t.string :language

      t.timestamps
    end
  end
end
