class DefineDistanceFunction < ActiveRecord::Migration[5.0]
  def up
    execute "CREATE OR REPLACE FUNCTION lat_long_distance(lat1 numeric, lng1 numeric, lat2 numeric, lng2 numeric) RETURNS numeric AS
$$
DECLARE
    R integer;
    dlat numeric;
    dlon numeric;
    a numeric;
    c numeric;
    km numeric;
BEGIN
    R := 6371;
    dlat := radians(lat2 - lat1);
    dlon := radians(lng2 - lng1);
    a := sin(dlat/2) * sin(dlat/2) + cos(radians(lat1)) * cos(radians(lat2)) * sin(dlon/2) * sin(dlon/2);
    c := 2 * atan2(sqrt(a), sqrt(1 - a));
    km := R * c;
    RETURN km;
END;
$$
LANGUAGE 'plpgsql' IMMUTABLE;"
  end

  def down
    execute 'DROP FUNCTION IF EXISTS lat_long_distance(numeric, numeric, numeric, numeric)'
  end
end
