class AddExtrasToLoginSlide < ActiveRecord::Migration[5.0]
  def change
    add_column :login_slides, :animated, :boolean
    add_column :login_slides, :looped, :boolean
    add_column :login_slides, :display_order, :integer
  end
end
