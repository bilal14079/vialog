class CreateTopicOpens < ActiveRecord::Migration[5.0]
  def change
    create_table :topic_opens do |t|
      t.references :topic, foreign_key: true
      t.references :user, foreign_key: true
      t.datetime :time

      t.timestamps
    end
  end
end
