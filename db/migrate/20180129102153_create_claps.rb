class CreateClaps < ActiveRecord::Migration[5.0]
  def change
    create_table :claps do |t|
      t.references :opinion, foreign_key: true
      t.references :user, foreign_key: true
      t.integer :start
      t.integer :duration

      t.timestamps
    end
  end
end
