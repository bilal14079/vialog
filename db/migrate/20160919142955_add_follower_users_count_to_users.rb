class AddFollowerUsersCountToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :follower_users_count, :integer, null: false, default: 0
  end
end
