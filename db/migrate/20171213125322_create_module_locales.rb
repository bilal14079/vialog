class CreateModuleLocales < ActiveRecord::Migration[5.0]
  def change
    create_table :module_locales do |t|
      t.references :topic_module, foreign_key: true
      t.references :topic_locale, foreign_key: true

      t.timestamps
    end
  end
end
