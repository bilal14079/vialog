class AddLastCountryToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :last_country, :string
  end
end
