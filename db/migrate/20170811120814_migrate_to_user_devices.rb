class MigrateToUserDevices < ActiveRecord::Migration[5.0]
  def change
    PushNotificationRegistration.select(:user_id, :push_notification_token, :device_uid).group(:user_id, :push_notification_token, :device_uid).each do |t|
      UserDevice.create(user_id: t.user_id, device_uid: t.device_uid, push_notification_token: t.push_notification_token)
    end
  end
end
