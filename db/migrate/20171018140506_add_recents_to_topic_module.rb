class AddRecentsToTopicModule < ActiveRecord::Migration[5.0]
  def change
    add_column :topic_modules, :recents, :boolean
  end
end
