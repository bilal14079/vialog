class CreateTopicModules < ActiveRecord::Migration[5.0]
  def change
    create_table :topic_modules do |t|
      t.string :type, null: false

      t.integer :topic_dashboard_id, null: false, index: true
      t.integer :position, null: false

      t.string :title, null: false
      t.string :display_type, null: false

      t.integer :category_id, index: { where: 'category_id IS NOT NULL' }
      t.integer :banner_id, index: { where: 'banner_id IS NOT NULL' }

      t.timestamps
    end

    add_foreign_key :topic_modules, :topic_dashboards, on_delete: :cascade
    add_foreign_key :topic_modules, :categories, on_delete: :cascade
    add_foreign_key :topic_modules, :banners, on_delete: :cascade
  end
end
