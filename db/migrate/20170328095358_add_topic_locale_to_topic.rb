class AddTopicLocaleToTopic < ActiveRecord::Migration[5.0]
  def change
    add_reference :topics, :topic_locale, foreign_key: true
  end
end
