class AddQualityToOpinion < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :quality, :string, default: 'hd'
  end
end
