class CreateLoginSlides < ActiveRecord::Migration[5.0]
  def change
    create_table :login_slides do |t|
      t.attachment :image
      t.text :text
      t.string :dot_color

      t.timestamps
    end
  end
end
