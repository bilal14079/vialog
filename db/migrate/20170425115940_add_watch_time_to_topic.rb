class AddWatchTimeToTopic < ActiveRecord::Migration[5.0]
  def change
    add_column :topics, :watch_time, :integer, null: false, default: 0
  end
end
