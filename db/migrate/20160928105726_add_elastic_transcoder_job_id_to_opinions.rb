class AddElasticTranscoderJobIdToOpinions < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :elastic_transcoder_job_id, :string
    add_index :opinions, :elastic_transcoder_job_id
  end
end
