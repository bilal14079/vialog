class AddStatsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :seen_time, :integer
    add_column :users, :hosted_issues_count, :integer
    add_column :users, :open_asks_count, :integer
    add_column :users, :responded_asks_count, :integer
  end
end
