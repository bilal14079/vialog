class CreateMobileAppVersions < ActiveRecord::Migration[5.0]
  def change
    create_table :mobile_app_versions do |t|
      t.string :platform, null: false
      t.string :update_severity, null: false
      t.string :version_identifier, null: false

      t.index [:platform, :version_identifier], unique: true

      t.timestamps
    end
  end
end
