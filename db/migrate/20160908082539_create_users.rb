class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name, null: false, index: true
      t.string :profile_picture_url, null: false

      t.string :facebook_uid, index: { unique: true, where: 'facebook_uid IS NOT NULL' }
      t.string :twitter_uid, index: { unique: true, where: 'twitter_uid IS NOT NULL' }
      t.string :twitter_handle

      t.timestamps
    end
  end
end
