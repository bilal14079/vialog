class AddRadiusToTopicLocale < ActiveRecord::Migration[5.0]
  def change
    add_column :topic_locales, :radius, :decimal
  end
end
