class CreateOpinions < ActiveRecord::Migration[5.0]
  def change
    create_table :opinions do |t|
      t.integer :topic_id, null: false, index: true
      t.integer :user_id, null: false, index: true

      t.integer :feature_factor, null: false, default: 0

      t.string :video_hls_url
      t.string :video_thumbnail_url

      t.timestamps
    end

    add_foreign_key :opinions, :topics, on_delete: :cascade
    add_foreign_key :opinions, :users, on_delete: :cascade
  end
end
