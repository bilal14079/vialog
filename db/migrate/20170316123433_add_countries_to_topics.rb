class AddCountriesToTopics < ActiveRecord::Migration[5.0]
  def change
    add_column :topics, :countries, :string
  end
end
