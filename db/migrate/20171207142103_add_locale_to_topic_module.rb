class AddLocaleToTopicModule < ActiveRecord::Migration[5.0]
  def change
    add_reference :topic_modules, :topic_locale, foreign_key: true
  end
end
