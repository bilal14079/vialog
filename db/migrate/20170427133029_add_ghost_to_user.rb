class AddGhostToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :ghost, :boolean, default: false
  end
end
