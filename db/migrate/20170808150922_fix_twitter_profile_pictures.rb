class FixTwitterProfilePictures < ActiveRecord::Migration[5.0]
  def change
    User.where.not(twitter_handle: nil).find_each { |u| u.update(profile_picture_url: "https://twitter.com/#{u.twitter_handle}/profile_image?size=original")}
  end
end
