class AddVideoToAttachments < ActiveRecord::Migration[5.0]
  def up
    add_attachment :opinions, :video
  end

  def down
    remove_attachment :opinions, :video
  end
end
