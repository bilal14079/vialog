class AddTotalReactionCountToOpinions < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :total_reaction_count, :integer, null: false, default: 0
  end
end
