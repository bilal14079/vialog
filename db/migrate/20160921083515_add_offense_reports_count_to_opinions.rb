class AddOffenseReportsCountToOpinions < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :offense_reports_count, :integer, null: false, default: 0
  end
end
