class ChangeChallengeForSessionTokens < ActiveRecord::Migration[5.0]
  def change
    change_column :session_tokens, :challenge, :string, null: true
  end
end
