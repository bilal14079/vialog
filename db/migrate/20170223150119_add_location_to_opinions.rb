class AddLocationToOpinions < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :lat, :decimal
    add_column :opinions, :lng, :decimal
  end
end
