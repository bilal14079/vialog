class AddLockedDaysToOpinions < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :locked_days, :integer, null: false, default: -1
  end
end
