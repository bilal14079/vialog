class AddProcessedToOpinions < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :processed, :boolean, default: false
    add_index :opinions, :processed
  end
end
