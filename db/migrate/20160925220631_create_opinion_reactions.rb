class CreateOpinionReactions < ActiveRecord::Migration[5.0]
  def change
    create_table :opinion_reactions do |t|
      t.integer :opinion_id, null: false, index: true
      t.integer :user_id, null: false, index: true

      t.string :reaction_type, null: false
      t.integer :reaction_second, null: false

      t.index [:opinion_id, :user_id, :reaction_type, :reaction_second], unique: true, name: 'index_reactions_uniq'

      t.timestamps
    end

    add_foreign_key :opinion_reactions, :opinions, on_delete: :cascade
    add_foreign_key :opinion_reactions, :users, on_delete: :cascade
  end
end
