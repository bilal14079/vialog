class AddBadWordCountToOpinionTranslations < ActiveRecord::Migration[5.0]
  def change
    add_column :opinion_translations, :bad_word_count, :integer
  end
end
