class CreateTopics < ActiveRecord::Migration[5.0]
  def change
    create_table :topics do |t|
      t.string :title, null: false, index: { unique: true }
      t.integer :category_id, index: { where: 'category_id IS NOT NULL' }

      t.timestamps
    end

    add_foreign_key :topics, :categories, on_delete: :cascade
  end
end
