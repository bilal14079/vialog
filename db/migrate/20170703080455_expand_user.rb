class ExpandUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :bio, :text, limit: 140
    add_reference :users, :profile_video, foreign_key: { to_table: :opinions }
  end
end
