class AddTopicLimitToTopicModule < ActiveRecord::Migration[5.0]
  def change
    add_column :topic_modules, :topic_limit, :integer, default: 5
  end
end
