class CreateBadWords < ActiveRecord::Migration[5.0]
  def change
    create_table :bad_words do |t|
      t.string :language
      t.string :word

      t.timestamps
    end
  end
end
