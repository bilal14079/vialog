class CreateActionItems < ActiveRecord::Migration[5.0]
  def change
    create_table :action_items do |t|
      t.references :actor, foreign_key: { to_table: :users }, null: false
      t.integer :target_id, null: false
      t.string :target_type, null: false
      t.string :action_type, null: false

      t.timestamps
    end
  end
end
