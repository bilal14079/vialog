class AddProfanityToBadWord < ActiveRecord::Migration[5.0]
  def change
    add_column :bad_words, :profanity, :boolean, default: true
  end
end
