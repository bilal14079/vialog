class CreateTopicDashboards < ActiveRecord::Migration[5.0]
  def change
    create_table :topic_dashboards do |t|
      t.boolean :main, default: false, index: { unique: true, where: 'main = true' }

      t.timestamps
    end
  end
end
