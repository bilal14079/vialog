class AddProfileToTopic < ActiveRecord::Migration[5.0]
  def change
    add_column :topics, :profile_topic, :boolean, default: false
  end
end
