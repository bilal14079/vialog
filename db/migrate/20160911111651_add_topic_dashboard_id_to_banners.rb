class AddTopicDashboardIdToBanners < ActiveRecord::Migration[5.0]
  def change
    add_column :banners, :topic_dashboard_id, :integer, index: { where: 'topic_dashboard_id IS NOT NULL' }
    add_foreign_key :banners, :topic_dashboards, on_delete: :cascade
  end
end
