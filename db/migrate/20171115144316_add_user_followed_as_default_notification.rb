class AddUserFollowedAsDefaultNotification < ActiveRecord::Migration[5.0]
  def change
    users = PushNotificationRegistration.where.not(notification_type: Notification::USER_FOLLOWED).select(:user_id).group(:user_id).pluck(:user_id)
    users.each do |user_id|
      PushNotificationRegistration.create(user_id: user_id, notification_type: Notification::USER_FOLLOWED)
    end
  end
end
