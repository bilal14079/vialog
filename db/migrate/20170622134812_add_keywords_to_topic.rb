class AddKeywordsToTopic < ActiveRecord::Migration[5.0]
  def change
    add_column :topics, :keywords, :string
  end
end
