class AddDefaultSettings < ActiveRecord::Migration[5.0]
  def change
    User.find_each do |user|
      setting = user.user_settings.find_by(key: 'issue_tracking')

      user.user_settings.create(key: 'issue_tracking', value: 'true') unless setting
    end
  end
end
