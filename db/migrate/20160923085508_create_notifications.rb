class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.string :type, null: false
      t.string :notification_type, null: false
      t.integer :user_id, null: false, index: true
      t.integer :subject_id, null: false
      t.string :subject_type, null: false

      t.timestamps
    end

    add_foreign_key :notifications, :users, on_delete: :cascade
  end
end
