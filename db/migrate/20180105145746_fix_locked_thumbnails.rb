class FixLockedThumbnails < ActiveRecord::Migration[5.0]
  def change
    Topic.find_each(&:refresh_thumbnail_url)
  end
end
