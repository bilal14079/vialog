class AddReactionCountsToOpinions < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :reaction_love_count, :integer, null: false, default: 0
    add_column :opinions, :reaction_haha_count, :integer, null: false, default: 0
    add_column :opinions, :reaction_wow_count, :integer, null: false, default: 0
    add_column :opinions, :reaction_sad_count, :integer, null: false, default: 0
    add_column :opinions, :reaction_angry_count, :integer, null: false, default: 0
  end
end
