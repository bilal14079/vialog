class CreateUserDevices < ActiveRecord::Migration[5.0]
  def change
    create_table :user_devices do |t|
      t.references :user, foreign_key: true
      t.string :device_uid
      t.string :push_notification_token

      t.timestamps
    end
  end
end
