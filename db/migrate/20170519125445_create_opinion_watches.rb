class CreateOpinionWatches < ActiveRecord::Migration[5.0]
  def change
    create_table :opinion_watches do |t|
      t.references :user, foreign_key: true, null: false
      t.references :opinion, foreign_key: true, null: false

      t.timestamps
    end
  end
end
