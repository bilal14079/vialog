class AddThumbnailUrlToTopics < ActiveRecord::Migration[5.0]
  def change
    add_column :topics, :thumbnail_url, :string
  end
end
