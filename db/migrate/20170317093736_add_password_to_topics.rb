class AddPasswordToTopics < ActiveRecord::Migration[5.0]
  def change
    add_column :topics, :password, :string
  end
end
