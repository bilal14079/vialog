class RemoveTopicLocaleFromTopicModule < ActiveRecord::Migration[5.0]
  def change
    remove_column :topic_modules, :topic_locale_id, :reference
  end
end
