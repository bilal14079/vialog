class AddTranscodingRoundToOpinion < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :transcoding_round, :int, default: 0
  end
end
