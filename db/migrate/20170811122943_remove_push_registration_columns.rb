class RemovePushRegistrationColumns < ActiveRecord::Migration[5.0]
  def change
    remove_column :push_notification_registrations, :push_notification_token
    remove_column :push_notification_registrations, :device_uid

    ids = PushNotificationRegistration.select('min(id) as id').group(:user_id, :notification_type).collect(&:id)
    PushNotificationRegistration.where.not(id: ids).destroy_all
  end
end
