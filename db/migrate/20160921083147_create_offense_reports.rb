class CreateOffenseReports < ActiveRecord::Migration[5.0]
  def change
    create_table :offense_reports do |t|
      t.string :reason, null: false

      t.integer :opinion_id, null: false, index: true
      t.integer :user_id, null: false, index: true

      t.timestamps
    end

    add_index :offense_reports, [:opinion_id, :user_id, :reason], unique: true
    add_foreign_key :offense_reports, :opinions, on_delete: :cascade
    add_foreign_key :offense_reports, :users, on_delete: :cascade
  end
end
