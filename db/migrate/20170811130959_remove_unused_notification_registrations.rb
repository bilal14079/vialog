class RemoveUnusedNotificationRegistrations < ActiveRecord::Migration[5.0]
  def change
    PushNotificationRegistration.where(notification_type: [Notification::REACTION_RECEIVED, Notification::FOLLOWED_OPINION_UPLOADED, Notification::FOLLOWED_OPINION_TOPIC_UPLOADED]).destroy_all
  end
end
