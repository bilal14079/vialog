class RenameBadWordToWord < ActiveRecord::Migration[5.0]
  def change
    rename_table :bad_words, :words
  end
end
