class AddLockToOpinions < ActiveRecord::Migration[5.0]
  def change
    add_column :opinions, :locked_at, :datetime
  end
end
