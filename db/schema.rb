# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180326121236) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "unaccent"

  create_table "action_items", force: :cascade do |t|
    t.integer  "actor_id",    null: false
    t.integer  "target_id",   null: false
    t.string   "target_type", null: false
    t.string   "action_type", null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["actor_id"], name: "index_action_items_on_actor_id", using: :btree
  end

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.string   "author_type"
    t.integer  "author_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree
  end

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  end

  create_table "banners", force: :cascade do |t|
    t.string   "title",                  null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "topic_dashboard_id"
    t.string   "thumbnail_file_name"
    t.string   "thumbnail_content_type"
    t.integer  "thumbnail_file_size"
    t.datetime "thumbnail_updated_at"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name",         null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.boolean  "default_mark"
    t.index ["name"], name: "index_categories_on_name", unique: true, using: :btree
  end

  create_table "claps", force: :cascade do |t|
    t.integer  "opinion_id"
    t.integer  "user_id"
    t.integer  "start"
    t.integer  "duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["opinion_id"], name: "index_claps_on_opinion_id", using: :btree
    t.index ["user_id"], name: "index_claps_on_user_id", using: :btree
  end

  create_table "followings", force: :cascade do |t|
    t.integer  "follower_id",   null: false
    t.integer  "followed_id",   null: false
    t.string   "followed_type", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["follower_id", "followed_id", "followed_type"], name: "index_followings_unique", unique: true, using: :btree
  end

  create_table "ghost_metadata", force: :cascade do |t|
    t.integer  "creator_user_id"
    t.integer  "ghost_user_id"
    t.integer  "topic_id"
    t.datetime "ask_time"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["creator_user_id"], name: "index_ghost_metadata_on_creator_user_id", using: :btree
    t.index ["ghost_user_id"], name: "index_ghost_metadata_on_ghost_user_id", using: :btree
    t.index ["topic_id"], name: "index_ghost_metadata_on_topic_id", using: :btree
  end

  create_table "hashtags", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_hashtags_on_name", unique: true, using: :btree
  end

  create_table "login_slides", force: :cascade do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.text     "text"
    t.string   "dot_color"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.boolean  "animated"
    t.boolean  "looped"
    t.integer  "display_order"
  end

  create_table "mobile_app_versions", force: :cascade do |t|
    t.string   "platform",           null: false
    t.string   "update_severity",    null: false
    t.string   "version_identifier", null: false
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["platform", "version_identifier"], name: "index_mobile_app_versions_on_platform_and_version_identifier", unique: true, using: :btree
  end

  create_table "module_locales", force: :cascade do |t|
    t.integer  "topic_module_id"
    t.integer  "topic_locale_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["topic_locale_id"], name: "index_module_locales_on_topic_locale_id", using: :btree
    t.index ["topic_module_id"], name: "index_module_locales_on_topic_module_id", using: :btree
  end

  create_table "notifications", force: :cascade do |t|
    t.string   "type",                              null: false
    t.string   "notification_type",                 null: false
    t.integer  "user_id",                           null: false
    t.integer  "subject_id",                        null: false
    t.string   "subject_type",                      null: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.boolean  "read",              default: false, null: false
    t.index ["user_id"], name: "index_notifications_on_user_id", using: :btree
  end

  create_table "offense_reports", force: :cascade do |t|
    t.string   "reason",     null: false
    t.integer  "opinion_id", null: false
    t.integer  "user_id",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["opinion_id", "user_id", "reason"], name: "index_offense_reports_on_opinion_id_and_user_id_and_reason", unique: true, using: :btree
    t.index ["opinion_id"], name: "index_offense_reports_on_opinion_id", using: :btree
    t.index ["user_id"], name: "index_offense_reports_on_user_id", using: :btree
  end

  create_table "opinion_reactions", force: :cascade do |t|
    t.integer  "opinion_id",      null: false
    t.integer  "user_id",         null: false
    t.string   "reaction_type",   null: false
    t.integer  "reaction_second", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["opinion_id", "user_id", "reaction_type", "reaction_second"], name: "index_reactions_uniq", unique: true, using: :btree
    t.index ["opinion_id"], name: "index_opinion_reactions_on_opinion_id", using: :btree
    t.index ["user_id"], name: "index_opinion_reactions_on_user_id", using: :btree
  end

  create_table "opinion_requests", force: :cascade do |t|
    t.integer  "requester_user_id",                 null: false
    t.integer  "requested_user_id",                 null: false
    t.integer  "topic_id",                          null: false
    t.integer  "opinion_id"
    t.boolean  "fullfilled",        default: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.index ["opinion_id"], name: "index_opinion_requests_on_opinion_id", using: :btree
    t.index ["requested_user_id"], name: "index_opinion_requests_on_requested_user_id", using: :btree
    t.index ["requester_user_id", "requested_user_id", "topic_id"], name: "index_opinion_requests_unfullfilled_unique", unique: true, where: "(fullfilled = false)", using: :btree
    t.index ["requester_user_id"], name: "index_opinion_requests_on_requester_user_id", using: :btree
    t.index ["topic_id"], name: "index_opinion_requests_on_topic_id", using: :btree
  end

  create_table "opinion_translations", force: :cascade do |t|
    t.integer  "opinion_id"
    t.text     "text"
    t.string   "language"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "bad_word_count"
    t.index ["opinion_id"], name: "index_opinion_translations_on_opinion_id", using: :btree
  end

  create_table "opinion_watches", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.integer  "opinion_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["opinion_id"], name: "index_opinion_watches_on_opinion_id", using: :btree
    t.index ["user_id"], name: "index_opinion_watches_on_user_id", using: :btree
  end

  create_table "opinions", force: :cascade do |t|
    t.integer  "topic_id",                                                          null: false
    t.integer  "user_id",                                                           null: false
    t.integer  "feature_factor",                                    default: 0,     null: false
    t.string   "video_hls_url"
    t.string   "video_thumbnail_url"
    t.datetime "created_at",                                                        null: false
    t.datetime "updated_at",                                                        null: false
    t.integer  "offense_reports_count",                             default: 0,     null: false
    t.integer  "reaction_love_count",                               default: 0,     null: false
    t.integer  "reaction_haha_count",                               default: 0,     null: false
    t.integer  "reaction_wow_count",                                default: 0,     null: false
    t.integer  "reaction_sad_count",                                default: 0,     null: false
    t.integer  "reaction_angry_count",                              default: 0,     null: false
    t.string   "video_file_name"
    t.string   "video_content_type"
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
    t.string   "elastic_transcoder_job_id"
    t.boolean  "processed",                                         default: false
    t.string   "token",                                             default: "",    null: false
    t.decimal  "duration",                  precision: 6, scale: 2, default: "0.0"
    t.decimal  "lat"
    t.decimal  "lng"
    t.datetime "locked_at"
    t.integer  "locked_days",                                       default: -1,    null: false
    t.integer  "total_reaction_count",                              default: 0,     null: false
    t.integer  "watch_time",                                        default: 0,     null: false
    t.string   "device_id"
    t.integer  "bad_word_count",                                    default: -1
    t.string   "quality",                                           default: "hd"
    t.integer  "transcoding_round",                                 default: 0
    t.integer  "clap_count"
    t.boolean  "cover",                                             default: false
    t.index ["elastic_transcoder_job_id"], name: "index_opinions_on_elastic_transcoder_job_id", using: :btree
    t.index ["processed"], name: "index_opinions_on_processed", using: :btree
    t.index ["topic_id"], name: "index_opinions_on_topic_id", using: :btree
    t.index ["user_id"], name: "index_opinions_on_user_id", using: :btree
  end

  create_table "player_ids", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "device_uid"
    t.string   "player_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_player_ids_on_user_id", using: :btree
  end

  create_table "push_notification_registrations", force: :cascade do |t|
    t.integer  "user_id",           null: false
    t.string   "notification_type", null: false
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["notification_type"], name: "index_push_notification_registrations_on_notification_type", using: :btree
    t.index ["user_id"], name: "index_push_notification_registrations_on_user_id", using: :btree
  end

  create_table "session_tokens", force: :cascade do |t|
    t.string   "provider",   null: false
    t.string   "challenge"
    t.string   "token",      null: false
    t.string   "device_uid", null: false
    t.integer  "user_id",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["provider", "challenge"], name: "index_session_tokens_on_provider_and_challenge", unique: true, using: :btree
    t.index ["token"], name: "index_session_tokens_on_token", unique: true, using: :btree
    t.index ["user_id"], name: "index_session_tokens_on_user_id", using: :btree
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "hashtag_id",    null: false
    t.integer  "taggable_id",   null: false
    t.string   "taggable_type", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["hashtag_id", "taggable_id", "taggable_type"], name: "index_taggings_on_hashtag_id_and_taggable_id_and_taggable_type", unique: true, using: :btree
  end

  create_table "topic_dashboards", force: :cascade do |t|
    t.boolean  "main",       default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["main"], name: "index_topic_dashboards_on_main", unique: true, where: "(main = true)", using: :btree
  end

  create_table "topic_locales", force: :cascade do |t|
    t.string   "name",         null: false
    t.string   "country_code"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.decimal  "radius"
  end

  create_table "topic_modules", force: :cascade do |t|
    t.string   "type",                           null: false
    t.integer  "topic_dashboard_id",             null: false
    t.integer  "position",                       null: false
    t.string   "title",                          null: false
    t.string   "display_type",                   null: false
    t.integer  "category_id"
    t.integer  "banner_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "title_bg_color"
    t.string   "title_color"
    t.boolean  "recents"
    t.integer  "topic_limit",        default: 5
    t.index ["banner_id"], name: "index_topic_modules_on_banner_id", where: "(banner_id IS NOT NULL)", using: :btree
    t.index ["category_id"], name: "index_topic_modules_on_category_id", where: "(category_id IS NOT NULL)", using: :btree
    t.index ["topic_dashboard_id"], name: "index_topic_modules_on_topic_dashboard_id", using: :btree
  end

  create_table "topic_opens", force: :cascade do |t|
    t.integer  "topic_id"
    t.integer  "user_id"
    t.datetime "time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["topic_id"], name: "index_topic_opens_on_topic_id", using: :btree
    t.index ["user_id"], name: "index_topic_opens_on_user_id", using: :btree
  end

  create_table "topics", force: :cascade do |t|
    t.string   "title",                                null: false
    t.integer  "category_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "opinions_count",       default: 0,     null: false
    t.string   "thumbnail_url"
    t.string   "countries"
    t.string   "password"
    t.integer  "total_reaction_count", default: 0,     null: false
    t.integer  "topic_locale_id"
    t.integer  "watch_time",           default: 0,     null: false
    t.string   "keywords"
    t.boolean  "profile_topic",        default: false
    t.integer  "creator_id"
    t.integer  "clap_count"
    t.integer  "follower_count"
    t.datetime "last_opinion_time"
    t.index ["category_id"], name: "index_topics_on_category_id", where: "(category_id IS NOT NULL)", using: :btree
    t.index ["creator_id"], name: "index_topics_on_creator_id", using: :btree
    t.index ["title"], name: "index_topics_on_title", unique: true, using: :btree
    t.index ["topic_locale_id"], name: "index_topics_on_topic_locale_id", using: :btree
  end

  create_table "user_devices", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "device_uid"
    t.string   "push_notification_token"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["user_id"], name: "index_user_devices_on_user_id", using: :btree
  end

  create_table "user_settings", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "key"
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_settings_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",                                           null: false
    t.string   "profile_picture_url",                            null: false
    t.string   "facebook_uid"
    t.string   "twitter_uid"
    t.string   "twitter_handle"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.integer  "opinions_count",        default: 0,              null: false
    t.integer  "follower_users_count",  default: 0,              null: false
    t.integer  "followed_users_count",  default: 0,              null: false
    t.integer  "followed_topics_count", default: 0,              null: false
    t.string   "email"
    t.decimal  "last_lat"
    t.decimal  "last_lng"
    t.string   "last_country"
    t.boolean  "guest",                 default: false
    t.boolean  "ghost",                 default: false
    t.text     "bio"
    t.integer  "profile_video_id"
    t.string   "handle"
    t.string   "intercom_status",       default: "unregistered"
    t.integer  "seen_time"
    t.integer  "hosted_issues_count"
    t.integer  "open_asks_count"
    t.integer  "responded_asks_count"
    t.datetime "last_login"
    t.index ["facebook_uid"], name: "index_users_on_facebook_uid", unique: true, where: "(facebook_uid IS NOT NULL)", using: :btree
    t.index ["name"], name: "index_users_on_name", using: :btree
    t.index ["profile_video_id"], name: "index_users_on_profile_video_id", using: :btree
    t.index ["twitter_uid"], name: "index_users_on_twitter_uid", unique: true, where: "(twitter_uid IS NOT NULL)", using: :btree
  end

  create_table "words", force: :cascade do |t|
    t.string   "language"
    t.string   "word"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.boolean  "profanity",  default: true
  end

  add_foreign_key "action_items", "users", column: "actor_id"
  add_foreign_key "banners", "topic_dashboards", on_delete: :cascade
  add_foreign_key "claps", "opinions"
  add_foreign_key "claps", "users"
  add_foreign_key "followings", "users", column: "follower_id"
  add_foreign_key "ghost_metadata", "topics"
  add_foreign_key "ghost_metadata", "users", column: "creator_user_id"
  add_foreign_key "ghost_metadata", "users", column: "ghost_user_id"
  add_foreign_key "module_locales", "topic_locales"
  add_foreign_key "module_locales", "topic_modules"
  add_foreign_key "notifications", "users", on_delete: :cascade
  add_foreign_key "offense_reports", "opinions", on_delete: :cascade
  add_foreign_key "offense_reports", "users", on_delete: :cascade
  add_foreign_key "opinion_reactions", "opinions", on_delete: :cascade
  add_foreign_key "opinion_reactions", "users", on_delete: :cascade
  add_foreign_key "opinion_requests", "opinions", on_delete: :cascade
  add_foreign_key "opinion_requests", "topics", on_delete: :cascade
  add_foreign_key "opinion_requests", "users", column: "requested_user_id", on_delete: :cascade
  add_foreign_key "opinion_requests", "users", column: "requester_user_id", on_delete: :cascade
  add_foreign_key "opinion_translations", "opinions"
  add_foreign_key "opinion_watches", "opinions"
  add_foreign_key "opinion_watches", "users"
  add_foreign_key "opinions", "topics", on_delete: :cascade
  add_foreign_key "opinions", "users", on_delete: :cascade
  add_foreign_key "player_ids", "users"
  add_foreign_key "push_notification_registrations", "users", on_delete: :cascade
  add_foreign_key "session_tokens", "users", on_delete: :cascade
  add_foreign_key "taggings", "hashtags", on_delete: :cascade
  add_foreign_key "topic_modules", "banners", on_delete: :cascade
  add_foreign_key "topic_modules", "categories", on_delete: :cascade
  add_foreign_key "topic_modules", "topic_dashboards", on_delete: :cascade
  add_foreign_key "topic_opens", "topics"
  add_foreign_key "topic_opens", "users"
  add_foreign_key "topics", "categories", on_delete: :cascade
  add_foreign_key "topics", "topic_locales"
  add_foreign_key "topics", "users", column: "creator_id"
  add_foreign_key "user_devices", "users"
  add_foreign_key "user_settings", "users"
  add_foreign_key "users", "opinions", column: "profile_video_id"
end
