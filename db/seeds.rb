# frozen_string_literal: true

MobileAppVersion.create platform: 'ios',
                        update_severity: 'soft',
                        version_identifier: '1.0.0'

TopicDashboard.create main: true

load 'db/dev_seeds.rb' unless Rails.env.production?
