# frozen_string_literal: true

Admin.create email: 'admin@example.com',
             password: 'password',
             password_confirmation: 'password'

user = User.create name: 'test user',
                   profile_picture_url: 'https://robohash.org/towards-test-user.png',
                   facebook_uid: '1412114918802057',
                   twitter_uid: '35710905',
                   twitter_handle: 'reden87'

SessionToken.create user: user,
                    provider: 'facebook',
                    device_uid: 'test',
                    token: 'test',
                    challenge: 'test'

movies = Category.find_or_create_by name: 'movies'
politics = Category.find_or_create_by name: 'politics'

Hashtag.create name: 'yolo'
Hashtag.create name: 'oscars'
Hashtag.create name: 'us elections'

50.times do
  Topic.create category: movies, title: FFaker::Movie.title
  Topic.create category: politics, title: "#{FFaker::Name.name} for president!"

  name = [FFaker::Name.name, FFaker::Internet.user_name].sample
  user = User.create name: name, profile_picture_url: "https://robohash.org/#{CGI::escape(name)}.png"

  100.times do
    video_name = %w(beasts beasts_mark_kermode beasts_ricquetta).sample
    url_prefix = 'https://s3-eu-west-1.amazonaws.com/towards-dev/beasts'
    Opinion.create user: user,
                   topic_id: Topic.where(category: movies).pluck(:id).sample,
                   video_hls_url: "#{url_prefix}/#{video_name}/#{video_name}.m3u8",
                   video_thumbnail_url: "#{url_prefix}/#{video_name}/thumb.png",
                   video_file_name: 'test.mov',
                   video_content_type: 'video/quicktime',
                   video_file_size: 10.kilobytes,
                   video_updated_at: Time.current
  end
end
